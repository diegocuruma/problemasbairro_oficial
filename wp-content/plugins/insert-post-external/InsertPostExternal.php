<?php

/**
 * Plugin Name: Insert Post External
 * Description: Cria conteúdo (post) a partir da ação do usuário
 * Version: 1.5
 * Author: Nelis Rodrigues e Tiago Pires
 * Author URI: https://www.facebook.com/tao.pires
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

final class InsertPostExternal
{

    /**
     * InsertPostExternal Instance
     * @var null
     */
    private static $_instance = null;

    /**
     * InsertPostExternal constructor.
     */
    public function __construct()
    {
        $this->includes();
    }

    /**
     * Instância Principal do Módulo de Lista de Presentes.
     * @static
     * @see MPL()
     * @return InsertPostExternal - Main instance.
     */
    public static function instance()
    {
        if (is_null(self::$_instance)) self::$_instance = new self;
    }

    /**
     * Seleciona o tipo de requisição
     * @param $type
     * @return bool
     */
    private function is_request($type)
    {
        switch ($type) {
            case 'admin' :
                return is_admin();
            case 'ajax' :
                return defined('DOING_AJAX');
            case 'cron' :
                return defined('DOING_CRON');
            case 'frontend' :
                return (!is_admin() || defined('DOING_AJAX')) && !defined('DOING_CRON');
        }
    }

    /**
     * Includes dos arquivos
     */
    private function includes()
    {
        if ($this->is_request('frontend')) {

            // Scripts
            include_once('includes/model/class_insert_post.php');
            include_once('includes/view/short_code.php');

        }
    }

}

InsertPostExternal::instance();