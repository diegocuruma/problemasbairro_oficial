<?php global $post; ?>

<form name="cadastro-de-anuncio" action="<?php the_permalink(); ?>"
      method="POST" enctype="multipart/form-data" class="insert_post col-md-12">

    <div class="row">

        <?php do_action('mipe-notice'); ?>

        <div class="col-md-4">
            <div class="form-group">
               <label>Nome do bairro</label>
                <input type="text" class="form-control required" required id="nome"
                       placeholder="Nome do bairro" name="nome">
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group">
               <label>Nome da cidade</label>
                <input type="text" class="form-control required" required id="cidade"
                       placeholder="Nome da sua cidade" name="cidade">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               <label>Nome da rua ou avenida</label>
                <input type="text" class="form-control required" required id="rua"
                       placeholder="Rua" name="rua">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
               <label>Seu e-mail</label>
                <input type="email" class="form-control required email" required id="email"
                       placeholder="Email" name="email">
            </div>
        </div>

        <!--<div class="col-md-4">
            <div class="form-group">
                <input type="text" class="form-control required" required id="telefone"
                       placeholder="Telefone" name="telefone">
            </div>
        </div>-->
           
           
        <div class="col-md-4">
            <div class="form-group">
               <label>Para exibir seu nome na denuncia escreva seu nome, se não quiser, deixe em branco.</label>
                <input type="text" class="form-control" id="a_denuncia_e_anonima"
                       placeholder="Caso deseje exibir escreva aqui, caso não, deixe em branco:" name="a_denuncia_e_anonima">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               <label>Para deixar skype ou whatsapp para que outras pessoas possam entrar em contato e vocês colaborativamente possam marcar para solucionar o problema.</label>
                <input type="text" class="form-control" id="numero_skype"
                       placeholder="Caso deseje exibir escreva aqui, caso não, deixe em branco:" name="numero_skype">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               <label>O problema é recorrente?</label>
                <input type="text" class="form-control required" required id="recorrente"
                       placeholder="Escreva Sim ou Não" name="recorrente">
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <?php

                $args = array(
                    'taxonomy' => 'category',
                    'name' => 'categoria',
                    'id' => 'categoria',
                    'class' => 'form-control required',
                    'show_option_none' => 'Categoria',
                    'option_none_value' => '',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'value_field' => 'term_id',
                    'hide_empty' => 0,
                    'required' => true,
                    'exclude' => 7,
                );

                wp_dropdown_categories($args);

                ?>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <textarea class="form-control" id="descricao" placeholder="Descrição"
                          name="descricao" rows="10"></textarea>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">
                <input type="file" class="btn btn--blue" name="image_logo" id="image_logo"
                       value="Logo da empresa"/>
                <?php wp_nonce_field('image_logo', 'image_logo_nonce'); ?>
                <small style="color: red;">Somente arquivos no formato .jpg, .jpeg ou .png</small>
                <br/><br/>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">
            <input type="submit" class="btn-custom btn-custom--yellow" value="Cadastrar"/>
        </div>

    </div>

    <input type="hidden" name="action" value="insert_post_external"/>
    <?php wp_nonce_field('new-post'); ?>

</form>