<?php

if (!defined('ABSPATH')) exit;

// Exit if accessed directly.

class MIPE_Insert_Post
{

    public static function init()
    {
        // Inserir Post
        add_action('wp', array(__CLASS__, 'insert_post'));
        add_action('mipe-notice', array(__CLASS__, 'notice'));
    }

    public static function insert_post()
    {

        global $notice;

        if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'insert_post_external') {

            $notice = array();

            // Informações Estabelecimento
            $name = esc_attr($_POST['nome']);
            $email = esc_attr($_POST['email']);
            //$telefone = esc_attr($_POST['telefone']);
            $categoria = (int)esc_attr($_POST['categoria']);
            $denunciaAnonima = esc_attr($_POST['a_denuncia_e_anonima']);
            $numeroSkype = esc_attr($_POST['numero_skype']);
            $recorrente = esc_attr($_POST['recorrente']);
            $descricao = esc_attr($_POST['descricao']);
            $rua = esc_attr($_POST['rua']);
            $cidade = esc_attr($_POST['cidade']);

            // Informações Logo
            if (!empty($_FILES['image_logo']['name'])) {

                $image = $_FILES['image_logo']['name'];
                $extension = strtolower(pathinfo($image, PATHINFO_EXTENSION));

                if (!empty($_FILES['image_logo']['error'])) :
                    $notice[] = array('alert-danger', 'Error!', ' Erro ao enviar sua logo, por favor tente novamente.');

                elseif ($_FILES['image_logo']['size'] >= 2097152) :
                    $notice[] = array('alert-danger', 'Error!', ' Erro ao enviar sua logo, o tamanho não pode passar de 2M.');

                elseif (strpos('jpg;jpeg;png', $extension) === false) :
                    $notice[] = array('alert-danger', 'Error!', ' Erro ao enviar sua logo, o formato da imagem deve ser JPG, JPEG ou PNG');

                else:

                    require_once(ABSPATH . 'wp-admin/includes/image.php');
                    require_once(ABSPATH . 'wp-admin/includes/file.php');
                    require_once(ABSPATH . 'wp-admin/includes/media.php');

                endif;

            }

            // Mensagens de erro
            if (empty($name)) $notice[] = array('alert-danger', 'Error!', ' Por favor adicione o nome..');
            if (empty($email)) $notice[] = array('alert-danger', 'Error!', ' Por favor adicione o email.');
            //if (empty($telefone)) $notice[] = array('alert-danger', 'Error!', ' Por favor adicione o telefone.');
            if (empty($descricao)) $notice[] = array('alert-danger', 'Error!', ' Por favor adicione a descrição.');

            if (empty($notice)) {

                $args = array(
                    'post_author' => 1,
                    'post_title' => $name,
                    'post_content' => $descricao,
                    'post_type' => 'post',
                    'post_status' => 'draft'
                );

                $establishment_id = wp_insert_post($args);

                if (!$establishment_id) :

                    $notice[] = array('alert-warning', 'Alerta!', ' Erro ao gravar seus dados. Tente novamente.');

                else :

                    // Taxonomias (Categorias)
                    //wp_set_object_terms($establishment_id, array(15, 13, $establishment_local), 'local');
                    wp_set_object_terms($establishment_id, array($categoria), 'categoria', true);
                    //wp_set_object_terms($establishment_id, 'bronze', 'plano', true);

                    // Campos Personalizados
                    $predicted = array(
                        'email' => $email,
                        'telefone' => $telefone,
                        'descricao' => $descricao,
                        'recorrente' => $recorrente,
                        'a_denuncia_e_anonima' => $denunciaAnonima,
                        'numero_skype' => $numeroSkype,
                        'rua' => $rua,
                        'cidade' => $cidade,
                    );

                    foreach ($predicted as $key => $value) :
                        update_post_meta($establishment_id, $key, $value);
                    endforeach;

                    if(isset($image)){
                        // Pega a imagem e associa a banda e depois inseri
                        $attachment_id = media_handle_upload('image_logo', $establishment_id);
                        update_post_meta($establishment_id, '_thumbnail_id', $attachment_id);

                        if (!$attachment_id) $notice[] = array('alert-warning', 'Alerta!', ' Erro na geração da logo.');
                    }

                    $notice[] = array('alert-success', 'Parabéns!', ' Sua denúncia foi cadastrada com sucesso. Em 48h, após verificação pelo admin será exibida no site.');

                endif;

            }

        }
    }

    public static function notice()
    {

        global $notice;

        if (is_array($notice) && !empty($notice)) :
            foreach ($notice as $item) : ?>

                <div class="alert <?php echo $item[0] ?>" role="alert">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <strong><?php echo $item[1] ?></strong><?php echo $item[2] ?>
                </div>

                <?php
            endforeach;
        endif;

    }

}

MIPE_Insert_Post::init();
