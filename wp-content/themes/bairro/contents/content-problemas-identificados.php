<?php
/**
 * Created by PhpStorm.
 * User: Felipe/diego
 * Date: 21/11/2018
 * Time: 17:05
 */
?>
<section class="c-product-highlight--open c-product-highlight">
    <div class="o-wrapper">
        <h2 class="c-product-highlight__title c-title">Problemas identificados</h2>
        <ul class="c-product-highlight__list u-clearfix">
        <?php
            $identificados = new WP_Query( array ( 'post_type' => 'post', 'cat' => '8,4,3,1', 'showposts' => 3 ) );
            //$identificados = new WP_Query( array ( 'post_type' => 'post', 'category_name' => 'identificados', 'showposts' => 3 ) );
            while ( $identificados -> have_posts() ): $identificados -> the_post();
        ?>

                   <?php
                        global $name_cat;
                        $content = get_the_content();
                        $resumo = substr($content, 0, 150).'...';
                        $repCat = get_the_terms($post->ID, 'category');
                        $condition = get_field('problema');
                        if($repCat) {
                            foreach($repCat as $cat) {
                                $name_cat = $cat->name;
                            }
                        }
                    ?>
                    <?php if($condition == 'nao') : ?>
                        <li class="c-product-card c-product-highlight__item">
                            <a href="<?php the_permalink();?>" title="Ver detalhes" class="c-product-card__header flex flex--wrap">
                                <?php if(has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('capa', array('class' => 'c-product-card__img u-db')); ?>
                                <?php else :?>
                                    <img src="<?php echo WP_IMAGES ?>/image-default.jpg" class="c-product-card__img u-db">
                                <?php endif; ?>
                                <h3 class="c-product-card__title u-fwm"><?php the_title(); ?></h3>
                                <span class="c-product-card__date u-fwm"><?php the_time('d/m/Y'); ?></span>
                            </a>
                            <p class="c-product-card__resume"><?php echo $resumo; ?></p>
                            <div class="c-product-card__footer">
                                <strong class="c-product-card__place u-fwm"><?php the_field('bairro');?></strong>
                                <p class="c-product-card__cat u-fwm"><span>Categoria:</span> <em><?php echo $name_cat; ?></em></p>
                                <a href="<?php the_permalink();?>" title="Ver detalhes" class="c-btn--ghost c-btn--small c-btn">Ver detalhes</a>
                            </div>
                        </li>
                    <?php endif; ?>
            <?php endwhile; ?>
        </ul>

          <a href="<?php echo get_permalink(get_page_by_path('problemas-identificados')); ?>" title="Ver problemas identificados" class="">
                  Ver problemas identificados
          </a>
    </div>
</section>
