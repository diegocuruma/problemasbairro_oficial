<div class="o-wrapper">
    <section class="o-main__content u-clearfix">
        <?php get_template_part('contents/content','loop-news'); ?>
    </section>
    <aside class="o-main__aside">
        <?php dynamic_sidebar('sidebar-blog'); ?>
    </aside>
</div>