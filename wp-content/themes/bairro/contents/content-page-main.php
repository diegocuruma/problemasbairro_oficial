<div class="o-wrapper">
    <section class="u-clearfix">
        <?php if( have_posts() ): while( have_posts() ):  the_post(); ?>
            <div class="c-text">
                <?php the_content(); ?>
            </div>
        <?php endwhile; endif; ?>
    </section>
</div>

<?php #echo do_shortcode('[galeries]'); ?>