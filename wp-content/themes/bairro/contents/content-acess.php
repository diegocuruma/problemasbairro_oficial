<?php
/**
 * Created by PhpStorm.
 * User: Felipe
 * Date: 17/11/2018
 * Time: 11:19
 */

// acessibilidade
// link: https://medium.com/hash-js/implemente-uma-versão-de-alto-contraste-no-seu-site-dcef330a09fd
?>
<ul class="c-block-acess flex flex-gap--md">
    <!-- <li class="c-block-acess__item">
        <a href="<?php echo get_site_url(); ?>/multiplique-a-ideia" title="Multiplique a ideia" class="c-block-acess__link">Multiplique a ideia</a>
    </li> -->
    <li class="c-block-acess__item">
        <a class="c-block-acess__link" href="#" id="altocontraste" accesskey="3" onclick="window.toggleContrast()" onkeydown="window.toggleContrast()" title="Alto contraste [Alt + 3]" class="c-block-acess__link">Alto contraste</a>
    </li>
    <!-- <li class="c-block-acess__item">
        <a href="#" class="c-block-acess__link" title="Transparencia">Transparencia</a>
    </li> -->
    <li class="c-block-acess__item">
        <a href="https://instagram.com/omeubairro_" class="c-block-acess__link" target="_blank "title="Instagram">Instagram</a>
    </li>
</ul>
