<?php
/**
 * Created by PhpStorm.
 * User: Felipe
 * Date: 24/11/2018
 * Time: 13:06
 */
$new_query = new WP_Query( array(
    'post_type'      => 'banco',
    'orderby'        => 'menu_order',
    'paged'          => $paged
) );
?>
<section class="c-complementary">
    <div class="o-wrapper">
        <h2 class="c-complementary__title c-title">Banco de ideias</h2>
        <ul class="c-complementary__list">
            <?php while ( $new_query->have_posts() ) : $new_query->the_post(); ?>
                <li class="c-card-ideias c-complementary__item">
                    <h3 class="c-card-ideias__title u-fwl"><?php the_title();?></h3>
                    <a href="<?php the_permalink();?>" class="c-card-ideias__link u-fwm">Saiba mais</a>
                </li>
            <?php endwhile; ?>
        </ul>
    </div>
</section>
