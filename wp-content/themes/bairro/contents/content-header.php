<header class="o-main__header">
    <div class="o-wrapper">
        <h1 class="c-page-title c-title">
            <?php
                if( is_404() ){
                    echo "Página não encontrada.";
                }
                elseif( is_tax('categoria') ){
                    echo single_cat_title();
                }
                // elseif(is_search() || is_singular('post') ){
                elseif( is_search() ){
                    echo 'Search';
                }
                else{
                    the_title();
                }
            ?>
        </h1>
    </div>
</header>

<?php #echo do_shortcode('[image_page]'); ?>
<?php #echo wp_breadcrumbs(); ?>