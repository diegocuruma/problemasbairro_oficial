<div class="o-wrapper">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <section class="o-main__content u-clearfix">
            <?php #the_title(); ?>

            <div class="c-product-meta flex flex--space-between flex--center-y">
                <h3 class="c-product-meta__place"><?php the_field('rua');?></h3>
                <?php
                    $count = 0;
                    $posttags = get_the_tags();

                    if ($posttags) {
                        foreach($posttags as $tag) {
                            $break = $count++;
                            echo '<span class="c-product-meta__tag">#'.$tag->name. '</span>';

                            if( $break == 1 ){ break; }
                        }
                    }
                ?>
                <span class="c-product-meta__date"><?php the_time('d/m/Y'); ?></span>
            </div>


            <figure class="c-product-thumbnail">
                <?php if(has_post_thumbnail()) : ?>
                    <img src="<?php the_post_thumbnail_url();?>" width="470" class="c-product-thumbnail__img">
                <?php else : ?>
                    <img src="<?php echo WP_IMAGES ?>/image-default.jpg" width="470" class="c-product-thumbnail__img">
                <?php endif; ?>
                <figcaption class="c-product-thumbnail__caption"></figcaption>
            </figure>
          

            <div class="c-product-text c-text"><?php the_content(); ?></div>
        </section>

        <aside class="o-main__aside">
            <ul class="c-product-infos">
                <li class="c-product-infos__item">Tipo:

                <?php
                $tax = 'category';
                $cats = get_the_terms($post->ID, $tax);
                if ($cats) {
                    foreach ($cats as $cat) {
                        echo '<span><strong>#' . $cat->name . ' </strong></span>';
                    }
                }
                ?>




                </li>
                <!--<li class="c-product-infos__item">Tag: <?php the_field('tag');?></li>-->
                <!--<li class="c-product-infos__item">Solucionado: <?php the_field('solucionado');?></li>-->
                <?php if(get_field('cidade')) : ?>
                    <li class="c-product-infos__item">Cidade: <?php the_field('cidade');?></li>
                <?php endif; ?>

                <li class="c-product-infos__item">Bairro: <?php the_title();?></li>
                <?php if(get_field('a_denuncia_e_anonima')) : ?>
                    <li class="c-product-infos__item">Denunciante: <?php the_field('a_denuncia_e_anonima');?></li>
                <?php endif; ?>
                <li class="c-product-infos__item">Recorrente: <?php the_field('recorrente');?></li>
                <?php if(get_field('numero_skype')) : ?>
                <li class="c-product-infos__item">
                Entrar em contato pra ajudar a resolver colaborativamente:<br><br> <?php the_field('numero_skype');?>
                </li>
                <?php endif; ?>

            </ul>

            <!--<a href="<?php site_url(); ?>" class="c-btn--secondary c-btn">Deixe um comentário abaixo para o status do Problema</a>-->
            <a href="#comentario" class="c-btn--secondary c-btn">Deixe um comentário abaixo para o status do Problema</a>
        </aside>
    <?php endwhile; endif; ?>

</div>

<footer class="o-main__footer" id="comentario">
    <div class="o-wrapper">
        <div class="c-comment flex flex--wrap">
            <h4 class="c-comment__title">Comentários</h4>

            <dl class="c-comment-info c-comment__info flex flex--wrap">
                <dt class="c-comment-info__title">Para melhor solução digite como está o problema:</dt>
                <dd class="c-comment-info__tag">[Resolvido]</dd>
                <dd class="c-comment-info__tag">[Atual]</dd>
            </dl>
        </div>
        <?php comments_template(); ?>
    </div>
</footer>
