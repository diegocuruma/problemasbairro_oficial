<?php
/**
 * Created by PhpStorm.
 * User: Felipe
 * Date: 17/11/2018
 * Time: 12:06
 */
?>
<section class="block-categorias">
    <div class="container">
        <div class="row">
            <section class="block-categorias-content col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <ul>
                            <?php
                                $args = array(
                                    'orderby' => 'name',
                                    'order' => 'ASC'
                                );

                                $categories = get_categories($args);
                                foreach($categories as $category) {
                                    echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "Ver postagens em %s" ), $category->name ) . '" ' . '>' . $category->name.'</a></li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <aside class="col-md-2">
                        <a href="<?php echo site_url(); ?>/identificar-problema">identificar problema</a>
                    </aside>
                </div>
            </section>
        </div>
    </div>
</section>
