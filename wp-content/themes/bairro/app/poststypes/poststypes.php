<?php

// Post Type Produtos
if (!function_exists('banco')) {

    function banco()
    {
        $labels = array(
            'name' => 'Banco de Ideias',
            'singular_name' => 'Banco de Ideia',
            'menu_name' => 'Banco de Ideias',
            'parent_item_colon' => 'Ideias',
            'all_items' => 'Todos as Ideias',
            'view_item' => 'Ver Ideia',
            'add_new_item' => 'Adicionar nova',
            'add_new' => 'Nova Ideia',
            'edit_item' => 'Editar Ideia',
            'update_item' => 'Atualizar Ideia',
            'search_items' => 'Ideia',
            'not_found' => 'Nenhuma Ideia Encontrada',
            'not_found_in_trash' => 'Nenhuma ideia encontrada na lixeira',
        );

        $rewrite = array(
            'slug' => 'banco',
            'with_front' => true,
            'pages' => true,
            'feeds' => false,
        );

        $args = array(
            'label' => 'Ideias',
            'description' => 'Ideias',
            'labels' => $labels,
            'supports' => array('title', 'thumbnail','editor'),
            'taxonomies' => array(),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            //puxar o posttype para o menu e criar a sub sessão
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 6,
            'menu_icon' => 'dashicons-cart',
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'query_var' => 'banco',
            'rewrite' => $rewrite,
            'capability_type' => 'post',
        );
        register_post_type('banco', $args);
    }

    add_action('init', 'banco');
}

// Register Taxonomy for cardapio
/*if (!function_exists('tax_for_produto')) {
    function tax_for_produto()
    {
        register_taxonomy('categoria', 'produto', array(
                'label' => __('Categoria'),
                'rewrite' => array(
                    'slug' => 'produtos',
                    'with_front' => true,
                    'hierarchical' => true,
                ),
                'hierarchical' => true)
        );
    }
    add_action('init', 'tax_for_produto');
}*/



/* Anúncios */
/*function ads() {
    $labels = array(
        'name'                => 'Problemas',
        'singular_name'       => 'Problemas',
        'menu_name'           => 'Problemas',
        'parent_item_colon'   => 'Problemas:',
        'all_items'           => 'Todos os Problemas',
        'view_item'           => 'Ver Problemas',
        'add_new_item'        => 'Adicionar novo problema',
        'add_new'             => 'Novo problema',
        'edit_item'           => 'Editar problema',
        'update_item'         => 'Atualizar problema',
        'search_items'        => 'Problemas',
        'not_found'           => 'Nenhum problema Encontrado',
        'not_found_in_trash'  => 'Nenhuma problema Encontrado na Lixeira',
    );
    $rewrite = array(
        'slug'                => 'problema',
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => false,
    );
    $args = array(
        'label'               => 'Problemas',
        'description'         => 'Problemas',
        'labels'              => $labels,
        'supports'            => array( 'title', 'thumbnail', 'editor' ),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //puxar o posttype para o menu e criar a sub sessão
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => true,
        'menu_position'       => 7,
        'menu_icon'           => 'dashicons-lightbulb',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'query_var'           => 'problemas',
        'rewrite'             => $rewrite,
        'capability_type'     => 'post',
    );
    register_post_type( 'problema', $args );
}

add_action( 'init', 'ads', 0 );*/

/* Taxonome Categoria */
/*
function tax_for_ads(){
    register_taxonomy( 'categoria', 'problema', array(
        'label' => __( 'Categoria' ),
        'rewrite' => array(
            'slug' => 'categoria',
            'with_front' => true,
            'hierarchical' => true,
        ),
        'hierarchical' => true ) );
}

add_action( 'init', 'tax_for_ads' );
*/
