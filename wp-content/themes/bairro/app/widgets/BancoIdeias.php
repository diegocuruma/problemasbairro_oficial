<?php


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class BancoIdeias extends WP_Widget
{

    public function __construct()
    {

        parent::__construct(
            'banco_ideias',
            'Widget Bando de Ideias (Home)',
            array(
                'classname' => 'banco_ideias',
                'description' => 'Bando de ideias úteis'
            )
        );

    }


    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        extract($args);

        ?>
        <!-- Your Html -->
            <?php get_template_part('contents/content','banco-de-ideias')?>
        <!--/ Your Html -->
        <?php
    }


}


/* Register the widget */
add_action('widgets_init', function(){return register_widget("BancoIdeias");});

