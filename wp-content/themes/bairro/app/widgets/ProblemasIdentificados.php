<?php


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Problemas extends WP_Widget
{

    public function __construct()
    {

        parent::__construct(
            'problemas',
            'Widget Problemas Identificados (Home)',
            array(
                'classname' => 'problemas',
                'description' => 'Problemas identificados já cadastrados'
            )
        );

    }


    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        extract($args);

        ?>
           
            <!-- Your Html -->
                <?php get_template_part('contents/content','problemas-identificados')?>
            <!--/ Your Html -->
        <?php
    }


}


/* Register the widget */
add_action('widgets_init', function(){return register_widget("Problemas");});

