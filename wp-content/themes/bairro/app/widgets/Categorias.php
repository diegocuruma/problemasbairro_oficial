<?php
/**
 * Created by PhpStorm.
 * User: Felipe
 * Date: 17/11/2018
 * Time: 12:03
 */
?>
<?php


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Categorias extends WP_Widget
{

    public function __construct()
    {

        parent::__construct(
            'categorias',
            'Widget Categorias (Home)',
            array(
                'classname' => 'categorias',
                'description' => 'Categorias dos problemas'
            )
        );

    }


    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        extract($args);

        ?>
        <!-- Your Html -->
            <?php get_template_part('contents/content','categorias');?>
        <!--/ Your Html -->
        <?php
    }


}


/* Register the widget */
add_action('widgets_init', function(){return register_widget("Categorias");});

