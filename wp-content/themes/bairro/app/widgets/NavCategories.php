<?php


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class NavCategories extends WP_Widget
{

    public function __construct()
    {

        parent::__construct(
            'categories',
            'Categorias ( Barra Lateral - Blog )',
            array(
                'classname' => 'categories',
                'description' => 'CAtegorias do blog.'
            )
        );

    }


    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        extract($args);

        ?>
        
        <nav class="c-categories">
            <h4 class="c-categories__title u-fwn">Categorias</h4>

            <ul class="c-categories__list">
                <?php
                    $params = array(
                        'orderby' => 'name',
                        'order' => 'ASC',
                        'hide_empty' => 0,
                        'hierarchical' => 'false'
                    );

                    $categories = get_terms('category',$params);

                    foreach($categories as $category):
                ?>
                    <li class="c-categories__item">
                        <a href="<?php echo get_category_link($category)?>" title="<?php echo $category->name; ?>"  class="c-categories__link"><?php echo $category->name; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>

        <?php
    }
}


/* Register the widget */
add_action('widgets_init', function(){return register_widget("NavCategories");});