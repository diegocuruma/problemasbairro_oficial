<?php
/*
/* Shortcode Galeria
*/
if (!function_exists('galeries')) {
    function galeries()
    {
        ?>
        <ul class="row galeries">
            <?php if (have_rows('imagens')): while (have_rows('imagens')): the_row(); ?>
                <li class="col-md-3 col-3 galeries__item">
                    <?php
                    $image_id = get_sub_field('imagem');
                    $image_data_full = get_thumb( $image_id['id'], 'full');
                    $image_data = get_thumb($image_id['id'], 'medium');
                    ?>
                    <a href="<?php echo $image_data_full['url_thumb']; ?>" data-fancybox="images">
                        <figure>
                            <img src="<?php echo WP_IMAGES; ?>/ring.gif" data-original="<?php echo $image_data['url_thumb']; ?>" alt="Imagem"
                                 title="<?php echo $image_data['title']; ?>"
                                 width="<?php echo $image_data['url_thumb_width']; ?>"
                                 height="<?php echo $image_data['url_thumb_height']; ?>" class="lazy img-fluid"/>
                                 
                        </figure>
                    </a>
                </li>
            <?php endwhile; endif; ?>
        </ul>
        <?php
  }

    add_shortcode('galeries', 'galeries');

}


