<?php

/**
 * @package sklbase
 * @since sklbase 1.0
 * @license GPL 2.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> <?php html_tag_schema(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="author" content="E-deas, contato@e-deas.com.br"/>
    <meta name="copyright" content="E-deas Web">
    <meta name="theme-color" content="#1a1a1a">

    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="author" href="<?php echo WP_HOME ?>/humans.txt" type="text/plain"/>

    <!--manifest-->
    <link rel="manifest" href="<?php echo WP_HOME ?>/manifest.json"/>
    <link rel="shortcut icon" href="<?php echo WP_HOME ?>/icon.png"/>

    <!--service worker-->
    <script defer src="<?php echo WP_HOME ?>/site.js"></script>

    <link rel="shortcut icon" href="<?php echo WP_IMAGES ?>/favicon.png"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="o-header">
    <section class="o-header__top">
        <div class="o-wrapper flex flex--end">
            <?php echo get_template_part('contents/content', 'acess');?>
        </div>
    </section>
    <section class="o-header__main">
        <div class="o-wrapper flex flex--wrap flex--center-y">
            <a class="c-logo" href="<?php echo WP_HOME; ?>" title="<?php echo bloginfo('title'); ?>"><?php echo getLogoHeader(); ?></a>

            <nav id="acbl-menu" class="c-nav-primary">
                <a href="#" class="c-btn-rwd u-db" data-toggle-class="is-active" data-toggle-target=".c-menu-primary, .c-cta-rwd, .c-logo-rwd" data-toggle-target-previous>
                    <svg class="ham hamRotate ham1 u-db" viewBox="0 0 100 100" width="40" onclick="this.classList.toggle('active')">
                        <path class="line top" d="m 30,33 h 40 c 0,0 9.044436,-0.654587 9.044436,-8.508902 0,-7.854315 -8.024349,-11.958003 -14.89975,-10.85914 -6.875401,1.098863 -13.637059,4.171617 -13.637059,16.368042 v 40" /><path class="line middle" d="m 30,50 h 40" /><path class="line bottom" d="m 30,67 h 40 c 12.796276,0 15.357889,-11.717785 15.357889,-26.851538 0,-15.133752 -4.786586,-27.274118 -16.667516,-27.274118 -11.88093,0 -18.499247,6.994427 -18.435284,17.125656 l 0.252538,40" />
                    </svg>
                </a>

                <a href="<?php echo WP_HOME; ?>/identificar-problemas" class="c-cta-rwd c-btn c-btn--secondary c-btn--small">Identificar problemas</a>

                <div class="c-logo-rwd"><?php echo getLogoHeader(); ?></div>

                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'menu',
                        'menu' => 'container',
                        'depth' => 0,
                        'container' => false,
                        'menu_class' => 'c-menu-primary',
                        //Process nav menu using our custom nav walker
                        'walker' => new wp_bootstrap_navwalker())
                    );
                ?>
            </nav>
        </div>
    </section>

    <?php if( !is_home() ): ?>
        <?php echo do_shortcode('[image_page]'); ?>
    <?php endif; ?>
</header>
<main id="acbl-content" class="o-main">
    <?php if( !is_home() ): ?>
        <article class="<?php is_singular('post') ? print 'o-product' : print 'o-basic-page'; ?>">
    <?php endif; ?>

        <?php if( !is_home() ): ?>
            <?php get_template_part('contents/content','header' ); ?>
        <?php endif; ?>
