<?php get_header(); ?>

<div class="o-wrapper">
    <section class="">
        <?php while (have_posts()) : the_post()?>
            <div class="c-text">
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </section>
</div>

<?php get_footer(); ?>