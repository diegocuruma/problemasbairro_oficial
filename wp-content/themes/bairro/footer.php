    <?php !is_home() ? print '</article>' : print ''; ?>
</main>
<footer class="o-footer">
    <hr>
    <div class="o-wrapper">
        <dl class="c-devs">
            <dt class="c-devs__title u-fwm">Quem desenvolveu:</dt>
            <?php if( have_rows('desenvolvedores','option') ) : ?>
                <?php while (have_rows('desenvolvedores','option') ) : the_row(); ?>
                    <dd class="c-devs__item col-md-1">
                        <img src="<?php the_sub_field('imagem');?>" alt="Foto do desenvolvedor" title="<?php the_sub_field('nome');?>" class="c-devs__img" width="48" height="48">
                    </dd>
                <?php endwhile; ?>
            <?php endif;?>
        </dl>
        <dl class="c-apoio">
            <dt class="c-apoio__title u-fwb">Apoio:</dt>
            <?php if( have_rows('apoio','option') ) : ?>
                <?php while (have_rows('apoio','option') ) : the_row(); ?>
                    <dd class="c-apoio__item">
                        <img src="<?php the_sub_field('imagem');?>" alt="Apoio" class="c-apoio__img"  width="64" height="64">
                    </dd>
                <?php endwhile; ?>
            <?php endif;?>
        </dl>

        <h6 class="c-cms-title">Feito com: Wordpress</h6>
    </div>
</footer>

        <?php wp_footer(); ?>
	</body>
</html>