
jQuery(document).ready(function ($) {

    /**
     * Painel Principal ( Home )
     */
    var swiper = new Swiper('.s1', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: false,
        // spaceBetween:0,
        //centeredSlides: true,
        loop: false,
        autoplay: false,
        effect: 'fade',
        preventClicks: true,
        autoplayDisableOnInteraction: false
    });


    /**
     * Logos ( Carousel )
     */
    var swiper = new Swiper('.s2', {
        slidesPerView: 5,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        loop: true,
        autoplay: 2000,
        autoplayDisableOnInteraction: false,
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });


    /**
     * Fancybox
     */
    $(".gal-item a").addClass('fancybox');
    $(".fancybox").attr('rel', 'gallery');

    $(".fancybox").fancybox({
        maxWidth: 1200,
        maxHeight: 768,
        fitToView: false,
        width: '100%',
        height: '100%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });


    /**
     * Se for mobile insere not-hover nos elementos setados
     */
    /*
     if(is_Mobile()) {
         jQuery(document).ready(function($) {
            $('a').addClass('not-hover');
         });
     }
     */


    /**
     * Menu do bootstrap com hover ao inves de click
     */
    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });


    /**
     * Fancybox e form com callback exemplo
     */
    $("a.form-fancy").on('click', function () {
        $.fancybox(
            $('.formulario').html(),
            {
                'maxWidth': 990,
                'maxHeight': 768,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'hideOnContentClick': false,
                afterShow: function () {

                    $('.telefone input').focusout(function () {
                        var phone, element;
                        element = $(this);
                        element.unmask();
                        phone = element.val().replace(/\D/g, "");
                        if (phone.length > 10) {
                            element.mask('(99) 99999-9999');
                        } else {
                            element.mask('(99) 9999-99999');
                        }
                    }).trigger('focusout');

                    $(".ida input").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        onClose: function (selectedDate) {
                            $(".volta input").datepicker("option", "minDate", selectedDate);
                        }
                    });

                    $(".volta input").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        onClose: function (selectedDate) {
                            $(".ida input").datepicker("option", "maxDate", selectedDate);
                        }
                    });

                }
            }
        );
    });


    /**
     * Adiciona classe no menu
     */
    $('.menu-item > a').addClass('menu-link');


    /**
     * Alert nos forms
     */
    $('.wpcf7-form-control-wrap span').addClass('bg-danger');


    /**
     * Lazy load para carregamento de imagens
     */
    $(".lazy").lazyload({effect: "fadeIn"});


    /**
     * Colocando active no menu on page
     */
    /*$('.navbar li').click(function(e) {
     $('ul.nav > li').click(function (e) {
     e.preventDefault();
     $('ul.nav > li').removeClass('active');
     $(this).addClass('active');
     });
     })*/


    /**
     * Filtro do select2 pra pagina representantes
     */
    /*$(".js-example-placeholder-single").select2({
     placeholder: "Selecione a cidade",
     allowClear: true,
     language: "pt_PT"
     });

     $(".js-states").select2({
     placeholder: "Selecione o estado",
     allowClear: true,
     language: "PT"
     });*/


    /**
     * js pra mudar de cor e add class adicional no header
     */
    $(window).scroll(function () {
        if ($(document).scrollTop() > 120) {
            $('nav').addClass('shrink');
        } else {
            $('nav').removeClass('shrink');
        }
    });


    /**
     * active dos marcadores no menu
     */

    // add a class
    $('.menu-item').addClass('menu__item');
    $('.menu__item a').addClass('menu__link');
    $('.menu__item a').addClass('menu__link');

    // add a class active no 1 item "a"
    $('.menu-item-4 a').addClass('menu__link--active');

    // remove class padrao do bt
    $('.menu-item').removeClass('active');

    /*começa a magia dos actives*/
    $('.menu__link').on('click', function () {
        //c.preventDefault();

        var id = $(this).attr('href'), targetOffset = $(id).offset().top;

        // add
        $('.menu__item a').removeClass('menu__link--active');
        $(this).addClass('menu__link--active');

        // anima
        $('html,body').animate({
            scrollTop: targetOffset - 100
        }, 500);
    });


});




(function () {
    var Contrast = {
        storage: 'contrastState',
        cssClass: 'contrast',
        currentState: null,
        check: checkContrast,
        getState: getContrastState,
        setState: setContrastState,
        toogle: toogleContrast,
        updateView: updateViewContrast
    };

    window.toggleContrast = function () { Contrast.toogle(); };

    Contrast.check();

    function checkContrast() {
        this.updateView();
    }

    function getContrastState() {
        return localStorage.getItem(this.storage) === 'true';
    }

    function setContrastState(state) {
        localStorage.setItem(this.storage, '' + state);
        this.currentState = state;
        this.updateView();
    }

    function updateViewContrast() {
        var body = document.body;

        if (this.currentState === null)
            this.currentState = this.getState();

        if (this.currentState)
            body.classList.add(this.cssClass);
        else
            body.classList.remove(this.cssClass);
    }

    function toogleContrast() {
        this.setState(!this.currentState);
    }
})();


/*! easy-toggle-state v1.10.0 | (c) 2019 Matthieu Bué <https://twikito.com> | MIT License | https://twikito.github.io/easy-toggle-state/ */
!function(){"use strict";function t(t,e,r){return e in t?Object.defineProperty(t,e,{value:r,enumerable:!0,configurable:!0,writable:!0}):t[e]=r,t}function e(t){return function(t){if(Array.isArray(t)){for(var e=0,r=new Array(t.length);e<t.length;e++)r[e]=t[e];return r}}(t)||function(t){if(Symbol.iterator in Object(t)||"[object Arguments]"===Object.prototype.toString.call(t))return Array.from(t)}(t)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance")}()}var r=document.documentElement.getAttribute("data-easy-toggle-state-custom-prefix")||"toggle",n=function(t){return["data",arguments.length>1&&void 0!==arguments[1]?arguments[1]:r,t].filter(Boolean).join("-")},i=n("arrows"),o=n("class"),a=n("escape"),c=n("event"),u=n("group"),s=n("is-active"),g=n("outside"),l=n("outside-event"),f=n("radio-group"),A=n("target"),d=n("target-all"),h=n("target-next"),v=n("target-only"),b=n("target-parent"),E=n("target-previous"),m=n("target-self"),T=n("state"),w=n("trigger-off"),p=new Event("toggleAfter"),y=new Event("toggleBefore"),S=function(t,r){var n=t?"[".concat(t,"]"):"";return e(r?r.querySelectorAll(n):document.querySelectorAll("[".concat(o,"]").concat(n).trim()))},L=function(t,e){return t.dispatchEvent(e)},k=function(e){var r,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:(t(r={},"aria-checked",e.isToggleActive),t(r,"aria-expanded",e.isToggleActive),t(r,"aria-hidden",!e.isToggleActive),t(r,"aria-selected",e.isToggleActive),r);return Object.keys(n).forEach(function(t){return e.hasAttribute(t)&&e.setAttribute(t,n[t])})},x=function(t){var e=t.hasAttribute(u)?u:f;return S("".concat(e,'="').concat(t.getAttribute(e),'"')).filter(function(t){return t.isToggleActive})},O=function(t,r){if(0===r.length)return console.warn("There's no match for the selector '".concat(t,"' for this trigger")),[];var n=t.match(/#\w+/gi);return n&&n.forEach(function(t){var n=e(r).filter(function(e){return e.id===t.slice(1)});n.length>1&&console.warn("There's ".concat(n.length," matches for the selector '").concat(t,"' for this trigger"))}),e(r)},I=function(t){return document.addEventListener(t.getAttribute(l)||t.getAttribute(c)||"click",D,!1)},D=function t(e){var r=e.target,n=e.type,i=!1;S(g).filter(function(t){return t.getAttribute(l)===n||t.getAttribute(c)===n&&!t.hasAttribute(l)||"click"===n&&!t.hasAttribute(c)&&!t.hasAttribute(l)}).forEach(function(t){var e=r.closest("["+T+'="true"]');e&&e.easyToggleStateTrigger===t&&(i=!0),!i&&t!==r&&t.isToggleActive&&(t.hasAttribute(u)||t.hasAttribute(f)?H:Y)(t)}),i||document.removeEventListener(n,t,!1),r.hasAttribute(g)&&r.isToggleActive&&I(r)},j=function(t){return Y(t.currentTarget.targetElement)},q=function(t){if(t.hasAttribute(g))return t.hasAttribute(f)?console.warn("You can't use '".concat(g,"' on a radio grouped trigger")):t.isToggleActive?I(t):void 0},B=function(t,e,r){return function(t){if(t.hasAttribute(A)||t.hasAttribute(d)){var e=t.getAttribute(A)||t.getAttribute(d);return O(e,document.querySelectorAll(e))}if(t.hasAttribute(b)){var r=t.getAttribute(b);return O(r,t.parentElement.querySelectorAll(r))}if(t.hasAttribute(m)){var n=t.getAttribute(m);return O(n,t.querySelectorAll(n))}return t.hasAttribute(E)?O("previous",[t.previousElementSibling].filter(Boolean)):t.hasAttribute(h)?O("next",[t.nextElementSibling].filter(Boolean)):[]}(t).forEach(function(n){L(n,y),n.isToggleActive=!n.isToggleActive,k(n),r&&!n.classList.contains(e)&&n.classList.add(e),r||n.classList.toggle(e),t.hasAttribute(g)&&(n.setAttribute(T,t.isToggleActive),n.easyToggleStateTrigger=t),L(n,p),function(t,e){var r=S(w,t);if(0!==r.length)e.isToggleActive?r.forEach(function(t){t.targetElement=e,t.addEventListener("click",j,!1)}):r.forEach(function(t){t.removeEventListener("click",j,!1)})}(n,t)})},Y=function(t){L(t,y);var e=t.getAttribute(o)||"is-active";return t.isToggleActive=!t.isToggleActive,k(t),t.hasAttribute(v)||t.classList.toggle(e),L(t,p),B(t,e,!1),q(t)},C=function(e){var r;L(e,y);var n=e.getAttribute(o)||"is-active";return e.isToggleActive=!0,k(e,(t(r={},"aria-checked",!0),t(r,"aria-expanded",!0),t(r,"aria-hidden",!1),t(r,"aria-selected",!0),r)),e.hasAttribute(v)||e.classList.contains(n)||e.classList.add(n),L(e,p),B(e,n,!0),q(e)},H=function(t){var e=x(t);return 0===e.length?Y(t):-1===e.indexOf(t)?(e.forEach(Y),Y(t)):-1===e.indexOf(t)||t.hasAttribute(f)?void 0:Y(t)},M=function(){S(s).filter(function(t){return!t.isETSDefInit}).forEach(function(t){return t.hasAttribute(u)||t.hasAttribute(f)?x(t).length>0?console.warn("Toggle group '".concat(t.getAttribute(u)||t.getAttribute(f),"' must not have more than one trigger with '").concat(s,"'")):(C(t),void(t.isETSDefInit=!0)):C(t)});var t=S().filter(function(t){return!t.isETSInit});return t.forEach(function(t){t.addEventListener(t.getAttribute(c)||"click",function(e){e.preventDefault(),(t.hasAttribute(u)||t.hasAttribute(f)?H:Y)(t)},!1),t.isETSInit=!0}),S(a).length>0&&!document.isETSEscInit&&(document.addEventListener("keydown",function(t){"Escape"!==t.key&&"Esc"!==t.key||S(a).forEach(function(t){if(t.isToggleActive)return t.hasAttribute(f)?console.warn("You can't use '".concat(a,"' on a radio grouped trigger")):(t.hasAttribute(u)?H:Y)(t)})},!1),document.isETSEscInit=!0),S(i).length>0&&!document.isETSArrInit&&(document.addEventListener("keydown",function(t){var e=document.activeElement;if(-1!==["ArrowUp","ArrowDown","ArrowLeft","ArrowRight","Home","End"].indexOf(t.key)&&e.hasAttribute(o)&&e.hasAttribute(i)){if(!e.hasAttribute(u)&&!e.hasAttribute(f))return console.warn("You can't use '".concat(i,"' on a trigger without '").concat(u,"' or '").concat(f,"'"));t.preventDefault();var r=e.hasAttribute(u)?S("".concat(u,"='").concat(e.getAttribute(u),"'")):S("".concat(f,"='").concat(e.getAttribute(f),"'")),n=e;switch(t.key){case"ArrowUp":case"ArrowLeft":n=r.indexOf(e)>0?r[r.indexOf(e)-1]:r[r.length-1];break;case"ArrowDown":case"ArrowRight":n=r.indexOf(e)<r.length-1?r[r.indexOf(e)+1]:r[0];break;case"Home":n=r[0];break;case"End":n=r[r.length-1]}return n.focus(),n.dispatchEvent(new Event(n.getAttribute(c)||"click"))}},!1),document.isETSArrInit=!0),t};document.addEventListener("DOMContentLoaded",function t(){M(),document.removeEventListener("DOMContentLoaded",t)}),window.initEasyToggleState=M}();