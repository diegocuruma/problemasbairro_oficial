<?php get_header(); ?>
<section class="c-product-highlight--open c-product-highlight">
    <div class="o-wrapper">
        
        <?php
        if (is_page('problemas-identificados')):
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $params = array('post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged);
            query_posts($params);
        endif;
        ?>  
        
        
        <ul class="c-product-highlight__list u-clearfix">
 
           <?php while(have_posts() ) : the_post(); ?>
                <?php
                        global $name_cat;
                        $content = get_the_content();
                        $resumo = substr($content, 0, 150).'...';
                        $repCat = get_the_terms($post->ID, 'category');
                        $condition = get_field('problema');                        
                        if($repCat) {
                            foreach($repCat as $cat) {
                                $name_cat = $cat->name;
                            }
                        }
                    ?>
                    <li class="c-product-card c-product-highlight__item">
                        <a href="<?php the_permalink();?>" title="Ver detalhes" class="c-product-card__header flex flex--wrap">
                            <?php if(has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('capa', array('class' => 'c-product-card__img u-db')); ?>                                    
                            <?php else :?>
                                <img src="<?php echo WP_IMAGES ?>/image-default.jpg" class="c-product-card__img u-db">
                            <?php endif; ?>
                            <h3 class="c-product-card__title u-fwm"><?php the_title(); ?></h3>
                            <span class="c-product-card__date u-fwm"><?php the_time('d/m/Y'); ?></span>
                        </a>
                        <p class="c-product-card__resume"><?php echo $resumo; ?></p>
                        <div class="c-product-card__footer">
                            <strong class="c-product-card__place u-fwm"><?php the_field('bairro');?></strong>
                            <p class="c-product-card__cat u-fwm"><span>Categoria:</span> <em><?php echo $name_cat; ?></em></p>
                            <a href="<?php the_permalink();?>" title="Ver detalhes" class="c-btn--ghost c-btn--small c-btn">Ver detalhes</a>
                        </div>
                    </li>   
                                           
            <?php endwhile; ?>
        </ul>
        
    </div>
        <?php 
            echo wp_bootstrap_pagination();
            wp_reset_query(); 
        ?>
</section>
<?php get_footer(); ?>