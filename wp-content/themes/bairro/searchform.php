<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<form role="search" method="get" class="c-search-form" action="<?php echo esc_url(home_url('/')); ?>">
    <label class="c-search-form__label u-db">
        <strong class="c-search-form__txt u-dn">Buscar por:</strong>
        <input type="search" value="<?php echo get_search_query(); ?>" name="s"  class="c-search-form__input u-fwn" placeholder="Buscar meu bairro"/>
    </label>
    <button type="submit" class="c-search-form__btn c-btn--secondary c-btn--small c-btn">Buscar</button>
</form>
