��          �      ,      �     �     �  .   �  "   �          !      /     P     \  F   i  "   �  /   �               ?  +   V  "  �     �  %   �  0   �  $        ?     X     f     �     �  B   �      �  +        1     Q     m  +   �              	                                      
                          Click to expand/contract Click to login as this user Core was not passed in "Switch_User_Frontend". Current user successfully changed. Invalid user ID. Mário Valney Oops... error: please try again. Switch User Switch User: Switch to another user account quickly. Do not activate in production! The Switch_User was overwritten... There was a connection error, please try again. You are logged as this user You are not allowed to do that. http://mariovalney.com http://projetos.mariovalney.com/switch-user PO-Revision-Date: 2017-05-09 01:47:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: GlotPress/2.4.0-alpha
Language: pt_BR
Project-Id-Version: Plugins - Switch User - Development (trunk)
 Clique para expandir/encolher Clique para entrar como esse usuário Core não foi passado em "Switch_User_Frontend". Usuário atual alterado com sucesso. ID do usuário inválido Mário Valney Opa... erro: tente novamente. Switch User Trocar Usuário: Troque de conta de usuário rapidamente. Não ative em produção! O Switch_User foi sobrescrito... Houve um erro de conexão, tente novamente. Você entrou como esse usuário Você não pode fazer isso. http://mariovalney.com http://projetos.mariovalney.com/switch-user 