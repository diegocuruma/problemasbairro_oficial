# Translation of Plugins - Google Analytics Dashboard for WP (GADWP) - Stable (latest release) in Portuguese (Brazil)
# This file is distributed under the same license as the Plugins - Google Analytics Dashboard for WP (GADWP) - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2017-05-22 13:48:39+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: pt_BR\n"
"Project-Id-Version: Plugins - Google Analytics Dashboard for WP (GADWP) - Stable (latest release)\n"

#. Plugin Name of the plugin/theme
msgid "Google Analytics Dashboard for WP (GADWP)"
msgstr "Google Analytics Dashboard for WP (GADWP)"

#: front/setup.php:118
msgid "Precision: "
msgstr "Precisão:"

#: admin/settings.php:1200 admin/settings.php:1532
msgid "developer mode (requires advanced API knowledge)"
msgstr "modo de desenvolvedor (requer conhecimento avançado de API)"

#: admin/settings.php:1034
msgid "Sessions ratio: %s."
msgstr "Relação de seções: %s."

#: admin/settings.php:1032
msgid "The report was based on %s of sessions."
msgstr "O relatório foi baseado em %s de sessões."

#: admin/settings.php:1030
msgid "Last Detected on %s."
msgstr "Última detecção em %s."

#: admin/settings.php:1023
msgid "Sampled Data"
msgstr "Dados de amostra"

#: admin/settings.php:1016
msgid "Error Details: "
msgstr "Detalhes do erro:"

#: admin/settings.php:1007
msgid "Last Error: "
msgstr "Último erro:"

#: admin/settings.php:841
msgid "Optimize"
msgstr "Otimizar"

#: admin/settings.php:836
msgid "Enhanced Ecommerce Plugin"
msgstr "Plugin de e-commerce melhorado"

#: admin/settings.php:835
msgid "Ecommerce Plugin"
msgstr "Plugin de e-commerce"

#: admin/settings.php:829
msgid "Ecommerce Tracking:"
msgstr "Rastreamento de e-commerce:"

#: admin/settings.php:825
msgid "Ecommerce"
msgstr "E-commerce"

#: admin/settings.php:802
msgid "seconds"
msgstr "segundos"

#: admin/settings.php:682
msgid "enable page scrolling depth variables: "
msgstr "ativar variáveis de profundidade de rolagem da página:"

#: admin/settings.php:671
msgid "Page Scrolling Depth Variables"
msgstr "Variáveis de profundidade de rolagem de página"

#: admin/settings.php:596
msgid "Main Variables"
msgstr "Variáveis principais"

#: admin/settings.php:511
msgid "Custom Dimensions"
msgstr "Dimensões personalizadas"

#: admin/settings.php:473
msgid "track page scrolling depth"
msgstr "rastrear a profundidade de rolagem da página"

#: admin/settings.php:461
msgid "track form submit actions"
msgstr "rastrear ações de envio de formulário"

#: admin/settings.php:437
msgid "track affiliate links"
msgstr "rastrear links de afiliados"

#: admin/settings.php:405 admin/settings.php:502
msgid "HTML Body"
msgstr "Body do HTML"

#: admin/settings.php:404 admin/settings.php:501
msgid "HTML Head"
msgstr "Head do HTML"

#: admin/settings.php:399 admin/settings.php:496
msgid "Code Placement:"
msgstr "Posição do cófigo:"

#: admin/settings.php:373
msgid "Tag Manager"
msgstr "Gerenciador de tags"

#: admin/settings.php:352
msgid "DataLayer Variables"
msgstr "Variáveis DataLayer"

#: admin/settings.php:350 admin/settings.php:352
msgid "Integration"
msgstr "Integração"

#: admin/setup.php:309 front/setup.php:117
msgid "Exit Rate"
msgstr "Taxa de saída"

#: admin/setup.php:301 admin/setup.php:302 admin/setup.php:303
#: admin/setup.php:304 admin/setup.php:305 admin/setup.php:306
#: front/setup.php:109 front/setup.php:110 front/setup.php:111
#: front/setup.php:112 front/setup.php:113 front/setup.php:114
msgid "Future Use"
msgstr "Uso futuro"

#: admin/setup.php:209
msgid "Session Duration"
msgstr "Duração da sessão"

#: admin/setup.php:208 admin/setup.php:308 front/setup.php:116
msgid "Page Load Time"
msgstr "Tempo de carregamento da página"

#: admin/setup.php:207 admin/setup.php:307 front/setup.php:115
msgid "Time on Page"
msgstr "Tempo na página"

#: tools/gapi.php:525
msgid "Source:"
msgstr "Origem:"

#: tools/gapi.php:525
msgid "URI:"
msgstr "URI:"

#: admin/setup.php:178 tools/gapi.php:521
msgid "404 Errors"
msgstr "Erros 404"

#: admin/settings.php:301
msgid "404 Page Title contains:"
msgstr "O título da página 404 contém:"

#: admin/settings.php:297
msgid "404 Errors Report"
msgstr "Relatório de erros 404"

#: admin/settings.php:291
msgid "Maps API Key:"
msgstr "Chave de API do Mapas:"

#: admin/setup.php:192 admin/setup.php:292 front/setup.php:100
msgid "Invalid response"
msgstr "Resposta inválida"

#: admin/settings.php:580 admin/settings.php:660
msgid "Tags:"
msgstr "Tags:"

#: tools/gapi.php:746
msgid "Other"
msgstr "Outros"

#: tools/gapi.php:683
msgid "Devices"
msgstr "Dispositivos"

#: admin/setup.php:200 admin/setup.php:300 front/setup.php:108
msgid "Mobile Brand"
msgstr "Marca do aparelho"

#: admin/setup.php:199 admin/setup.php:299 front/setup.php:107
msgid "Screen Resolution"
msgstr "Resolução da tela"

#: admin/setup.php:198 admin/setup.php:298 front/setup.php:106
msgid "Operating System"
msgstr "Sistemas operacionais"

#: admin/setup.php:197 admin/setup.php:297 front/setup.php:105
msgid "Browser"
msgstr "Navegador"

#: admin/setup.php:177 admin/setup.php:278 front/setup.php:86
msgid "Technology"
msgstr "Tecnologia"

#: admin/setup.php:176 admin/setup.php:277 front/setup.php:85
msgid "Traffic"
msgstr "Tráfego"

#. translators: Example: 'l, F j, Y' will become 'Thusday, November 17, 2015'
#. For details see:
#. http://php.net/manual/en/function.date.php#refsect1-function.date-parameters
#: tools/gapi.php:408 tools/gapi.php:783
msgid "l, F j, Y"
msgstr "l, j \\d\\e F \\d\\e Y"

#. translators: Example: 'F, Y' will become 'November, 2015' For details see:
#. http://php.net/manual/en/function.date.php#refsect1-function.date-parameters
#: tools/gapi.php:399
msgid "F, Y"
msgstr "F \\\\d\\\\e Y"

#: admin/settings.php:751
msgid "Cross-domain Tracking"
msgstr "Rastreamento de Domínio cruzado"

#: admin/settings.php:775
msgid "Cookie Customization"
msgstr "Customização do Cookie"

#: admin/settings.php:797
msgid "Cookie Expires:"
msgstr "Expiração do Cookie"

#: admin/settings.php:788
msgid "Cookie Name:"
msgstr "Nome do Cookie "

#: admin/settings.php:779
msgid "Cookie Domain:"
msgstr "Domínio do Cookie"

#. Author of the plugin/theme
msgid "Alin Marcu"
msgstr "Alin Marcu"

#. Description of the plugin/theme
msgid "Displays Google Analytics Reports and Real-Time Statistics in your Dashboard. Automatically inserts the tracking code in every page of your website."
msgstr "Mostrar Relarórios do Google Analytics e Estatísticas em Tempo Real em seu Painel. Automaticamente insere o código de rastreamento em todas as páginas do seu site."

#. #-#-#-#-#  google-analytics-dashboard-for-wp-code.pot (Google Analytics
#. Dashboard for WP (GADWP) 5.0.0.1)  #-#-#-#-#
#. Plugin URI of the plugin/theme
#. #-#-#-#-#  google-analytics-dashboard-for-wp-code.pot (Google Analytics
#. Dashboard for WP (GADWP) 5.0.0.1)  #-#-#-#-#
#. Author URI of the plugin/theme
msgid "https://deconf.com"
msgstr "https://deconf.com"

#: tools/gapi.php:731
msgid "Type"
msgstr "Tipo"

#: tools/gapi.php:683
msgid "Channels"
msgstr "Canal"

#: tools/gapi.php:625
msgid "Cities from"
msgstr "das Cidades"

#: tools/gapi.php:615
msgid "Countries"
msgstr "Países"

#: tools/gapi.php:373 tools/gapi.php:376 tools/gapi.php:774
msgid "Date"
msgstr "Dia"

#: tools/gapi.php:370
msgid "Hour"
msgstr "Hora"

#: tools/gapi.php:362
msgid "Unique Page Views"
msgstr "Visualização única de página"

#: tools/gapi.php:359
msgid "Organic Searches"
msgstr "Pesquisas Ôrganicas"

#: tools/gapi.php:175
msgid "Save Access Code"
msgstr "Salvar Código de Acesso"

#: tools/gapi.php:162
msgid "Access Code:"
msgstr "Código de acesso:"

#: tools/gapi.php:162 tools/gapi.php:165
msgid "Use the red link to get your access code!"
msgstr "Use o link vermelho para pegar seu código de acesso!"

#: tools/gapi.php:157
msgid "Get Access Code"
msgstr "Obter Código de Acesso"

#: tools/gapi.php:157
msgid "Use this link to get your access code:"
msgstr "Use este link para obter o seu código de acesso:"

#: gadwp.php:55 gadwp.php:63 gadwp.php:70
msgid "This is not allowed, read the documentation!"
msgstr "Isso não é permitido, leia a documentação!"

#: front/widgets.php:179
msgid "Give credits:"
msgstr "Créditos para:"

#: front/widgets.php:172
msgid "Stats for:"
msgstr "Estatísticas para:"

#: front/widgets.php:169
msgid "Anonymize stats:"
msgstr "Anonimizar estatísticas:"

#: front/widgets.php:165
msgid "Totals"
msgstr "Totais"

#: front/widgets.php:164
msgid "Chart"
msgstr "Gráfico"

#: front/widgets.php:163
msgid "Chart & Totals"
msgstr "Gráfico e Totais"

#: front/widgets.php:162
msgid "Display:"
msgstr "Exibir:"

#: front/widgets.php:159
msgid "Title:"
msgstr "Título:"

#: front/widgets.php:151
msgid "Google Analytics Stats"
msgstr "Estatísticas do Google Analytics"

#: front/widgets.php:136
msgid "generated by"
msgstr "gerado por"

#: front/widgets.php:130
msgid "Sessions:"
msgstr "Sessões:"

#: front/widgets.php:130
msgid "Period:"
msgstr "período:"

#: front/widgets.php:21
msgid "Will display your google analytics stats in a widget"
msgstr "Exibirá suas estatísticas google analytics em um Widget"

#: admin/setup.php:202
msgid "KEYWORDS"
msgstr "PALAVRAS-CHAVE"

#: admin/setup.php:201
msgid "REFERRALS"
msgstr "REFERÊNCIAS"

#: admin/setup.php:206
msgid "NEW"
msgstr "NOVO"

#: admin/setup.php:205
msgid "DIRECT"
msgstr "DIRETO"

#: admin/setup.php:204
msgid "CAMPAIGN"
msgstr "CAMPANHA"

#: admin/setup.php:203
msgid "SOCIAL"
msgstr "SOCIAL"

#: admin/setup.php:173 tools/gapi.php:493
msgid "Pages"
msgstr "Páginas"

#: admin/setup.php:167 admin/setup.php:186 front/widgets.php:40
#: tools/gapi.php:365 tools/gapi.php:774
msgid "Sessions"
msgstr "Sessão"

#: admin/setup.php:156
msgid "Real-Time"
msgstr "Tempo Real"

#: admin/widgets.php:56
msgid "Find out more!"
msgstr "Saber mais!"

#: admin/widgets.php:56
msgid "Something went wrong while retrieving property data. You need to create and properly configure a Google Analytics account:"
msgstr "Algo deu errado ao recuperar os dados da propriedade. Você precisa criar e configurar corretamente uma conta do Google Analytics:"

#: admin/widgets.php:43 admin/widgets.php:50
msgid "Select Domain"
msgstr "Selecionar Domínio"

#: admin/widgets.php:43 admin/widgets.php:50
msgid "An admin should asign a default Google Analytics Profile."
msgstr "Um administrador deve atribuir um Perfil Google Analytics padrão."

#: admin/widgets.php:28 front/widgets.php:21
msgid "Google Analytics Dashboard"
msgstr "Painel do Google Analytics"

#: admin/setup.php:362
msgid "the plugin&#39;s settings page"
msgstr "Página de configuração do Plugin"

#: admin/setup.php:362
msgid "the documentation page"
msgstr "Documentação"

#: admin/setup.php:362
msgid "For details, check out %1$s and %2$s."
msgstr "Para mais detalhes, veja %1$s e %2$s."

#: admin/setup.php:362
msgid "Google Analytics Dashboard for WP has been updated to version %s."
msgstr "Google Analytics Dashboard para WP foi atualizado para a versão %s."

#: admin/setup.php:344
msgid "Settings"
msgstr "Configurações"

#: admin/setup.php:196 admin/setup.php:296 admin/widgets.php:35
#: front/setup.php:104
msgid "This plugin needs an authorization:"
msgstr "Este Plugin precisa de uma autorização:"

#: admin/setup.php:195 admin/setup.php:295 front/setup.php:103
msgid "report generated by"
msgstr "Relatório gerado por"

#: admin/setup.php:194 admin/setup.php:294 front/setup.php:102
#: front/widgets.php:107
msgid "This report is unavailable"
msgstr "Este Relatório não está disponível"

#: admin/setup.php:193 admin/setup.php:293 front/setup.php:101
msgid "Not enough data collected"
msgstr "Dados coletados insuficientes"

#: admin/setup.php:191 admin/setup.php:291 front/setup.php:99
msgid "Pages/Session"
msgstr "Página/Sessão"

#: admin/setup.php:190 admin/setup.php:290 front/setup.php:98
msgid "Organic Search"
msgstr "Pesquisa Orgânica"

#: admin/setup.php:184 admin/setup.php:285 front/setup.php:92
msgid "Search Engines"
msgstr "Mecanismos de Busca"

#: admin/setup.php:185 admin/setup.php:284 front/setup.php:93
msgid "Social Networks"
msgstr "Rede Social"

#: admin/setup.php:183 admin/setup.php:283 front/setup.php:91
msgid "Visitor Type"
msgstr "Tipo de Visitantes"

#: admin/setup.php:182 admin/setup.php:282 front/setup.php:90
msgid "Traffic Mediums"
msgstr "Média de Tráfego"

#: admin/setup.php:181 admin/setup.php:281 front/setup.php:89
msgid "A JavaScript Error is blocking plugin resources!"
msgstr "Um JavaScript está bloqueando os recursos do plugin!"

#: admin/setup.php:175 admin/setup.php:276 front/setup.php:84
#: tools/gapi.php:592
msgid "Searches"
msgstr "Pesquisas"

#: admin/setup.php:174 admin/setup.php:275 front/setup.php:83
#: tools/gapi.php:557
msgid "Referrers"
msgstr "Referências"

#: admin/setup.php:172 admin/setup.php:274 front/setup.php:82
msgid "Location"
msgstr "Localização"

#: admin/setup.php:171 admin/setup.php:189 admin/setup.php:273
#: admin/setup.php:289 front/setup.php:81 front/setup.php:97 tools/gapi.php:356
msgid "Bounce Rate"
msgstr "Taxa de Rejeição"

#: admin/setup.php:170 admin/setup.php:188 admin/setup.php:272
#: admin/setup.php:288 front/setup.php:80 front/setup.php:96 tools/gapi.php:353
msgid "Page Views"
msgstr "Visualizações de Páginas"

#: admin/setup.php:169 admin/setup.php:271 front/setup.php:79
msgid "Organic"
msgstr "Orgânica"

#: admin/setup.php:168 admin/setup.php:187 admin/setup.php:270
#: admin/setup.php:287 front/setup.php:78 front/setup.php:95 tools/gapi.php:350
msgid "Users"
msgstr "Usuários"

#: admin/setup.php:269 admin/setup.php:286 front/setup.php:77
#: front/setup.php:94
msgid "Unique Views"
msgstr "Visão Única"

#: admin/setup.php:164 admin/setup.php:266 front/setup.php:74
msgid "Three"
msgstr "Três"

#: admin/setup.php:163 admin/setup.php:265 front/setup.php:73
msgid "One"
msgstr "Um"

#: admin/setup.php:163 admin/setup.php:164 admin/setup.php:265
#: admin/setup.php:266 front/setup.php:73 front/setup.php:74
msgid "%s Year"
msgid_plural "%s Years"
msgstr[0] "%s Ano"
msgstr[1] "%s Anos"

#: admin/setup.php:159 admin/setup.php:160 admin/setup.php:161
#: admin/setup.php:162 admin/setup.php:261 admin/setup.php:262
#: admin/setup.php:263 admin/setup.php:264 front/setup.php:69
#: front/setup.php:70 front/setup.php:71 front/setup.php:72
#: front/widgets.php:68 front/widgets.php:71 front/widgets.php:74
#: front/widgets.php:173 front/widgets.php:174 front/widgets.php:175
msgid "Last %d Days"
msgstr "Últimos %d dias"

#: admin/setup.php:158 admin/setup.php:260 front/setup.php:68
msgid "Yesterday"
msgstr "Ontem"

#: admin/setup.php:157 admin/setup.php:259 front/setup.php:67
msgid "Today"
msgstr "Hoje"

#: admin/setup.php:46
msgid "Tracking Code"
msgstr "Código de Rastreio"

#: admin/setup.php:45
msgid "Frontend Settings"
msgstr "Configurações Externas"

#: admin/setup.php:44
msgid "Backend Settings"
msgstr "Configurações Internas"

#: admin/setup.php:42 admin/setup.php:58
msgid "Google Analytics"
msgstr "Google Analytics"

#: admin/settings.php:1737
msgid "Web Analytics"
msgstr "Web Analytics"

#: admin/settings.php:1737
msgid "%s service with users tracking at IP level."
msgstr "%s serviços com rastreamento de usuário a nível de ip."

#: admin/settings.php:1751
msgid "WordPress Plugins"
msgstr "Plugin do Wordpress"

#: admin/settings.php:1751
msgid "Other %s written by the same author"
msgstr "Outros %s escritos pelo mesmo autor"

#: admin/settings.php:1744
msgid "Improve search rankings"
msgstr "Melhorar classificação em buscas"

#: admin/settings.php:1744
msgid "%s by moving your website to HTTPS/SSL."
msgstr "%s movendo seu site para HTTPS/SSL."

#: admin/settings.php:1730
msgid "Further Reading"
msgstr "Leitura adicional"

#: admin/settings.php:1724
msgid "rate this plugin"
msgstr "Avaliar este Plugin"

#: admin/settings.php:1724
msgid "Your feedback and review are both important, %s!"
msgstr "Seu Feedback e Review são importantes, %s!"

#: admin/settings.php:1699
msgid "Follow & Review"
msgstr "Siga e Review"

#: admin/settings.php:1691
msgid "Setup Tutorial & Demo"
msgstr "Tutorial de Instalação e demonstrações"

#: admin/settings.php:1635
msgid "exclude Super Admin tracking for the entire network"
msgstr "Excluir rastreamento do Super Administrador para toda a rede"

#: admin/settings.php:1573
msgid "Properties/Views Settings"
msgstr "Propriedades/Ver Definições"

#: admin/settings.php:1563
msgid "Refresh Properties"
msgstr "Atualizar Propriedades"

#: admin/settings.php:1512
msgid "use a single Google Analytics account for the entire network"
msgstr "Use uma única conta do Google Analytics para toda a Rede"

#: admin/settings.php:1500
msgid "Network Setup"
msgstr "Configuração de Rede"

#: admin/settings.php:1414
msgid "Properties refreshed."
msgstr "Propriedades Atualizadas"

#: admin/settings.php:1320 admin/settings.php:1656 admin/widgets.php:35
msgid "Authorize Plugin"
msgstr "Autorize o Plugin"

#: admin/settings.php:1298 admin/settings.php:1619
msgid "automatic updates for minor versions (security and maintenance releases only)"
msgstr "atualizações automáticas para versões menores (Só correções de Segurança e de Manutenção)"

#: admin/settings.php:1287 admin/settings.php:1607
msgid "Automatic Updates"
msgstr "Atualizações Automáticas"

#: admin/settings.php:1274
msgid "Theme Color:"
msgstr "Cor do Tema:"

#: admin/settings.php:1259
msgid "Lock Selection"
msgstr "Travar Seleção"

#: admin/settings.php:1255 admin/settings.php:1597
msgid "Property not found"
msgstr "Propriedade não econtrada"

#: admin/settings.php:1242
msgid "Select View:"
msgstr "Selecionar Exibição"

#: admin/settings.php:1238 admin/setup.php:43 admin/setup.php:59
msgid "General Settings"
msgstr "Configurações Gerais"

#: admin/settings.php:1229
msgid "Reset Errors"
msgstr "Reiniciar Erros"

#: admin/settings.php:1228 admin/settings.php:1321 admin/settings.php:1562
#: admin/settings.php:1657
msgid "Clear Cache"
msgstr "Limpar Cache"

#: admin/settings.php:1227 admin/settings.php:1561
msgid "Clear Authorization"
msgstr "Limpar Autorização"

#: admin/settings.php:1215 admin/settings.php:1548
msgid "Client Secret:"
msgstr "Cliente Secreto:"

#: admin/settings.php:1207 admin/settings.php:1539
msgid "Client ID:"
msgstr "ID do cliente:"

#: admin/settings.php:1194 admin/settings.php:1526
msgid "tutorial"
msgstr "tutorial"

#: admin/settings.php:1194 admin/settings.php:1526
msgid "video"
msgstr "vídeo"

#: admin/settings.php:1194 admin/settings.php:1526
msgid "You should watch the %1$s and read this %2$s before proceeding to authorization. This plugin requires a properly configured Google Analytics account!"
msgstr "Você deveria assistir %1$s e ler isso %2$s antes de prosseguir com a autorização. Este plugin requer uma conta no Google Analytics corretamente configurada!"

#: admin/settings.php:1189 admin/settings.php:1522
msgid "Plugin Authorization"
msgstr "Autorização do Plugin"

#: admin/settings.php:1177 admin/settings.php:1488
msgid "Use the red link (see below) to generate and get your access code!"
msgstr "Use o link vermelho (abaixo) para gerar e pegar seu código de acesso."

#: admin/settings.php:1162 admin/settings.php:1473
msgid "Google Analytics Settings"
msgstr "Configurações do Google Analytics"

#: admin/settings.php:1152 admin/settings.php:1463
msgid "All other domains/properties were removed."
msgstr "Todos os outros domínios/propriedades foram removidas."

#: admin/settings.php:1139
msgid "All errors reseted."
msgstr "Todos erros reiniciados"

#: admin/settings.php:1129 admin/settings.php:1449
msgid "Token Reseted and Revoked."
msgstr "Token redefinido e revogado"

#: admin/settings.php:1120 admin/settings.php:1440
msgid "Cleared Cache."
msgstr "Cache Limpo."

#: admin/settings.php:1114 admin/settings.php:1408
msgid "The access code is <strong>NOT</strong> your <strong>Tracking ID</strong> (UA-XXXXX-X). Try again, and use the red link to get your access code"
msgstr "O código de acesso <strong> NÃO É</ strong> o <strong> Acompanhamento de ID </ strong> (UA-XXXXX-X). Tente de novo, e use o link vermelho para obter o seu código de acesso"

#: admin/settings.php:1082 admin/settings.php:1369
msgid "Plugin authorization succeeded."
msgstr "Plugin autorizado com sucesso."

#: admin/settings.php:1069
msgid "Library conflicts between WordPress plugins"
msgstr "Conflito de Bibliotecas entre Plugins do Wordpress"

#: admin/settings.php:1069 admin/settings.php:1354
msgid "Loading the required libraries. If this results in a blank screen or a fatal error, try this solution:"
msgstr "Carregando as bibliotecas necessárias. Se isso resulta em uma tela em branco ou um erro fatal, tente esta solução:"

#: admin/settings.php:1045
msgid "Plugin Configuration"
msgstr "Configuração do Plugin"

#: admin/settings.php:1012
msgid "Error Details"
msgstr "Detalhes do Erro"

#: admin/settings.php:1006 admin/settings.php:1016 admin/settings.php:1036
msgid "None"
msgstr "Nenhum"

#: admin/settings.php:1001
msgid "Last Error detected"
msgstr "Último erro detectado"

#: admin/settings.php:995
msgid "Plugin Settings"
msgstr "Configurações do Plugin"

#: admin/settings.php:995
msgid "Errors & Details"
msgstr "Erros & Detalhes"

#: admin/settings.php:987
msgid "Google Analytics Errors & Debugging"
msgstr "Erros & Depuração do Google Analytics"

#: admin/settings.php:913
msgid "Exclude tracking for:"
msgstr "Excluir Rastreio para:"

#: admin/settings.php:767
msgid "Cross Domains:"
msgstr "Domínio Cruzado:"

#: admin/settings.php:762
msgid "enable cross domain tracking"
msgstr "habilitar Rastreio de domínio cruzado"

#: admin/settings.php:747
msgid "enable enhanced link attribution"
msgstr "permitir atribuição de link aprimorada"

#: admin/settings.php:735
msgid "exclude events from bounce-rate calculation"
msgstr "Excluir eventos de cálculo de taxa de rejeição"

#: admin/settings.php:694
msgid "Page Speed SR:"
msgstr "Velocidade de Página SR:"

#: admin/settings.php:690
msgid "Advanced Tracking"
msgstr "Rastreio Avançado"

#: admin/settings.php:567 admin/settings.php:648
msgid "User Type:"
msgstr "Tipo de Usuário:"

#: admin/settings.php:554 admin/settings.php:636
msgid "Categories:"
msgstr "Categorias:"

#: admin/settings.php:528 admin/settings.php:612
msgid "Publication Year:"
msgstr "Ano de Publicação:"

#: admin/settings.php:515 admin/settings.php:600
msgid "Authors:"
msgstr "Autores:"

#: admin/settings.php:449
msgid "track fragment identifiers, hashmarks (#) in URI links"
msgstr "rastrear identificadores de fragmento, hash (#) nos links"

#: admin/settings.php:487
msgid "Affiliates Regex:"
msgstr "Expressão Regular Afiliada:"

#: admin/settings.php:478
msgid "Downloads Regex:"
msgstr "Expressões Regulares de Download:"

#: admin/settings.php:723
msgid "enable remarketing, demographics and interests reports"
msgstr "permitir recolocação, demográficas e relatórios de interesses"

#: admin/settings.php:711
msgid "anonymize IPs while tracking"
msgstr "IPs anônimos ao rastreamento"

#: admin/settings.php:367
msgid "Tracking Type:"
msgstr "Tipo de Rastreio"

#: admin/settings.php:383 admin/settings.php:1268
msgid "Time Zone:"
msgstr "Fuso horário:"

#: admin/settings.php:383 admin/settings.php:1268
msgid "Default URL:"
msgstr "URL Padrão:"

#: admin/settings.php:383 admin/settings.php:1268
msgid "Tracking ID:"
msgstr "ID de rastreamento:"

#: admin/settings.php:383 admin/settings.php:1249 admin/settings.php:1268
#: admin/settings.php:1590
msgid "View Name:"
msgstr "Ver Nome:"

#: admin/settings.php:374 admin/settings.php:606 admin/settings.php:618
#: admin/settings.php:630 admin/settings.php:642 admin/settings.php:654
#: admin/settings.php:666 admin/settings.php:834
msgid "Disabled"
msgstr "Desabilitar"

#: admin/settings.php:363
msgid "Tracking Settings"
msgstr "Configurações de rastreamento"

#: admin/settings.php:350
msgid "Advanced Settings"
msgstr "Configurações avançadas"

#: admin/settings.php:350 admin/settings.php:352 admin/settings.php:909
#: admin/settings.php:1624
msgid "Exclude Tracking"
msgstr "Excluir Rastreamento"

#: admin/settings.php:350
msgid "Custom Definitions"
msgstr "Definições personalizadas"

#: admin/settings.php:350 admin/settings.php:414
msgid "Events Tracking"
msgstr "Eventos de rastreamento"

#: admin/settings.php:350 admin/settings.php:352 admin/settings.php:354
msgid "Basic Settings"
msgstr "Configurações básicas"

#: admin/settings.php:342
msgid "Google Analytics Tracking Code"
msgstr "Código de rastreamento do Google Analytics"

#: admin/settings.php:285
msgid "Target Geo Map to country:"
msgstr "Mapa Geográfico alvo para o país:"

#: admin/settings.php:281
msgid "Location Settings"
msgstr "Configurações de Localização"

#: admin/settings.php:275
msgid "Maximum number of pages to display on real-time tab:"
msgstr "Número máximo de páginas para mostrar na guia Tempo Real:"

#: admin/settings.php:272
msgid "Real-Time Settings"
msgstr "Definições de Tempo Real"

#: admin/settings.php:267
msgid "enable the main Dashboard Widget"
msgstr "habilitar Widget no Painel principal"

#: admin/settings.php:255
msgid "enable reports on Posts List and Pages List"
msgstr "habilitar relatórios nas listas de Posts e Páginas"

#: admin/settings.php:243
msgid "enable Switch View functionality"
msgstr "habilitar funcionalidade: Trocar exibição"

#: admin/settings.php:193
msgid "Google Analytics Backend Settings"
msgstr "Configurações de back-end do Google Analytics"

#: admin/settings.php:164 admin/settings.php:312 admin/settings.php:949
#: admin/settings.php:1309 admin/settings.php:1645
msgid "Save Changes"
msgstr "Salvar Mudanças"

#: admin/settings.php:154
msgid "enable web page reports on frontend"
msgstr "habilitar relaórios na frente do site"

#: admin/settings.php:118 admin/settings.php:207
msgid "Show stats to:"
msgstr "Mostrar Status de:"

#: admin/settings.php:114 admin/settings.php:203
msgid "Permissions"
msgstr "Permissões"

#: admin/settings.php:104
msgid "Google Analytics Frontend Settings"
msgstr "Configurações Externas do Google Analytics"

#: admin/settings.php:99 admin/settings.php:188 admin/settings.php:337
#: admin/settings.php:983 admin/settings.php:1171 admin/settings.php:1482
#: admin/setup.php:196 admin/setup.php:296 front/setup.php:104
msgid "authorize the plugin"
msgstr "Autorize o Plugin"

#: admin/settings.php:99 admin/settings.php:188 admin/settings.php:337
#: admin/settings.php:983 admin/settings.php:1171 admin/settings.php:1482
#: admin/setup.php:47 admin/setup.php:60
msgid "Errors & Debug"
msgstr "Erros & Depuração"

#: admin/settings.php:99 admin/settings.php:188 admin/settings.php:337
#: admin/settings.php:983 admin/settings.php:1171 admin/settings.php:1482
msgid "Something went wrong, check %1$s or %2$s."
msgstr "Algo deu errado, verifique %1$s ou %2$s."

#: admin/settings.php:95 admin/settings.php:184 admin/settings.php:333
#: admin/settings.php:1122 admin/settings.php:1132 admin/settings.php:1141
#: admin/settings.php:1147 admin/settings.php:1157 admin/settings.php:1434
#: admin/settings.php:1442 admin/settings.php:1452 admin/settings.php:1458
#: admin/settings.php:1468
msgid "Cheating Huh?"
msgstr "A Curiosidade Matou o Gato :F"

#: admin/settings.php:93 admin/settings.php:182 admin/settings.php:331
#: admin/settings.php:1145 admin/settings.php:1456
msgid "Settings saved."
msgstr "Definições Salvas"

#: admin/item-reports.php:53 admin/settings.php:372 front/item-reports.php:30
msgid "Analytics"
msgstr "Analytics"