-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 01-Ago-2019 às 09:09
-- Versão do servidor: 5.6.43
-- versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `omeubair_bd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_aiowps_events`
--

CREATE TABLE `bai_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_aiowps_failed_logins`
--

CREATE TABLE `bai_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_aiowps_global_meta`
--

CREATE TABLE `bai_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_aiowps_login_activity`
--

CREATE TABLE `bai_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_aiowps_login_activity`
--

INSERT INTO `bai_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'diegocurumim', '2019-02-28 16:07:10', '2019-02-28 16:12:56', '186.213.47.108', '', ''),
(2, 1, 'diegocurumim', '2019-03-01 08:20:11', '0000-00-00 00:00:00', '186.213.47.108', '', ''),
(3, 1, 'diegocurumim', '2019-03-01 09:42:36', '0000-00-00 00:00:00', '186.213.47.108', '', ''),
(4, 1, 'diegocurumim', '2019-03-02 22:32:25', '0000-00-00 00:00:00', '177.42.172.126', '', ''),
(5, 1, 'diegocurumim', '2019-03-03 13:17:42', '0000-00-00 00:00:00', '187.114.120.41', '', ''),
(6, 1, 'diegocurumim', '2019-03-07 09:27:57', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(7, 1, 'diegocurumim', '2019-03-07 11:43:45', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(8, 1, 'diegocurumim', '2019-03-07 12:13:52', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(9, 1, 'diegocurumim', '2019-03-07 13:04:25', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(10, 1, 'diegocurumim', '2019-03-07 15:10:46', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(11, 1, 'diegocurumim', '2019-03-07 15:23:58', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(12, 1, 'diegocurumim', '2019-03-08 08:52:00', '0000-00-00 00:00:00', '179.180.128.173', '', ''),
(13, 1, 'diegocurumim', '2019-03-09 21:45:22', '0000-00-00 00:00:00', '177.207.69.253', '', ''),
(14, 1, 'diegocurumim', '2019-03-10 21:41:58', '0000-00-00 00:00:00', '179.183.155.132', '', ''),
(15, 1, 'diegocurumim', '2019-03-11 13:51:58', '0000-00-00 00:00:00', '179.180.139.31', '', ''),
(16, 1, 'diegocurumim', '2019-03-12 08:53:02', '0000-00-00 00:00:00', '177.134.211.16', '', ''),
(17, 1, 'diegocurumim', '2019-03-12 13:52:15', '0000-00-00 00:00:00', '177.134.211.16', '', ''),
(18, 1, 'diegocurumim', '2019-03-12 23:33:13', '0000-00-00 00:00:00', '187.18.220.48', '', ''),
(19, 1, 'diegocurumim', '2019-03-15 08:39:40', '0000-00-00 00:00:00', '179.176.19.34', '', ''),
(20, 1, 'diegocurumim', '2019-03-17 12:33:13', '0000-00-00 00:00:00', '177.207.145.191', '', ''),
(21, 1, 'diegocurumim', '2019-03-18 11:37:02', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(22, 1, 'diegocurumim', '2019-03-19 13:20:34', '0000-00-00 00:00:00', '186.213.20.100', '', ''),
(23, 1, 'diegocurumim', '2019-03-22 14:11:19', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(24, 1, 'diegocurumim', '2019-04-02 09:10:55', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(25, 1, 'diegocurumim', '2019-04-08 11:26:54', '0000-00-00 00:00:00', '179.152.244.10', '', ''),
(26, 1, 'diegocurumim', '2019-05-06 09:58:58', '0000-00-00 00:00:00', '179.156.169.11', '', ''),
(27, 1, 'diegocurumim', '2019-06-10 13:43:05', '0000-00-00 00:00:00', '181.221.192.227', '', ''),
(28, 1, 'diegocurumim', '2019-07-16 14:19:44', '0000-00-00 00:00:00', '181.221.192.227', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_aiowps_login_lockdown`
--

CREATE TABLE `bai_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_aiowps_permanent_block`
--

CREATE TABLE `bai_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_commentmeta`
--

CREATE TABLE `bai_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_comments`
--

CREATE TABLE `bai_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_links`
--

CREATE TABLE `bai_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_options`
--

CREATE TABLE `bai_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_options`
--

INSERT INTO `bai_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://omeubairro.online', 'yes'),
(2, 'home', 'http://omeubairro.online', 'yes'),
(3, 'blogname', 'O meu bairro', 'yes'),
(4, 'blogdescription', 'Um portal de denúncias onde pessoas de forma colaborativa ajudam a identificar e resolver problemas de saneamento básico, lixo e asfalto', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'diegocuruma@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:18:{i:0;s:37:\"acf-options-page/acf-options-page.php\";i:1;s:29:\"acf-repeater/acf-repeater.php\";i:2;s:25:\"add-to-any/add-to-any.php\";i:3;s:30:\"advanced-custom-fields/acf.php\";i:4;s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";i:5;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:6;s:33:\"classic-editor/classic-editor.php\";i:7;s:51:\"codepress-admin-columns/codepress-admin-columns.php\";i:8;s:32:\"disqus-comment-system/disqus.php\";i:9;s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";i:10;s:43:\"insert-post-external/InsertPostExternal.php\";i:11;s:59:\"intuitive-custom-post-order/intuitive-custom-post-order.php\";i:12;s:23:\"lazy-load/lazy-load.php\";i:13;s:24:\"modulo-banner/banner.php\";i:14;s:55:\"resize-image-after-upload/resize-image-after-upload.php\";i:15;s:41:\"wordpress-importer/wordpress-importer.php\";i:16;s:35:\"wp-fastest-cache/wpFastestCache.php\";i:17;s:25:\"wp-toolkit/wp-toolkit.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'bairro', 'yes'),
(41, 'stylesheet', 'bairro', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:4:{s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";a:2:{i:0;s:15:\"GADWP_Uninstall\";i:1;s:9:\"uninstall\";}s:59:\"intuitive-custom-post-order/intuitive-custom-post-order.php\";s:15:\"hicpo_uninstall\";s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}s:29:\"webp-express/webp-express.php\";s:22:\"webp_express_uninstall\";}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'bai_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:64:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"manage_admin_columns\";b:1;s:20:\"wpseo_manage_options\";b:1;s:16:\"aiosp_manage_seo\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'pt_BR', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:4:\"home\";a:4:{i:0;s:11:\"bigbanner-2\";i:1;s:11:\"problemas-2\";i:2;s:22:\"problemas_resolvidos-2\";i:3;s:14:\"banco_ideias-2\";}s:12:\"sidebar-blog\";a:2:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'cron', 'a:10:{i:1564664031;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1564664248;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1564682475;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1564683752;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564685848;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564687718;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564746803;a:1:{s:18:\"wpseo_onpage_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1564746804;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564749191;a:1:{s:31:\"aiosp_sitemap_daily_update_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(124, 'can_compress_scripts', '1', 'no'),
(135, 'theme_mods_twentyseventeen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1541787799;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(136, 'current_theme', 'Site Problemas do meu bairro', 'yes'),
(137, 'theme_mods_bairro', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:4:\"menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(138, 'theme_switched', '', 'yes'),
(139, 'widget_about', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(140, 'widget_searchform', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(143, 'recently_activated', 'a:3:{s:19:\"wp-webp/wp-webp.php\";i:1551441095;s:29:\"webp-express/webp-express.php\";i:1551440900;s:36:\"contact-form-7/wp-contact-form-7.php\";i:1551440503;}', 'yes'),
(145, 'gadwp_options', '{\"client_id\":\"\",\"client_secret\":\"\",\"access_front\":[\"administrator\"],\"access_back\":[\"administrator\"],\"tableid_jail\":\"190464053\",\"theme_color\":\"#ffba19\",\"switch_profile\":0,\"tracking_type\":\"universal\",\"ga_anonymize_ip\":0,\"user_api\":0,\"ga_event_tracking\":0,\"ga_event_downloads\":\"zip|mp3*|mpe*g|pdf|docx*|pptx*|xlsx*|rar*\",\"track_exclude\":[],\"ga_target_geomap\":\"\",\"ga_realtime_pages\":10,\"token\":\"{\\\"access_token\\\":\\\"ya29.Gl1HB6QUC1keEoHuaW3R-7JZ_6XJzW74R-qhFmnYBAefnDMFGMyWa6tr2RWMVohc_6GMDML6o_oKkF7iLMnNyddq_AAYDrdZXp9iMNA1YTHuEL6Fr3J1pVfpYgSdcm8\\\",\\\"expires_in\\\":3600,\\\"refresh_token\\\":\\\"1\\\\\\/POpEbsL6BfIRjISpLcU6xkUGBfJyZ21MlBWF4nqj7LE\\\",\\\"scope\\\":\\\"https:\\\\\\/\\\\\\/www.googleapis.com\\\\\\/auth\\\\\\/analytics.readonly\\\",\\\"token_type\\\":\\\"Bearer\\\",\\\"created\\\":1563297598}\",\"ga_profiles_list\":[[\"Dados brutos\",\"96560448\",\"UA-58725031-1\",\"http:\\/\\/radiopontoalternativo.com.br\",-10800,\"America\\/Fortaleza\",null],[\"Vista de teste\",\"153761684\",\"UA-58725031-1\",\"http:\\/\\/radiopontoalternativo.com.br\",-28800,\"America\\/Los_Angeles\",null],[\"Vista principal\",\"153762997\",\"UA-58725031-1\",\"http:\\/\\/radiopontoalternativo.com.br\",-28800,\"America\\/Los_Angeles\",null],[\"Todos os dados do website\",\"114920818\",\"UA-72460180-1\",\"http:\\/\\/diegocuruma.github.io\\/curukit\",-10800,\"America\\/Fortaleza\",null],[\"Todos os dados do website\",\"114922427\",\"UA-72460180-2\",\"http:\\/\\/www.curuwork.pe.hu\",-10800,\"America\\/Fortaleza\",null],[\"Todos os dados do website\",\"133128325\",\"UA-86861296-1\",\"http:\\/\\/devunderground.com.br\",-10800,\"America\\/Fortaleza\",null],[\"Todos os dados do website\",\"162737563\",\"UA-108251609-1\",\"https:\\/\\/lacula.com.br\\/\",-10800,\"America\\/Fortaleza\",null],[\"Todos os dados do website\",\"190464053\",\"UA-135021161-1\",\"http:\\/\\/omeubairro.online\",-10800,\"America\\/Fortaleza\",null]],\"ga_enhanced_links\":0,\"ga_remarketing\":0,\"network_mode\":0,\"ga_speed_samplerate\":1,\"ga_user_samplerate\":100,\"ga_event_bouncerate\":0,\"ga_crossdomain_tracking\":0,\"ga_crossdomain_list\":\"\",\"ga_author_dimindex\":0,\"ga_category_dimindex\":0,\"ga_tag_dimindex\":0,\"ga_user_dimindex\":0,\"ga_pubyear_dimindex\":0,\"ga_pubyearmonth_dimindex\":0,\"ga_aff_tracking\":0,\"ga_event_affiliates\":\"\\/out\\/\",\"automatic_updates_minorversion\":\"1\",\"backend_item_reports\":1,\"backend_realtime_report\":0,\"frontend_item_reports\":0,\"dashboard_widget\":1,\"api_backoff\":0,\"ga_cookiedomain\":\"\",\"ga_cookiename\":\"\",\"ga_cookieexpires\":\"\",\"pagetitle_404\":\"Page Not Found\",\"maps_api_key\":\"\",\"tm_author_var\":0,\"tm_category_var\":0,\"tm_tag_var\":0,\"tm_user_var\":0,\"tm_pubyear_var\":0,\"tm_pubyearmonth_var\":0,\"web_containerid\":\"\",\"amp_containerid\":\"\",\"amp_tracking_tagmanager\":0,\"amp_tracking_analytics\":0,\"amp_tracking_clientidapi\":0,\"trackingcode_infooter\":0,\"trackingevents_infooter\":0,\"ecommerce_mode\":\"disabled\",\"ga_formsubmit_tracking\":0,\"optimize_tracking\":0,\"optimize_containerid\":\"\",\"optimize_pagehiding\":0,\"superadmin_tracking\":0,\"ga_pagescrolldepth_tracking\":0,\"tm_pagescrolldepth_tracking\":0,\"ga_event_precision\":0,\"ga_force_ssl\":0,\"with_endpoint\":1,\"ga_optout\":0,\"ga_dnt_optout\":0,\"tm_optout\":0,\"tm_dnt_optout\":0,\"ga_with_gtag\":0,\"usage_tracking\":0,\"hide_am_notices\":0,\"network_hide_am_notices\":0,\"ga_enhanced_excludesa\":0,\"ga_hash_tracking\":0,\"gadwp_hidden\":\"Y\"}', 'yes'),
(146, 'hicpo_ver', '3.1.1', 'yes'),
(147, 'jr_resizeupload_version', '1.8.5', 'yes'),
(148, 'jr_resizeupload_width', '330', 'yes'),
(149, 'jr_resizeupload_height', '220', 'yes'),
(150, 'jr_resizeupload_quality', '90', 'yes'),
(151, 'jr_resizeupload_resize_yesno', 'yes', 'yes'),
(152, 'jr_resizeupload_recompress_yesno', 'no', 'yes'),
(153, 'jr_resizeupload_convertbmp_yesno', 'no', 'yes'),
(154, 'jr_resizeupload_convertpng_yesno', 'no', 'yes'),
(155, 'jr_resizeupload_convertgif_yesno', 'no', 'yes'),
(156, 'widget_gadwp-frontwidget-report', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(157, 'widget_bigbanner', 'a:2:{i:2;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(158, 'acf_version', '4.4.12', 'yes'),
(159, '_amn_exact-metrics_last_checked', '1563235200', 'yes'),
(160, 'exactmetrics_tracking_notice', '1', 'yes'),
(161, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(168, 'options_facebook', '', 'no'),
(169, '_options_facebook', 'field_581cda55a83c1', 'no'),
(170, 'options_instagram', '', 'no'),
(171, '_options_instagram', 'field_581cda60a83c2', 'no'),
(172, 'options_informacoes_do_cabecalho', '0', 'no'),
(173, '_options_informacoes_do_cabecalho', 'field_5857e73c7caaf', 'no'),
(174, 'options_logo', '734', 'no'),
(175, '_options_logo', 'field_5804d84419004', 'no'),
(176, 'options_logo_min', '', 'no'),
(177, '_options_logo_min', 'field_582f4469974f4', 'no'),
(178, 'options_parallax', '', 'no'),
(179, '_options_parallax', 'field_582f56ef85cd5', 'no'),
(180, 'options_link_do_parallax', '', 'no'),
(181, '_options_link_do_parallax', 'field_5832fb55e5a00', 'no'),
(182, 'options_titulo_parallax', '', 'no'),
(183, '_options_titulo_parallax', 'field_582f58e379a39', 'no'),
(184, 'options_texto_de_descricao', '', 'no'),
(185, '_options_texto_de_descricao', 'field_582f591879a3a', 'no'),
(186, 'options_texto_principal', '', 'no'),
(187, '_options_texto_principal', 'field_5832e9390d0bb', 'no'),
(188, 'options_foto_destaque_de_pagina_interna', '17', 'no'),
(189, '_options_foto_destaque_de_pagina_interna', 'field_56d739f522592', 'no'),
(190, 'options_iframe', '', 'no'),
(191, '_options_iframe', 'field_585a8623912d9', 'no'),
(192, 'options_endereço', '0', 'no'),
(193, '_options_endereço', 'field_58334a1efc0ca', 'no'),
(194, 'options_link_do_google_maps', '', 'no'),
(195, '_options_link_do_google_maps', 'field_5804d83419003', 'no'),
(205, 'widget_banco_ideias', 'a:2:{i:2;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(206, 'widget_categorias', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(207, 'widget_problemas', 'a:2:{i:2;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(208, 'widget_problemas_resolvidos', 'a:2:{i:2;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(252, 'disqus_sync_token', '4254ab43bf298438e2ec2417cf3069c3', 'yes'),
(253, 'disqus_forum_url', 'problemasdobairro', 'yes'),
(254, 'disqus_public_key', '', 'yes'),
(255, 'disqus_secret_key', '', 'yes'),
(256, 'disqus_admin_access_token', '', 'yes'),
(337, 'categoria_children', 'a:0:{}', 'yes'),
(371, 'options_desenvolvedores_0_imagem', '720', 'no'),
(372, '_options_desenvolvedores_0_imagem', 'field_5c740a7dabe75', 'no'),
(373, 'options_desenvolvedores', '5', 'no'),
(374, '_options_desenvolvedores', 'field_5c740a70abe74', 'no'),
(375, 'options_desenvolvedores_1_imagem', '698', 'no'),
(376, '_options_desenvolvedores_1_imagem', 'field_5c740a7dabe75', 'no'),
(377, 'options_desenvolvedores_2_imagem', '699', 'no'),
(378, '_options_desenvolvedores_2_imagem', 'field_5c740a7dabe75', 'no'),
(379, 'options_apoio_0_imagem', '700', 'no'),
(380, '_options_apoio_0_imagem', 'field_5c740d8b97882', 'no'),
(383, 'options_apoio', '2', 'no'),
(384, '_options_apoio', 'field_5c740d8b97881', 'no'),
(394, 'new_admin_email', 'diegocuruma@gmail.com', 'yes'),
(411, 'gadwp_redeemed_code', '4/_AAHcoHOSESSxakP0mR7KJIx6HO2mefQkBmkcJgIomKj1j-X7_FYWNg', 'yes'),
(510, 'aiowpsec_db_version', '1.9', 'yes'),
(511, 'aio_wp_security_configs', 'a:88:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:0:\"\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:1:\"1\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";i:5;s:24:\"aiowps_retry_time_period\";i:5;s:26:\"aiowps_lockout_time_length\";i:60;s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:1:\"1\";s:20:\"aiowps_email_address\";s:21:\"diegocuruma@gmail.com\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"yxvnkx6l1zc0wao3u20w\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_custom_login_captcha\";s:0:\"\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"7j7vbsqls83fo3rcdho1\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:0:\"\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:21:\"diegocuruma@gmail.com\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:1:\"1\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:0:\"\";s:26:\"aiowps_disable_index_views\";s:1:\"1\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:0:\"\";s:34:\"aiowps_advanced_char_string_filter\";s:1:\"1\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:1:\"1\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:0:\"\";s:29:\"aiowps_enable_comment_captcha\";s:1:\"1\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:33:\"aiowps_enable_bp_register_captcha\";s:0:\"\";s:35:\"aiowps_enable_bbp_new_topic_captcha\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:21:\"diegocuruma@gmail.com\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:1:\"1\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:1:\"1\";s:42:\"aiowps_disallow_unauthorized_rest_requests\";s:0:\"\";s:25:\"aiowps_ip_retrieve_method\";s:1:\"0\";s:22:\"aiowps_login_page_slug\";s:7:\"sistema\";}', 'yes'),
(529, 'category_children', 'a:0:{}', 'yes'),
(567, 'ac_version', '3.4.1', 'no'),
(571, 'cpac_options_post', 'a:8:{s:5:\"title\";a:6:{s:4:\"type\";s:5:\"title\";s:5:\"label\";s:7:\"Título\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:5:\"title\";s:10:\"label_type\";N;}s:6:\"author\";a:6:{s:4:\"type\";s:6:\"author\";s:5:\"label\";s:5:\"Autor\";s:5:\"width\";i:10;s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:6:\"author\";s:10:\"label_type\";N;}s:10:\"categories\";a:6:{s:4:\"type\";s:10:\"categories\";s:5:\"label\";s:10:\"Categorias\";s:5:\"width\";i:15;s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:10:\"categories\";s:10:\"label_type\";N;}s:4:\"tags\";a:6:{s:4:\"type\";s:4:\"tags\";s:5:\"label\";s:4:\"Tags\";s:5:\"width\";i:15;s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:4:\"tags\";s:10:\"label_type\";N;}s:8:\"comments\";a:6:{s:4:\"type\";s:8:\"comments\";s:5:\"label\";s:119:\"<span class=\"vers comment-grey-bubble\" title=\"Comentários\"><span class=\"screen-reader-text\">Comentários</span></span>\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:8:\"comments\";s:10:\"label_type\";N;}s:4:\"date\";a:6:{s:4:\"type\";s:4:\"date\";s:5:\"label\";s:4:\"Data\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:4:\"date\";s:10:\"label_type\";N;}s:11:\"gadwp_stats\";a:6:{s:4:\"type\";s:11:\"gadwp_stats\";s:5:\"label\";s:9:\"Análises\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:4:\"name\";s:11:\"gadwp_stats\";s:10:\"label_type\";N;}s:13:\"5c791bae15c1d\";a:11:{s:4:\"type\";s:21:\"column-featured_image\";s:5:\"label\";s:6:\"Anexos\";s:5:\"width\";s:0:\"\";s:10:\"width_unit\";s:1:\"%\";s:18:\"attachment_display\";s:9:\"thumbnail\";s:10:\"image_size\";s:11:\"cpac-custom\";s:12:\"image_size_w\";s:2:\"60\";s:12:\"image_size_h\";s:2:\"60\";s:15:\"number_of_items\";s:2:\"10\";s:4:\"name\";s:13:\"5c791bae15c1d\";s:10:\"label_type\";N;}}', 'no'),
(572, 'cpac_options_post__default', 'a:11:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:7:\"Título\";s:6:\"author\";s:5:\"Autor\";s:10:\"categories\";s:10:\"Categorias\";s:4:\"tags\";s:4:\"Tags\";s:8:\"comments\";s:119:\"<span class=\"vers comment-grey-bubble\" title=\"Comentários\"><span class=\"screen-reader-text\">Comentários</span></span>\";s:4:\"date\";s:4:\"Data\";s:11:\"gadwp_stats\";s:9:\"Análises\";s:23:\"wpfc_column_clear_cache\";s:5:\"Cache\";s:8:\"seotitle\";s:14:\"Título do SEO\";s:7:\"seodesc\";s:18:\"Descrição do SEO\";}', 'no'),
(577, 'wp_webp_allways', 'a:1:{s:2:\"cb\";s:1:\"1\";}', 'yes'),
(578, 'wp_webp_infooter', 'a:1:{s:2:\"cb\";s:1:\"1\";}', 'yes'),
(579, 'wp_webp_experimental', '', 'yes'),
(582, 'wpseo', 'a:20:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:3:\"9.7\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1551441203;s:18:\"recalibration_beta\";b:0;}', 'yes'),
(583, 'wpseo_titles', 'a:73:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:40:\"%%name%%, Autor em %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:66:\"Você pesquisou por %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:44:\"Página não encontrada %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:54:\"O post %%POSTLINK%% apareceu primeiro em %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:33:\"Erro 404: Página não encontrada\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:13:\"Arquivos para\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:7:\"Início\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:19:\"Você pesquisou por\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:11:\"title-banco\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:14:\"metadesc-banco\";s:0:\"\";s:13:\"noindex-banco\";b:0;s:14:\"showdate-banco\";b:0;s:24:\"display-metabox-pt-banco\";b:1;s:24:\"post_types-banco-maintax\";i:0;s:18:\"title-tax-category\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;}', 'yes'),
(584, 'wpseo_social', 'a:20:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(585, 'wpseo_flush_rewrite', '1', 'yes'),
(586, '_transient_timeout_wpseo_link_table_inaccessible', '1582977204', 'no'),
(587, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(588, '_transient_timeout_wpseo_meta_table_inaccessible', '1582977204', 'no'),
(589, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(591, 'rewrite_rules', 'a:127:{s:14:\"sitemap.xml.gz\";s:60:\"index.php?aiosp_sitemap_gzipped=1&aiosp_sitemap_path=root.gz\";s:25:\"sitemap_(.+)_(\\d+).xml.gz\";s:74:\"index.php?aiosp_sitemap_path=$matches[1].gz&aiosp_sitemap_page=$matches[2]\";s:19:\"sitemap_(.+).xml.gz\";s:43:\"index.php?aiosp_sitemap_path=$matches[1].gz\";s:11:\"sitemap.xml\";s:33:\"index.php?aiosp_sitemap_path=root\";s:22:\"sitemap_(.+)_(\\d+).xml\";s:71:\"index.php?aiosp_sitemap_path=$matches[1]&aiosp_sitemap_page=$matches[2]\";s:16:\"sitemap_(.+).xml\";s:40:\"index.php?aiosp_sitemap_path=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:33:\"banco/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"banco/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"banco/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"banco/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"banco/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"banco/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"banco/([^/]+)/embed/?$\";s:38:\"index.php?banco=$matches[1]&embed=true\";s:26:\"banco/([^/]+)/trackback/?$\";s:32:\"index.php?banco=$matches[1]&tb=1\";s:34:\"banco/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?banco=$matches[1]&paged=$matches[2]\";s:41:\"banco/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?banco=$matches[1]&cpage=$matches[2]\";s:30:\"banco/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?banco=$matches[1]&page=$matches[2]\";s:22:\"banco/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"banco/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"banco/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"banco/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"banco/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"banco/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"banner/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"banner/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"banner/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"banner/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"banner/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"banner/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"banner/([^/]+)/embed/?$\";s:39:\"index.php?banner=$matches[1]&embed=true\";s:27:\"banner/([^/]+)/trackback/?$\";s:33:\"index.php?banner=$matches[1]&tb=1\";s:35:\"banner/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?banner=$matches[1]&paged=$matches[2]\";s:42:\"banner/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?banner=$matches[1]&cpage=$matches[2]\";s:31:\"banner/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?banner=$matches[1]&page=$matches[2]\";s:23:\"banner/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"banner/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"banner/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"banner/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"banner/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"banner/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(592, 'wpseo_onpage', 'a:2:{s:6:\"status\";i:1;s:10:\"last_fetch\";i:1551441206;}', 'yes'),
(594, 'WpFc_api_key', 'e55b8a6f380b82a6bc4a3311b0e3162c', 'yes'),
(595, 'WpFastestCache', '{\"wpFastestCacheStatus\":\"on\",\"wpFastestCachePreload_number\":\"4\",\"wpFastestCacheLoggedInUser\":\"on\",\"wpFastestCacheMobile\":\"on\",\"wpFastestCacheNewPost\":\"on\",\"wpFastestCacheNewPost_type\":\"all\",\"wpFastestCacheUpdatePost\":\"on\",\"wpFastestCacheUpdatePost_type\":\"all\",\"wpFastestCacheMinifyHtml\":\"on\",\"wpFastestCacheMinifyCss\":\"on\",\"wpFastestCacheCombineCss\":\"on\",\"wpFastestCacheCombineJs\":\"on\",\"wpFastestCacheGzip\":\"on\",\"wpFastestCacheLBC\":\"on\",\"wpFastestCacheDisableEmojis\":\"on\",\"wpFastestCacheLanguage\":\"pt\"}', 'yes'),
(596, 'wpfc-group', '', 'yes');
INSERT INTO `bai_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(605, 'aioseop_options', 'a:85:{s:16:\"aiosp_home_title\";s:56:\"O meu bairro - Um portal de denúncias e banco de ideias\";s:22:\"aiosp_home_description\";s:138:\"Um portal de denúncias onde pessoas de forma colaborativa ajudam a identificar e resolver problemas de saneamento básico, lixo e asfalto\";s:20:\"aiosp_togglekeywords\";s:1:\"1\";s:19:\"aiosp_home_keywords\";s:0:\"\";s:26:\"aiosp_use_static_home_info\";s:1:\"0\";s:9:\"aiosp_can\";s:2:\"on\";s:30:\"aiosp_no_paged_canonical_links\";s:0:\"\";s:31:\"aiosp_customize_canonical_links\";s:0:\"\";s:20:\"aiosp_rewrite_titles\";s:1:\"1\";s:20:\"aiosp_force_rewrites\";s:1:\"1\";s:24:\"aiosp_use_original_title\";s:1:\"0\";s:28:\"aiosp_home_page_title_format\";s:12:\"%page_title%\";s:23:\"aiosp_page_title_format\";s:27:\"%page_title% | %blog_title%\";s:23:\"aiosp_post_title_format\";s:27:\"%post_title% | %blog_title%\";s:27:\"aiosp_category_title_format\";s:31:\"%category_title% | %blog_title%\";s:26:\"aiosp_archive_title_format\";s:30:\"%archive_title% | %blog_title%\";s:23:\"aiosp_date_title_format\";s:21:\"%date% | %blog_title%\";s:25:\"aiosp_author_title_format\";s:23:\"%author% | %blog_title%\";s:22:\"aiosp_tag_title_format\";s:20:\"%tag% | %blog_title%\";s:25:\"aiosp_search_title_format\";s:23:\"%search% | %blog_title%\";s:24:\"aiosp_description_format\";s:13:\"%description%\";s:22:\"aiosp_404_title_format\";s:33:\"Nothing found for %request_words%\";s:18:\"aiosp_paged_format\";s:14:\" - Part %page%\";s:17:\"aiosp_cpostactive\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:18:\"aiosp_cpostnoindex\";s:0:\"\";s:19:\"aiosp_cpostnofollow\";s:0:\"\";s:21:\"aiosp_posttypecolumns\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:19:\"aiosp_google_verify\";s:43:\"2xgWlBRK4RzDOskUT_yshb6Ve7l18Wc_WY4yvBCrp3Q\";s:17:\"aiosp_bing_verify\";s:0:\"\";s:22:\"aiosp_pinterest_verify\";s:0:\"\";s:22:\"aiosp_google_publisher\";s:0:\"\";s:28:\"aiosp_google_disable_profile\";s:0:\"\";s:29:\"aiosp_google_sitelinks_search\";s:0:\"\";s:26:\"aiosp_google_set_site_name\";s:0:\"\";s:30:\"aiosp_google_specify_site_name\";s:0:\"\";s:28:\"aiosp_google_author_advanced\";s:1:\"0\";s:28:\"aiosp_google_author_location\";a:1:{i:0;s:3:\"all\";}s:29:\"aiosp_google_enable_publisher\";s:2:\"on\";s:30:\"aiosp_google_specify_publisher\";s:0:\"\";s:25:\"aiosp_google_analytics_id\";s:0:\"\";s:25:\"aiosp_ga_advanced_options\";s:2:\"on\";s:15:\"aiosp_ga_domain\";s:0:\"\";s:21:\"aiosp_ga_multi_domain\";s:0:\"\";s:21:\"aiosp_ga_addl_domains\";s:0:\"\";s:21:\"aiosp_ga_anonymize_ip\";s:0:\"\";s:28:\"aiosp_ga_display_advertising\";s:0:\"\";s:22:\"aiosp_ga_exclude_users\";s:0:\"\";s:29:\"aiosp_ga_track_outbound_links\";s:0:\"\";s:25:\"aiosp_ga_link_attribution\";s:0:\"\";s:27:\"aiosp_ga_enhanced_ecommerce\";s:0:\"\";s:20:\"aiosp_use_categories\";s:0:\"\";s:26:\"aiosp_use_tags_as_keywords\";s:2:\"on\";s:32:\"aiosp_dynamic_postspage_keywords\";s:2:\"on\";s:22:\"aiosp_category_noindex\";s:2:\"on\";s:26:\"aiosp_archive_date_noindex\";s:2:\"on\";s:28:\"aiosp_archive_author_noindex\";s:2:\"on\";s:18:\"aiosp_tags_noindex\";s:0:\"\";s:20:\"aiosp_search_noindex\";s:0:\"\";s:17:\"aiosp_404_noindex\";s:0:\"\";s:17:\"aiosp_tax_noindex\";s:0:\"\";s:23:\"aiosp_paginated_noindex\";s:0:\"\";s:24:\"aiosp_paginated_nofollow\";s:0:\"\";s:27:\"aiosp_generate_descriptions\";s:2:\"on\";s:18:\"aiosp_skip_excerpt\";s:2:\"on\";s:20:\"aiosp_run_shortcodes\";s:0:\"\";s:33:\"aiosp_hide_paginated_descriptions\";s:0:\"\";s:32:\"aiosp_dont_truncate_descriptions\";s:0:\"\";s:19:\"aiosp_schema_markup\";s:2:\"on\";s:20:\"aiosp_unprotect_meta\";s:0:\"\";s:33:\"aiosp_redirect_attachement_parent\";s:0:\"\";s:14:\"aiosp_ex_pages\";s:0:\"\";s:20:\"aiosp_post_meta_tags\";s:0:\"\";s:20:\"aiosp_page_meta_tags\";s:0:\"\";s:21:\"aiosp_front_meta_tags\";s:0:\"\";s:20:\"aiosp_home_meta_tags\";s:0:\"\";s:12:\"aiosp_do_log\";s:0:\"\";s:19:\"last_active_version\";s:4:\"2.11\";s:29:\"aiosp_attachment_title_format\";s:27:\"%post_title% | %blog_title%\";s:31:\"aiosp_oembed_cache_title_format\";s:27:\"%post_title% | %blog_title%\";s:31:\"aiosp_user_request_title_format\";s:27:\"%post_title% | %blog_title%\";s:22:\"aiosp_acf_title_format\";s:27:\"%post_title% | %blog_title%\";s:36:\"aiosp_amn_exact-metrics_title_format\";s:27:\"%post_title% | %blog_title%\";s:24:\"aiosp_banco_title_format\";s:27:\"%post_title% | %blog_title%\";s:25:\"aiosp_banner_title_format\";s:27:\"%post_title% | %blog_title%\";s:7:\"modules\";a:3:{s:29:\"aiosp_feature_manager_options\";a:7:{s:36:\"aiosp_feature_manager_enable_sitemap\";s:2:\"on\";s:38:\"aiosp_feature_manager_enable_opengraph\";s:2:\"on\";s:35:\"aiosp_feature_manager_enable_robots\";s:0:\"\";s:40:\"aiosp_feature_manager_enable_file_editor\";s:0:\"\";s:46:\"aiosp_feature_manager_enable_importer_exporter\";s:0:\"\";s:39:\"aiosp_feature_manager_enable_bad_robots\";s:0:\"\";s:40:\"aiosp_feature_manager_enable_performance\";s:2:\"on\";}s:21:\"aiosp_sitemap_options\";a:39:{s:25:\"aiosp_sitemap_rss_sitemap\";s:0:\"\";s:24:\"aiosp_sitemap_daily_cron\";s:6:\"weekly\";s:21:\"aiosp_sitemap_indexes\";s:0:\"\";s:23:\"aiosp_sitemap_max_posts\";s:5:\"50000\";s:23:\"aiosp_sitemap_posttypes\";a:5:{i:0;s:3:\"all\";i:1;s:4:\"post\";i:2;s:4:\"page\";i:3;s:10:\"attachment\";i:4;s:5:\"banco\";}s:24:\"aiosp_sitemap_taxonomies\";a:4:{i:0;s:3:\"all\";i:1;s:8:\"category\";i:2;s:8:\"post_tag\";i:3;s:11:\"post_format\";}s:21:\"aiosp_sitemap_archive\";s:0:\"\";s:20:\"aiosp_sitemap_author\";s:0:\"\";s:20:\"aiosp_sitemap_images\";s:0:\"\";s:21:\"aiosp_sitemap_gzipped\";s:2:\"on\";s:20:\"aiosp_sitemap_robots\";s:2:\"on\";s:21:\"aiosp_sitemap_rewrite\";s:2:\"on\";s:24:\"aiosp_sitemap_addl_pages\";a:0:{}s:29:\"aiosp_sitemap_excl_categories\";s:0:\"\";s:24:\"aiosp_sitemap_excl_pages\";s:0:\"\";s:27:\"aiosp_sitemap_prio_homepage\";s:2:\"no\";s:23:\"aiosp_sitemap_prio_post\";s:2:\"no\";s:29:\"aiosp_sitemap_prio_post_banco\";s:2:\"no\";s:34:\"aiosp_sitemap_prio_post_attachment\";s:2:\"no\";s:28:\"aiosp_sitemap_prio_post_page\";s:2:\"no\";s:28:\"aiosp_sitemap_prio_post_post\";s:2:\"no\";s:29:\"aiosp_sitemap_prio_taxonomies\";s:2:\"no\";s:41:\"aiosp_sitemap_prio_taxonomies_post_format\";s:2:\"no\";s:38:\"aiosp_sitemap_prio_taxonomies_post_tag\";s:2:\"no\";s:38:\"aiosp_sitemap_prio_taxonomies_category\";s:2:\"no\";s:26:\"aiosp_sitemap_prio_archive\";s:2:\"no\";s:25:\"aiosp_sitemap_prio_author\";s:2:\"no\";s:27:\"aiosp_sitemap_freq_homepage\";s:2:\"no\";s:23:\"aiosp_sitemap_freq_post\";s:2:\"no\";s:29:\"aiosp_sitemap_freq_post_banco\";s:2:\"no\";s:34:\"aiosp_sitemap_freq_post_attachment\";s:2:\"no\";s:28:\"aiosp_sitemap_freq_post_page\";s:2:\"no\";s:28:\"aiosp_sitemap_freq_post_post\";s:2:\"no\";s:29:\"aiosp_sitemap_freq_taxonomies\";s:2:\"no\";s:41:\"aiosp_sitemap_freq_taxonomies_post_format\";s:2:\"no\";s:38:\"aiosp_sitemap_freq_taxonomies_post_tag\";s:2:\"no\";s:38:\"aiosp_sitemap_freq_taxonomies_category\";s:2:\"no\";s:26:\"aiosp_sitemap_freq_archive\";s:2:\"no\";s:25:\"aiosp_sitemap_freq_author\";s:2:\"no\";}s:23:\"aiosp_opengraph_options\";a:54:{s:27:\"aiosp_opengraph_scan_header\";s:0:\"\";s:23:\"aiosp_opengraph_setmeta\";s:0:\"\";s:19:\"aiosp_opengraph_key\";s:0:\"\";s:21:\"aiosp_opengraph_appid\";s:0:\"\";s:32:\"aiosp_opengraph_title_shortcodes\";s:0:\"\";s:38:\"aiosp_opengraph_description_shortcodes\";s:0:\"\";s:24:\"aiosp_opengraph_sitename\";s:12:\"O meu bairro\";s:25:\"aiosp_opengraph_hometitle\";s:0:\"\";s:27:\"aiosp_opengraph_description\";s:0:\"\";s:25:\"aiosp_opengraph_homeimage\";s:88:\"http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-logo-o-meu-bairro-2.png\";s:37:\"aiosp_opengraph_generate_descriptions\";s:0:\"\";s:22:\"aiosp_opengraph_defimg\";s:6:\"attach\";s:24:\"aiosp_opengraph_fallback\";s:0:\"\";s:20:\"aiosp_opengraph_dimg\";s:94:\"https://omeubairro.online/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png\";s:25:\"aiosp_opengraph_dimgwidth\";s:0:\"\";s:26:\"aiosp_opengraph_dimgheight\";s:0:\"\";s:24:\"aiosp_opengraph_meta_key\";s:0:\"\";s:21:\"aiosp_opengraph_image\";N;s:25:\"aiosp_opengraph_customimg\";N;s:26:\"aiosp_opengraph_imagewidth\";s:0:\"\";s:27:\"aiosp_opengraph_imageheight\";s:0:\"\";s:21:\"aiosp_opengraph_video\";N;s:26:\"aiosp_opengraph_videowidth\";s:0:\"\";s:27:\"aiosp_opengraph_videoheight\";s:0:\"\";s:23:\"aiosp_opengraph_defcard\";s:7:\"summary\";s:23:\"aiosp_opengraph_setcard\";N;s:28:\"aiosp_opengraph_twitter_site\";s:0:\"\";s:31:\"aiosp_opengraph_twitter_creator\";s:0:\"\";s:30:\"aiosp_opengraph_twitter_domain\";s:0:\"\";s:33:\"aiosp_opengraph_customimg_twitter\";N;s:24:\"aiosp_opengraph_gen_tags\";s:0:\"\";s:28:\"aiosp_opengraph_gen_keywords\";s:2:\"on\";s:30:\"aiosp_opengraph_gen_categories\";s:2:\"on\";s:29:\"aiosp_opengraph_gen_post_tags\";s:2:\"on\";s:21:\"aiosp_opengraph_types\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:21:\"aiosp_opengraph_title\";s:0:\"\";s:20:\"aiosp_opengraph_desc\";s:0:\"\";s:24:\"aiosp_opengraph_category\";s:0:\"\";s:23:\"aiosp_opengraph_section\";s:0:\"\";s:19:\"aiosp_opengraph_tag\";s:0:\"\";s:34:\"aiosp_opengraph_facebook_publisher\";s:0:\"\";s:31:\"aiosp_opengraph_facebook_author\";s:0:\"\";s:29:\"aiosp_opengraph_profile_links\";s:0:\"\";s:29:\"aiosp_opengraph_person_or_org\";s:3:\"org\";s:27:\"aiosp_opengraph_social_name\";s:0:\"\";s:35:\"aiosp_opengraph_post_fb_object_type\";s:7:\"article\";s:35:\"aiosp_opengraph_page_fb_object_type\";s:7:\"article\";s:41:\"aiosp_opengraph_attachment_fb_object_type\";s:7:\"article\";s:43:\"aiosp_opengraph_oembed_cache_fb_object_type\";s:7:\"article\";s:43:\"aiosp_opengraph_user_request_fb_object_type\";s:7:\"article\";s:34:\"aiosp_opengraph_acf_fb_object_type\";s:7:\"article\";s:48:\"aiosp_opengraph_amn_exact-metrics_fb_object_type\";s:7:\"article\";s:36:\"aiosp_opengraph_banco_fb_object_type\";s:7:\"article\";s:37:\"aiosp_opengraph_banner_fb_object_type\";s:7:\"article\";}}}', 'yes'),
(642, 'widget_a2a_share_save_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(643, 'widget_a2a_follow_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(644, 'addtoany_options', 'a:35:{s:8:\"position\";s:6:\"bottom\";s:30:\"display_in_posts_on_front_page\";s:1:\"1\";s:33:\"display_in_posts_on_archive_pages\";s:1:\"1\";s:19:\"display_in_excerpts\";s:2:\"-1\";s:16:\"display_in_posts\";s:1:\"1\";s:16:\"display_in_pages\";s:2:\"-1\";s:22:\"display_in_attachments\";s:2:\"-1\";s:15:\"display_in_feed\";s:1:\"1\";s:7:\"onclick\";s:2:\"-1\";s:9:\"icon_size\";s:2:\"16\";s:7:\"icon_bg\";s:8:\"original\";s:13:\"icon_bg_color\";s:7:\"#2a2a2a\";s:7:\"icon_fg\";s:8:\"original\";s:13:\"icon_fg_color\";s:7:\"#ffffff\";s:6:\"button\";s:10:\"A2A_SVG_32\";s:13:\"button_custom\";s:0:\"\";s:17:\"button_show_count\";s:2:\"-1\";s:6:\"header\";s:0:\"\";s:23:\"additional_js_variables\";s:0:\"\";s:14:\"additional_css\";s:0:\"\";s:12:\"custom_icons\";s:2:\"-1\";s:16:\"custom_icons_url\";s:1:\"/\";s:17:\"custom_icons_type\";s:3:\"png\";s:18:\"custom_icons_width\";s:0:\"\";s:19:\"custom_icons_height\";s:0:\"\";s:5:\"cache\";s:2:\"-1\";s:20:\"display_in_cpt_banco\";s:1:\"1\";s:11:\"button_text\";s:12:\"Compartilhar\";s:24:\"special_facebook_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:15:\"active_services\";a:5:{i:0;s:8:\"facebook\";i:1;s:7:\"twitter\";i:2;s:5:\"email\";i:3;s:8:\"whatsapp\";i:4;s:8:\"telegram\";}s:29:\"special_facebook_like_options\";a:2:{s:10:\"show_count\";s:2:\"-1\";s:4:\"verb\";s:4:\"like\";}s:29:\"special_twitter_tweet_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:30:\"special_google_plusone_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:33:\"special_google_plus_share_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:29:\"special_pinterest_pin_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}}', 'yes'),
(648, 'aiosp_sitemap_cron_last_run', '1564151833', 'yes'),
(730, 'gadwp_cache_qr2_2555708030', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:206:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:uniquePageviews&filters=ga:pagePath%3D%3D/maraponga-3/&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:30:{i:0;a:3:{i:0;s:8:\"20190131\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:1;a:3:{i:0;s:8:\"20190201\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:2;a:3:{i:0;s:8:\"20190202\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:3;a:3:{i:0;s:8:\"20190203\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:4;a:3:{i:0;s:8:\"20190204\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:5;a:3:{i:0;s:8:\"20190205\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:6;a:3:{i:0;s:8:\"20190206\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:7;a:3:{i:0;s:8:\"20190207\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:8;a:3:{i:0;s:8:\"20190208\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:9;a:3:{i:0;s:8:\"20190209\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:10;a:3:{i:0;s:8:\"20190210\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:11;a:3:{i:0;s:8:\"20190211\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:12;a:3:{i:0;s:8:\"20190212\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:13;a:3:{i:0;s:8:\"20190213\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:14;a:3:{i:0;s:8:\"20190214\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:15;a:3:{i:0;s:8:\"20190215\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:16;a:3:{i:0;s:8:\"20190216\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:17;a:3:{i:0;s:8:\"20190217\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:18;a:3:{i:0;s:8:\"20190218\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:19;a:3:{i:0;s:8:\"20190219\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:20;a:3:{i:0;s:8:\"20190220\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:21;a:3:{i:0;s:8:\"20190221\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:22;a:3:{i:0;s:8:\"20190222\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:23;a:3:{i:0;s:8:\"20190223\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:24;a:3:{i:0;s:8:\"20190224\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:25;a:3:{i:0;s:8:\"20190225\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:26;a:3:{i:0;s:8:\"20190226\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:27;a:3:{i:0;s:8:\"20190227\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:28;a:3:{i:0;s:8:\"20190228\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:29;a:3:{i:0;s:8:\"20190301\";i:1;s:6:\"Friday\";i:2;s:1:\"9\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:206:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:uniquePageviews&filters=ga:pagePath%3D%3D/maraponga-3/&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:30;s:19:\"totalsForAllResults\";a:1:{s:18:\"ga:uniquePageviews\";s:1:\"9\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:9:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:24:\"ga:date,ga:dayOfWeekName\";s:7:\"metrics\";a:1:{i:0;s:18:\"ga:uniquePageviews\";}s:7:\"filters\";s:26:\"ga:pagePath==/maraponga-3/\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:3:{i:0;a:3:{s:4:\"name\";s:7:\"ga:date\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:16:\"ga:dayOfWeekName\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:2;a:3:{s:4:\"name\";s:18:\"ga:uniquePageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1551582000;}', 'no'),
(731, 'gadwp_cache_qr3_4211510041', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:296:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:uniquePageviews,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:exitRate&filters=ga:pagePath%3D%3D/maraponga-3/&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:1:{i:0;a:9:{i:0;s:1:\"9\";i:1;s:1:\"5\";i:2;s:2:\"17\";i:3;s:4:\"25.0\";i:4;s:1:\"0\";i:5;s:4:\"4.25\";i:6;s:18:\"151.92307692307693\";i:7;s:3:\"0.0\";i:8;s:17:\"23.52941176470588\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:296:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:uniquePageviews,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:exitRate&filters=ga:pagePath%3D%3D/maraponga-3/&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:1;s:19:\"totalsForAllResults\";a:9:{s:18:\"ga:uniquePageviews\";s:1:\"9\";s:8:\"ga:users\";s:1:\"5\";s:12:\"ga:pageviews\";s:2:\"17\";s:13:\"ga:BounceRate\";s:4:\"25.0\";s:18:\"ga:organicSearches\";s:1:\"0\";s:22:\"ga:pageviewsPerSession\";s:4:\"4.25\";s:16:\"ga:avgTimeOnPage\";s:18:\"151.92307692307693\";s:18:\"ga:avgPageLoadTime\";s:3:\"0.0\";s:11:\"ga:exitRate\";s:17:\"23.52941176470588\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:8:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:7:\"metrics\";a:9:{i:0;s:18:\"ga:uniquePageviews\";i:1;s:8:\"ga:users\";i:2;s:12:\"ga:pageviews\";i:3;s:13:\"ga:BounceRate\";i:4;s:18:\"ga:organicSearches\";i:5;s:22:\"ga:pageviewsPerSession\";i:6;s:16:\"ga:avgTimeOnPage\";i:7;s:18:\"ga:avgPageLoadTime\";i:8;s:11:\"ga:exitRate\";}s:7:\"filters\";s:26:\"ga:pagePath==/maraponga-3/\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:9:{i:0;a:3:{s:4:\"name\";s:18:\"ga:uniquePageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:1;a:3:{s:4:\"name\";s:8:\"ga:users\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:2;a:3:{s:4:\"name\";s:12:\"ga:pageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:3;a:3:{s:4:\"name\";s:13:\"ga:BounceRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}i:4;a:3:{s:4:\"name\";s:18:\"ga:organicSearches\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:5;a:3:{s:4:\"name\";s:22:\"ga:pageviewsPerSession\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:6;a:3:{s:4:\"name\";s:16:\"ga:avgTimeOnPage\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:4:\"TIME\";}i:7;a:3:{s:4:\"name\";s:18:\"ga:avgPageLoadTime\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:8;a:3:{s:4:\"name\";s:11:\"ga:exitRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1551582000;}', 'no'),
(869, 'options_apoio_1_imagem', '721', 'no'),
(870, '_options_apoio_1_imagem', 'field_5c740d8b97882', 'no'),
(876, 'options_desenvolvedores_3_imagem', '722', 'no'),
(877, '_options_desenvolvedores_3_imagem', 'field_5c740a7dabe75', 'no'),
(919, 'options_desenvolvedores_4_imagem', '735', 'no'),
(920, '_options_desenvolvedores_4_imagem', 'field_5c740a7dabe75', 'no'),
(922, 'options_desenvolvedores_0_nome', 'Diego Curumim', 'no'),
(923, '_options_desenvolvedores_0_nome', 'field_5c81627523323', 'no'),
(924, 'options_desenvolvedores_1_nome', 'Felipe', 'no'),
(925, '_options_desenvolvedores_1_nome', 'field_5c81627523323', 'no'),
(926, 'options_desenvolvedores_2_nome', 'Jonas Sousa', 'no'),
(927, '_options_desenvolvedores_2_nome', 'field_5c81627523323', 'no'),
(928, 'options_desenvolvedores_3_nome', 'Nelis Rodrigues', 'no'),
(929, '_options_desenvolvedores_3_nome', 'field_5c81627523323', 'no'),
(930, 'options_desenvolvedores_4_nome', 'Geovani', 'no'),
(931, '_options_desenvolvedores_4_nome', 'field_5c81627523323', 'no'),
(934, 'cpac_options_banner__default', 'a:5:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:7:\"Título\";s:4:\"date\";s:4:\"Data\";s:11:\"gadwp_stats\";s:9:\"Análises\";s:23:\"wpfc_column_clear_cache\";s:5:\"Cache\";}', 'no'),
(981, 'gadwp_cache_qr10_2698253840', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:194:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:medium&metrics=ga:sessions&sort=-ga:sessions&filters=ga:medium!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:3:{i:0;a:2:{i:0;s:6:\"(none)\";i:1;s:2:\"85\";}i:1;a:2:{i:0;s:8:\"referral\";i:1;s:2:\"21\";}i:2;a:2:{i:0;s:7:\"organic\";i:1;s:1:\"2\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:194:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:medium&metrics=ga:sessions&sort=-ga:sessions&filters=ga:medium!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:3;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:3:\"108\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:10:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:9:\"ga:medium\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:7:\"filters\";s:20:\"ga:medium!=(not set)\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:9:\"ga:medium\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552100400;}', 'no'),
(982, 'gadwp_cache_qr10_539844929', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:204:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:visitorType&metrics=ga:sessions&sort=-ga:sessions&filters=ga:visitorType!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:2:{i:0;a:2:{i:0;s:17:\"Returning Visitor\";i:1;s:2:\"56\";}i:1;a:2:{i:0;s:11:\"New Visitor\";i:1;s:2:\"52\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:204:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:visitorType&metrics=ga:sessions&sort=-ga:sessions&filters=ga:visitorType!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:2;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:3:\"108\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:10:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:14:\"ga:visitorType\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:7:\"filters\";s:25:\"ga:visitorType!=(not set)\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:14:\"ga:visitorType\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552100400;}', 'no'),
(983, 'gadwp_cache_qr10_3164645489', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:218:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:source&metrics=ga:sessions&sort=-ga:sessions&filters=ga:medium%3D%3Dorganic;ga:keyword!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:1:{i:0;a:2:{i:0;s:4:\"bing\";i:1;s:1:\"2\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:218:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:source&metrics=ga:sessions&sort=-ga:sessions&filters=ga:medium%3D%3Dorganic;ga:keyword!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:1;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:1:\"2\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:10:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:9:\"ga:source\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:7:\"filters\";s:40:\"ga:medium==organic;ga:keyword!=(not set)\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:9:\"ga:source\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552100400;}', 'no'),
(984, 'gadwp_cache_qr10_2988558087', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:208:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:socialNetwork&metrics=ga:sessions&sort=-ga:sessions&filters=ga:socialNetwork!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:2:{i:0;a:2:{i:0;s:8:\"Facebook\";i:1;s:1:\"9\";}i:1;a:2:{i:0;s:9:\"Instagram\";i:1;s:1:\"4\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:208:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:socialNetwork&metrics=ga:sessions&sort=-ga:sessions&filters=ga:socialNetwork!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:2;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:2:\"13\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:10:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:16:\"ga:socialNetwork\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:7:\"filters\";s:27:\"ga:socialNetwork!=(not set)\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:16:\"ga:socialNetwork\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552100400;}', 'no'),
(986, 'gadwp_cache_qr4_265325396', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:166:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:pageTitle&metrics=ga:sessions&sort=-ga:sessions&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:31:{i:0;a:2:{i:0;s:56:\"O meu bairro - Um portal de denúncias e banco de ideias\";i:1;s:2:\"54\";}i:1;a:2:{i:0;s:152:\"Problemas do meu bairro – Um portal onde pessoas de forma colaborativa ajudam a identificar e resolver problemas de saneamento básico, lixo e asfalto\";i:1;s:2:\"19\";}i:2;a:2:{i:0;s:24:\"Maraponga | O meu bairro\";i:1;s:2:\"10\";}i:3;a:2:{i:0;s:141:\"O meu bairro – Um portal onde pessoas de forma colaborativa ajudam a identificar e resolver problemas de saneamento básico, lixo e asfalto\";i:1;s:1:\"9\";}i:4;a:2:{i:0;s:12:\"O meu bairro\";i:1;s:1:\"5\";}i:5;a:2:{i:0;s:38:\"Problemas Identificados | O meu bairro\";i:1;s:1:\"2\";}i:6;a:2:{i:0;s:36:\"Identificar problemas | O meu bairro\";i:1;s:1:\"1\";}i:7;a:2:{i:0;s:49:\"Identificar problemas – Problemas do meu bairro\";i:1;s:1:\"1\";}i:8;a:2:{i:0;s:32:\"Mapa de problemas | O meu bairro\";i:1;s:1:\"1\";}i:9;a:2:{i:0;s:45:\"Mapa de problemas – Problemas do meu bairro\";i:1;s:1:\"1\";}i:10;a:2:{i:0;s:24:\"O que é? | O meu bairro\";i:1;s:1:\"1\";}i:11;a:2:{i:0;s:37:\"O que é? – Problemas do meu bairro\";i:1;s:1:\"1\";}i:12;a:2:{i:0;s:30:\"Parque Manibura | O meu bairro\";i:1;s:1:\"1\";}i:13;a:2:{i:0;s:40:\"Problemas Identificados – O meu bairro\";i:1;s:1:\"1\";}i:14;a:2:{i:0;s:51:\"Página não encontrada – Problemas do meu bairro\";i:1;s:1:\"1\";}i:15;a:2:{i:0;s:29:\"Como funciona? | O meu bairro\";i:1;s:1:\"0\";}i:16;a:2:{i:0;s:31:\"Como funciona? – O meu bairro\";i:1;s:1:\"0\";}i:17;a:2:{i:0;s:42:\"Como funciona? – Problemas do meu bairro\";i:1;s:1:\"0\";}i:18;a:2:{i:0;s:68:\"Guia prático de combate à vigilância na internet – O meu bairro\";i:1;s:1:\"0\";}i:19;a:2:{i:0;s:79:\"Guia prático de combate à vigilância na internet – Problemas do meu bairro\";i:1;s:1:\"0\";}i:20;a:2:{i:0;s:38:\"Identificar problemas – O meu bairro\";i:1;s:1:\"0\";}i:21;a:2:{i:0;s:34:\"Mapa de problemas – O meu bairro\";i:1;s:1:\"0\";}i:22;a:2:{i:0;s:26:\"Maraponga – O meu bairro\";i:1;s:1:\"0\";}i:23;a:2:{i:0;s:37:\"Maraponga – Problemas do meu bairro\";i:1;s:1:\"0\";}i:24;a:2:{i:0;s:32:\"Parque Manibura – O meu bairro\";i:1;s:1:\"0\";}i:25;a:2:{i:0;s:43:\"Parque Manibura – Problemas do meu bairro\";i:1;s:1:\"0\";}i:26;a:2:{i:0;s:51:\"Problemas Identificados – Problemas do meu bairro\";i:1;s:1:\"0\";}i:27;a:2:{i:0;s:35:\"Problemas resolvidos | O meu bairro\";i:1;s:1:\"0\";}i:28;a:2:{i:0;s:40:\"Página não encontrada – O meu bairro\";i:1;s:1:\"0\";}i:29;a:2:{i:0;s:59:\"Resultados da pesquisa por “Maraponga” – O meu bairro\";i:1;s:1:\"0\";}i:30;a:2:{i:0;s:37:\"Vila Pery – Problemas do meu bairro\";i:1;s:1:\"0\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:166:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:pageTitle&metrics=ga:sessions&sort=-ga:sessions&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:31;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:3:\"108\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:9:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:12:\"ga:pageTitle\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:12:\"ga:pageTitle\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552100400;}', 'no'),
(1170, 'gadwp_cache_qr2_2161628556', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:202:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:uniquePageviews&filters=ga:pagePath%3D%3D/pio-xii/&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:30:{i:0;a:3:{i:0;s:8:\"20190210\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:1;a:3:{i:0;s:8:\"20190211\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:2;a:3:{i:0;s:8:\"20190212\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:3;a:3:{i:0;s:8:\"20190213\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:4;a:3:{i:0;s:8:\"20190214\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:5;a:3:{i:0;s:8:\"20190215\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:6;a:3:{i:0;s:8:\"20190216\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:7;a:3:{i:0;s:8:\"20190217\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:8;a:3:{i:0;s:8:\"20190218\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:9;a:3:{i:0;s:8:\"20190219\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:10;a:3:{i:0;s:8:\"20190220\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:11;a:3:{i:0;s:8:\"20190221\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:12;a:3:{i:0;s:8:\"20190222\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:13;a:3:{i:0;s:8:\"20190223\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:14;a:3:{i:0;s:8:\"20190224\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:15;a:3:{i:0;s:8:\"20190225\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:16;a:3:{i:0;s:8:\"20190226\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:17;a:3:{i:0;s:8:\"20190227\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:18;a:3:{i:0;s:8:\"20190228\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:19;a:3:{i:0;s:8:\"20190301\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:20;a:3:{i:0;s:8:\"20190302\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:21;a:3:{i:0;s:8:\"20190303\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:22;a:3:{i:0;s:8:\"20190304\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:23;a:3:{i:0;s:8:\"20190305\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:24;a:3:{i:0;s:8:\"20190306\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:25;a:3:{i:0;s:8:\"20190307\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:26;a:3:{i:0;s:8:\"20190308\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:27;a:3:{i:0;s:8:\"20190309\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:28;a:3:{i:0;s:8:\"20190310\";i:1;s:6:\"Sunday\";i:2;s:1:\"1\";}i:29;a:3:{i:0;s:8:\"20190311\";i:1;s:6:\"Monday\";i:2;s:1:\"3\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:202:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:uniquePageviews&filters=ga:pagePath%3D%3D/pio-xii/&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:30;s:19:\"totalsForAllResults\";a:1:{s:18:\"ga:uniquePageviews\";s:1:\"4\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:9:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:24:\"ga:date,ga:dayOfWeekName\";s:7:\"metrics\";a:1:{i:0;s:18:\"ga:uniquePageviews\";}s:7:\"filters\";s:22:\"ga:pagePath==/pio-xii/\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:3:{i:0;a:3:{s:4:\"name\";s:7:\"ga:date\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:16:\"ga:dayOfWeekName\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:2;a:3:{s:4:\"name\";s:18:\"ga:uniquePageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552446000;}', 'no'),
(1171, 'gadwp_cache_qr3_3911157740', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:292:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:uniquePageviews,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:exitRate&filters=ga:pagePath%3D%3D/pio-xii/&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:1:{i:0;a:9:{i:0;s:1:\"4\";i:1;s:1:\"4\";i:2;s:1:\"6\";i:3;s:3:\"0.0\";i:4;s:1:\"0\";i:5;s:3:\"0.0\";i:6;s:4:\"32.2\";i:7;s:3:\"0.0\";i:8;s:18:\"16.666666666666664\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:292:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:uniquePageviews,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:exitRate&filters=ga:pagePath%3D%3D/pio-xii/&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:1;s:19:\"totalsForAllResults\";a:9:{s:18:\"ga:uniquePageviews\";s:1:\"4\";s:8:\"ga:users\";s:1:\"4\";s:12:\"ga:pageviews\";s:1:\"6\";s:13:\"ga:BounceRate\";s:3:\"0.0\";s:18:\"ga:organicSearches\";s:1:\"0\";s:22:\"ga:pageviewsPerSession\";s:3:\"0.0\";s:16:\"ga:avgTimeOnPage\";s:4:\"32.2\";s:18:\"ga:avgPageLoadTime\";s:3:\"0.0\";s:11:\"ga:exitRate\";s:18:\"16.666666666666664\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:8:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:7:\"metrics\";a:9:{i:0;s:18:\"ga:uniquePageviews\";i:1;s:8:\"ga:users\";i:2;s:12:\"ga:pageviews\";i:3;s:13:\"ga:BounceRate\";i:4;s:18:\"ga:organicSearches\";i:5;s:22:\"ga:pageviewsPerSession\";i:6;s:16:\"ga:avgTimeOnPage\";i:7;s:18:\"ga:avgPageLoadTime\";i:8;s:11:\"ga:exitRate\";}s:7:\"filters\";s:22:\"ga:pagePath==/pio-xii/\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:9:{i:0;a:3:{s:4:\"name\";s:18:\"ga:uniquePageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:1;a:3:{s:4:\"name\";s:8:\"ga:users\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:2;a:3:{s:4:\"name\";s:12:\"ga:pageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:3;a:3:{s:4:\"name\";s:13:\"ga:BounceRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}i:4;a:3:{s:4:\"name\";s:18:\"ga:organicSearches\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:5;a:3:{s:4:\"name\";s:22:\"ga:pageviewsPerSession\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:6;a:3:{s:4:\"name\";s:16:\"ga:avgTimeOnPage\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:4:\"TIME\";}i:7;a:3:{s:4:\"name\";s:18:\"ga:avgPageLoadTime\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:8;a:3:{s:4:\"name\";s:11:\"ga:exitRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552446000;}', 'no');
INSERT INTO `bai_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1172, 'gadwp_cache_qr2_552166683', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:206:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:uniquePageviews&filters=ga:pagePath%3D%3D/maraponga-2/&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:30:{i:0;a:3:{i:0;s:8:\"20190210\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:1;a:3:{i:0;s:8:\"20190211\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:2;a:3:{i:0;s:8:\"20190212\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:3;a:3:{i:0;s:8:\"20190213\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:4;a:3:{i:0;s:8:\"20190214\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:5;a:3:{i:0;s:8:\"20190215\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:6;a:3:{i:0;s:8:\"20190216\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:7;a:3:{i:0;s:8:\"20190217\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:8;a:3:{i:0;s:8:\"20190218\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:9;a:3:{i:0;s:8:\"20190219\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:10;a:3:{i:0;s:8:\"20190220\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:11;a:3:{i:0;s:8:\"20190221\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:12;a:3:{i:0;s:8:\"20190222\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:13;a:3:{i:0;s:8:\"20190223\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:14;a:3:{i:0;s:8:\"20190224\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:15;a:3:{i:0;s:8:\"20190225\";i:1;s:6:\"Monday\";i:2;s:1:\"3\";}i:16;a:3:{i:0;s:8:\"20190226\";i:1;s:7:\"Tuesday\";i:2;s:1:\"1\";}i:17;a:3:{i:0;s:8:\"20190227\";i:1;s:9:\"Wednesday\";i:2;s:1:\"1\";}i:18;a:3:{i:0;s:8:\"20190228\";i:1;s:8:\"Thursday\";i:2;s:1:\"2\";}i:19;a:3:{i:0;s:8:\"20190301\";i:1;s:6:\"Friday\";i:2;s:1:\"1\";}i:20;a:3:{i:0;s:8:\"20190302\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:21;a:3:{i:0;s:8:\"20190303\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:22;a:3:{i:0;s:8:\"20190304\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:23;a:3:{i:0;s:8:\"20190305\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:24;a:3:{i:0;s:8:\"20190306\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:25;a:3:{i:0;s:8:\"20190307\";i:1;s:8:\"Thursday\";i:2;s:1:\"4\";}i:26;a:3:{i:0;s:8:\"20190308\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:27;a:3:{i:0;s:8:\"20190309\";i:1;s:8:\"Saturday\";i:2;s:1:\"1\";}i:28;a:3:{i:0;s:8:\"20190310\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:29;a:3:{i:0;s:8:\"20190311\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:206:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:uniquePageviews&filters=ga:pagePath%3D%3D/maraponga-2/&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:30;s:19:\"totalsForAllResults\";a:1:{s:18:\"ga:uniquePageviews\";s:2:\"13\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:9:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:24:\"ga:date,ga:dayOfWeekName\";s:7:\"metrics\";a:1:{i:0;s:18:\"ga:uniquePageviews\";}s:7:\"filters\";s:26:\"ga:pagePath==/maraponga-2/\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:3:{i:0;a:3:{s:4:\"name\";s:7:\"ga:date\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:16:\"ga:dayOfWeekName\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:2;a:3:{s:4:\"name\";s:18:\"ga:uniquePageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552446000;}', 'no'),
(1173, 'gadwp_cache_qr3_1136323708', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:296:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:uniquePageviews,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:exitRate&filters=ga:pagePath%3D%3D/maraponga-2/&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:1:{i:0;a:9:{i:0;s:2:\"13\";i:1;s:1:\"7\";i:2;s:2:\"14\";i:3;s:5:\"100.0\";i:4;s:1:\"0\";i:5;s:4:\"14.0\";i:6;s:17:\"52.63636363636363\";i:7;s:3:\"0.0\";i:8;s:18:\"21.428571428571427\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:296:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:uniquePageviews,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:exitRate&filters=ga:pagePath%3D%3D/maraponga-2/&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:1;s:19:\"totalsForAllResults\";a:9:{s:18:\"ga:uniquePageviews\";s:2:\"13\";s:8:\"ga:users\";s:1:\"7\";s:12:\"ga:pageviews\";s:2:\"14\";s:13:\"ga:BounceRate\";s:5:\"100.0\";s:18:\"ga:organicSearches\";s:1:\"0\";s:22:\"ga:pageviewsPerSession\";s:4:\"14.0\";s:16:\"ga:avgTimeOnPage\";s:17:\"52.63636363636363\";s:18:\"ga:avgPageLoadTime\";s:3:\"0.0\";s:11:\"ga:exitRate\";s:18:\"21.428571428571427\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:8:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:7:\"metrics\";a:9:{i:0;s:18:\"ga:uniquePageviews\";i:1;s:8:\"ga:users\";i:2;s:12:\"ga:pageviews\";i:3;s:13:\"ga:BounceRate\";i:4;s:18:\"ga:organicSearches\";i:5;s:22:\"ga:pageviewsPerSession\";i:6;s:16:\"ga:avgTimeOnPage\";i:7;s:18:\"ga:avgPageLoadTime\";i:8;s:11:\"ga:exitRate\";}s:7:\"filters\";s:26:\"ga:pagePath==/maraponga-2/\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:9:{i:0;a:3:{s:4:\"name\";s:18:\"ga:uniquePageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:1;a:3:{s:4:\"name\";s:8:\"ga:users\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:2;a:3:{s:4:\"name\";s:12:\"ga:pageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:3;a:3:{s:4:\"name\";s:13:\"ga:BounceRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}i:4;a:3:{s:4:\"name\";s:18:\"ga:organicSearches\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:5;a:3:{s:4:\"name\";s:22:\"ga:pageviewsPerSession\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:6;a:3:{s:4:\"name\";s:16:\"ga:avgTimeOnPage\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:4:\"TIME\";}i:7;a:3:{s:4:\"name\";s:18:\"ga:avgPageLoadTime\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:8;a:3:{s:4:\"name\";s:11:\"ga:exitRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552446000;}', 'no'),
(1204, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:21:\"diegocuruma@gmail.com\";s:7:\"version\";s:6:\"4.9.10\";s:9:\"timestamp\";i:1552467061;}', 'no'),
(1274, 'gadwp_cache_qr8_3009268148', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:172:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:channelGrouping&metrics=ga:sessions&sort=-ga:sessions&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:4:{i:0;a:2:{i:0;s:6:\"Direct\";i:1;s:3:\"106\";}i:1;a:2:{i:0;s:6:\"Social\";i:1;s:2:\"17\";}i:2;a:2:{i:0;s:8:\"Referral\";i:1;s:2:\"11\";}i:3;a:2:{i:0;s:14:\"Organic Search\";i:1;s:1:\"2\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:172:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:channelGrouping&metrics=ga:sessions&sort=-ga:sessions&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:4;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:3:\"136\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:9:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:18:\"ga:channelGrouping\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:18:\"ga:channelGrouping\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552705200;}', 'no'),
(1275, 'gadwp_cache_qr6_265325396', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:196:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:keyword&metrics=ga:sessions&sort=-ga:sessions&filters=ga:keyword!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:1:{i:0;a:2:{i:0;s:6:\"amazon\";i:1;s:1:\"2\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:196:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:keyword&metrics=ga:sessions&sort=-ga:sessions&filters=ga:keyword!%3D(not+set)&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:1;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:1:\"2\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:10:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:10:\"ga:keyword\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:4:\"sort\";a:1:{i:0;s:12:\"-ga:sessions\";}s:7:\"filters\";s:21:\"ga:keyword!=(not set)\";s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:2:{i:0;a:3:{s:4:\"name\";s:10:\"ga:keyword\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1552705200;}', 'no'),
(1945, 'cpac_options_banco__default', 'a:5:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:7:\"Título\";s:4:\"date\";s:4:\"Data\";s:11:\"gadwp_stats\";s:9:\"Análises\";s:23:\"wpfc_column_clear_cache\";s:5:\"Cache\";}', 'no'),
(2535, 'cpac_options_page__default', 'a:9:{s:2:\"cb\";s:25:\"<input type=\"checkbox\" />\";s:5:\"title\";s:7:\"Título\";s:6:\"author\";s:5:\"Autor\";s:8:\"comments\";s:119:\"<span class=\"vers comment-grey-bubble\" title=\"Comentários\"><span class=\"screen-reader-text\">Comentários</span></span>\";s:4:\"date\";s:4:\"Data\";s:11:\"gadwp_stats\";s:9:\"Análises\";s:23:\"wpfc_column_clear_cache\";s:5:\"Cache\";s:8:\"seotitle\";s:14:\"Título do SEO\";s:7:\"seodesc\";s:18:\"Descrição do SEO\";}', 'no'),
(4111, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:7:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:5;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.1.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:6;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.4.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.4.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.4\";s:7:\"version\";s:5:\"5.0.4\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1564644412;s:15:\"version_checked\";s:6:\"4.9.10\";s:12:\"translations\";a:0:{}}', 'no'),
(4112, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1564644413;s:7:\"checked\";a:1:{s:6:\"bairro\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(4487, 'gadwp_cache_qr2_3392981156', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:160:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:sessions&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:30:{i:0;a:3:{i:0;s:8:\"20190616\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:1;a:3:{i:0;s:8:\"20190617\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:2;a:3:{i:0;s:8:\"20190618\";i:1;s:7:\"Tuesday\";i:2;s:1:\"1\";}i:3;a:3:{i:0;s:8:\"20190619\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:4;a:3:{i:0;s:8:\"20190620\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:5;a:3:{i:0;s:8:\"20190621\";i:1;s:6:\"Friday\";i:2;s:1:\"1\";}i:6;a:3:{i:0;s:8:\"20190622\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:7;a:3:{i:0;s:8:\"20190623\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:8;a:3:{i:0;s:8:\"20190624\";i:1;s:6:\"Monday\";i:2;s:1:\"1\";}i:9;a:3:{i:0;s:8:\"20190625\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:10;a:3:{i:0;s:8:\"20190626\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:11;a:3:{i:0;s:8:\"20190627\";i:1;s:8:\"Thursday\";i:2;s:1:\"4\";}i:12;a:3:{i:0;s:8:\"20190628\";i:1;s:6:\"Friday\";i:2;s:1:\"2\";}i:13;a:3:{i:0;s:8:\"20190629\";i:1;s:8:\"Saturday\";i:2;s:1:\"0\";}i:14;a:3:{i:0;s:8:\"20190630\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:15;a:3:{i:0;s:8:\"20190701\";i:1;s:6:\"Monday\";i:2;s:1:\"1\";}i:16;a:3:{i:0;s:8:\"20190702\";i:1;s:7:\"Tuesday\";i:2;s:1:\"0\";}i:17;a:3:{i:0;s:8:\"20190703\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:18;a:3:{i:0;s:8:\"20190704\";i:1;s:8:\"Thursday\";i:2;s:1:\"0\";}i:19;a:3:{i:0;s:8:\"20190705\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:20;a:3:{i:0;s:8:\"20190706\";i:1;s:8:\"Saturday\";i:2;s:1:\"1\";}i:21;a:3:{i:0;s:8:\"20190707\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:22;a:3:{i:0;s:8:\"20190708\";i:1;s:6:\"Monday\";i:2;s:1:\"0\";}i:23;a:3:{i:0;s:8:\"20190709\";i:1;s:7:\"Tuesday\";i:2;s:1:\"1\";}i:24;a:3:{i:0;s:8:\"20190710\";i:1;s:9:\"Wednesday\";i:2;s:1:\"0\";}i:25;a:3:{i:0;s:8:\"20190711\";i:1;s:8:\"Thursday\";i:2;s:1:\"1\";}i:26;a:3:{i:0;s:8:\"20190712\";i:1;s:6:\"Friday\";i:2;s:1:\"0\";}i:27;a:3:{i:0;s:8:\"20190713\";i:1;s:8:\"Saturday\";i:2;s:1:\"1\";}i:28;a:3:{i:0;s:8:\"20190714\";i:1;s:6:\"Sunday\";i:2;s:1:\"0\";}i:29;a:3:{i:0;s:8:\"20190715\";i:1;s:6:\"Monday\";i:2;s:1:\"1\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:160:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&dimensions=ga:date,ga:dayOfWeekName&metrics=ga:sessions&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:30;s:19:\"totalsForAllResults\";a:1:{s:11:\"ga:sessions\";s:2:\"15\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:8:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:10:\"dimensions\";s:24:\"ga:date,ga:dayOfWeekName\";s:7:\"metrics\";a:1:{i:0;s:11:\"ga:sessions\";}s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:3:{i:0;a:3:{s:4:\"name\";s:7:\"ga:date\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:1;a:3:{s:4:\"name\";s:16:\"ga:dayOfWeekName\";s:10:\"columnType\";s:9:\"DIMENSION\";s:8:\"dataType\";s:6:\"STRING\";}i:2;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1563332400;}', 'no'),
(4488, 'gadwp_cache_qr3_1806607268', 'a:2:{s:5:\"value\";O:31:\"Deconf_Service_Analytics_GaData\":24:{s:17:\"\0*\0collection_key\";s:4:\"rows\";s:25:\"\0*\0internal_gapi_mappings\";a:0:{}s:20:\"\0*\0columnHeadersType\";s:44:\"Deconf_Service_Analytics_GaDataColumnHeaders\";s:24:\"\0*\0columnHeadersDataType\";s:5:\"array\";s:19:\"containsSampledData\";b:0;s:16:\"\0*\0dataTableType\";s:40:\"Deconf_Service_Analytics_GaDataDataTable\";s:20:\"\0*\0dataTableDataType\";s:0:\"\";s:2:\"id\";s:260:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:sessions,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:avgSessionDuration&start-date=30daysAgo&end-date=yesterday\";s:12:\"itemsPerPage\";i:1000;s:4:\"kind\";s:16:\"analytics#gaData\";s:8:\"nextLink\";N;s:12:\"previousLink\";N;s:18:\"\0*\0profileInfoType\";s:42:\"Deconf_Service_Analytics_GaDataProfileInfo\";s:22:\"\0*\0profileInfoDataType\";s:0:\"\";s:12:\"\0*\0queryType\";s:36:\"Deconf_Service_Analytics_GaDataQuery\";s:16:\"\0*\0queryDataType\";s:0:\"\";s:4:\"rows\";a:1:{i:0;a:9:{i:0;s:2:\"15\";i:1;s:2:\"14\";i:2;s:2:\"19\";i:3;s:17:\"93.33333333333333\";i:4;s:1:\"0\";i:5;s:18:\"1.2666666666666666\";i:6;s:3:\"8.5\";i:7;s:3:\"0.0\";i:8;s:18:\"2.2666666666666666\";}}s:10:\"sampleSize\";N;s:11:\"sampleSpace\";N;s:8:\"selfLink\";s:260:\"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:190464053&metrics=ga:sessions,ga:users,ga:pageviews,ga:BounceRate,ga:organicSearches,ga:pageviewsPerSession,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:avgSessionDuration&start-date=30daysAgo&end-date=yesterday\";s:12:\"totalResults\";i:1;s:19:\"totalsForAllResults\";a:9:{s:11:\"ga:sessions\";s:2:\"15\";s:8:\"ga:users\";s:2:\"14\";s:12:\"ga:pageviews\";s:2:\"19\";s:13:\"ga:BounceRate\";s:17:\"93.33333333333333\";s:18:\"ga:organicSearches\";s:1:\"0\";s:22:\"ga:pageviewsPerSession\";s:18:\"1.2666666666666666\";s:16:\"ga:avgTimeOnPage\";s:3:\"8.5\";s:18:\"ga:avgPageLoadTime\";s:3:\"0.0\";s:21:\"ga:avgSessionDuration\";s:18:\"2.2666666666666666\";}s:12:\"\0*\0modelData\";a:3:{s:5:\"query\";a:7:{s:10:\"start-date\";s:9:\"30daysAgo\";s:8:\"end-date\";s:9:\"yesterday\";s:3:\"ids\";s:12:\"ga:190464053\";s:7:\"metrics\";a:9:{i:0;s:11:\"ga:sessions\";i:1;s:8:\"ga:users\";i:2;s:12:\"ga:pageviews\";i:3;s:13:\"ga:BounceRate\";i:4;s:18:\"ga:organicSearches\";i:5;s:22:\"ga:pageviewsPerSession\";i:6;s:16:\"ga:avgTimeOnPage\";i:7;s:18:\"ga:avgPageLoadTime\";i:8;s:21:\"ga:avgSessionDuration\";}s:11:\"start-index\";i:1;s:11:\"max-results\";i:1000;s:13:\"samplingLevel\";s:16:\"HIGHER_PRECISION\";}s:11:\"profileInfo\";a:6:{s:9:\"profileId\";s:9:\"190464053\";s:9:\"accountId\";s:9:\"135021161\";s:13:\"webPropertyId\";s:14:\"UA-135021161-1\";s:21:\"internalWebPropertyId\";s:9:\"195204401\";s:11:\"profileName\";s:25:\"Todos os dados do website\";s:7:\"tableId\";s:12:\"ga:190464053\";}s:13:\"columnHeaders\";a:9:{i:0;a:3:{s:4:\"name\";s:11:\"ga:sessions\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:1;a:3:{s:4:\"name\";s:8:\"ga:users\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:2;a:3:{s:4:\"name\";s:12:\"ga:pageviews\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:3;a:3:{s:4:\"name\";s:13:\"ga:BounceRate\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"PERCENT\";}i:4;a:3:{s:4:\"name\";s:18:\"ga:organicSearches\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:7:\"INTEGER\";}i:5;a:3:{s:4:\"name\";s:22:\"ga:pageviewsPerSession\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:6;a:3:{s:4:\"name\";s:16:\"ga:avgTimeOnPage\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:4:\"TIME\";}i:7;a:3:{s:4:\"name\";s:18:\"ga:avgPageLoadTime\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:5:\"FLOAT\";}i:8;a:3:{s:4:\"name\";s:21:\"ga:avgSessionDuration\";s:10:\"columnType\";s:6:\"METRIC\";s:8:\"dataType\";s:4:\"TIME\";}}}s:12:\"\0*\0processed\";a:0:{}}s:7:\"expires\";i:1563332400;}', 'no'),
(4828, '_transient_timeout_aiosp_sitemap_rules_flushed', '1564687612', 'no'),
(4829, '_transient_aiosp_sitemap_rules_flushed', '1', 'no'),
(4831, '_site_transient_timeout_theme_roots', '1564646213', 'no'),
(4832, '_site_transient_theme_roots', 'a:1:{s:6:\"bairro\";s:7:\"/themes\";}', 'no'),
(4833, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1564644413;s:7:\"checked\";a:18:{s:25:\"add-to-any/add-to-any.php\";s:6:\"1.7.34\";s:51:\"codepress-admin-columns/codepress-admin-columns.php\";s:5:\"3.4.1\";s:30:\"advanced-custom-fields/acf.php\";s:6:\"4.4.12\";s:37:\"acf-options-page/acf-options-page.php\";s:5:\"2.1.0\";s:29:\"acf-repeater/acf-repeater.php\";s:5:\"2.1.0\";s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";s:4:\"2.11\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:7:\"4.3.8.3\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.4\";s:32:\"disqus-comment-system/disqus.php\";s:6:\"3.0.16\";s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";s:5:\"5.3.7\";s:43:\"insert-post-external/InsertPostExternal.php\";s:3:\"1.5\";s:59:\"intuitive-custom-post-order/intuitive-custom-post-order.php\";s:5:\"3.1.1\";s:23:\"lazy-load/lazy-load.php\";s:5:\"0.6.1\";s:24:\"modulo-banner/banner.php\";s:3:\"1.0\";s:55:\"resize-image-after-upload/resize-image-after-upload.php\";s:5:\"1.8.5\";s:41:\"wordpress-importer/wordpress-importer.php\";s:5:\"0.6.4\";s:35:\"wp-fastest-cache/wpFastestCache.php\";s:7:\"0.8.9.1\";s:25:\"wp-toolkit/wp-toolkit.php\";s:5:\"0.0.5\";}s:8:\"response\";a:10:{s:25:\"add-to-any/add-to-any.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:24:\"w.org/plugins/add-to-any\";s:4:\"slug\";s:10:\"add-to-any\";s:6:\"plugin\";s:25:\"add-to-any/add-to-any.php\";s:11:\"new_version\";s:6:\"1.7.36\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/add-to-any/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/add-to-any.1.7.36.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/add-to-any/assets/icon-256x256.png?rev=972738\";s:2:\"1x\";s:54:\"https://ps.w.org/add-to-any/assets/icon.svg?rev=972738\";s:3:\"svg\";s:54:\"https://ps.w.org/add-to-any/assets/icon.svg?rev=972738\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/add-to-any/assets/banner-1544x500.png?rev=2037442\";s:2:\"1x\";s:65:\"https://ps.w.org/add-to-any/assets/banner-772x250.png?rev=2037440\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.2\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.2\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/all-in-one-seo-pack\";s:4:\"slug\";s:19:\"all-in-one-seo-pack\";s:6:\"plugin\";s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";s:11:\"new_version\";s:5:\"3.1.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/all-in-one-seo-pack/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/all-in-one-seo-pack.3.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/all-in-one-seo-pack/assets/icon-256x256.png?rev=2075006\";s:2:\"1x\";s:72:\"https://ps.w.org/all-in-one-seo-pack/assets/icon-128x128.png?rev=2075006\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/all-in-one-seo-pack/assets/banner-1544x500.png?rev=1354894\";s:2:\"1x\";s:74:\"https://ps.w.org/all-in-one-seo-pack/assets/banner-772x250.png?rev=1354894\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:7:\"4.3.9.4\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:6:\"3.0.17\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.17.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:2:\"1x\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.1\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:47:\"w.org/plugins/google-analytics-dashboard-for-wp\";s:4:\"slug\";s:33:\"google-analytics-dashboard-for-wp\";s:6:\"plugin\";s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";s:11:\"new_version\";s:5:\"5.3.8\";s:3:\"url\";s:64:\"https://wordpress.org/plugins/google-analytics-dashboard-for-wp/\";s:7:\"package\";s:82:\"https://downloads.wordpress.org/plugin/google-analytics-dashboard-for-wp.5.3.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:85:\"https://ps.w.org/google-analytics-dashboard-for-wp/assets/icon-256x256.png?rev=970326\";s:2:\"1x\";s:85:\"https://ps.w.org/google-analytics-dashboard-for-wp/assets/icon-128x128.png?rev=970326\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/google-analytics-dashboard-for-wp/assets/banner-772x250.png?rev=1064664\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:59:\"intuitive-custom-post-order/intuitive-custom-post-order.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:41:\"w.org/plugins/intuitive-custom-post-order\";s:4:\"slug\";s:27:\"intuitive-custom-post-order\";s:6:\"plugin\";s:59:\"intuitive-custom-post-order/intuitive-custom-post-order.php\";s:11:\"new_version\";s:5:\"3.1.2\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/intuitive-custom-post-order/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/intuitive-custom-post-order.3.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/intuitive-custom-post-order/assets/icon-256x256.png?rev=1078797\";s:2:\"1x\";s:80:\"https://ps.w.org/intuitive-custom-post-order/assets/icon-128x128.png?rev=1078797\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:83:\"https://ps.w.org/intuitive-custom-post-order/assets/banner-1544x500.png?rev=1209666\";s:2:\"1x\";s:82:\"https://ps.w.org/intuitive-custom-post-order/assets/banner-772x250.png?rev=1078755\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.1.0\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:55:\"resize-image-after-upload/resize-image-after-upload.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:39:\"w.org/plugins/resize-image-after-upload\";s:4:\"slug\";s:25:\"resize-image-after-upload\";s:6:\"plugin\";s:55:\"resize-image-after-upload/resize-image-after-upload.php\";s:11:\"new_version\";s:5:\"1.8.6\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/resize-image-after-upload/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/resize-image-after-upload.1.8.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/resize-image-after-upload/assets/icon-256x256.png?rev=1940740\";s:2:\"1x\";s:78:\"https://ps.w.org/resize-image-after-upload/assets/icon-128x128.png?rev=1940740\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/resize-image-after-upload/assets/banner-1544x500.png?rev=1940740\";s:2:\"1x\";s:80:\"https://ps.w.org/resize-image-after-upload/assets/banner-772x250.png?rev=1940740\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.1.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:35:\"wp-fastest-cache/wpFastestCache.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/wp-fastest-cache\";s:4:\"slug\";s:16:\"wp-fastest-cache\";s:6:\"plugin\";s:35:\"wp-fastest-cache/wpFastestCache.php\";s:11:\"new_version\";s:7:\"0.8.9.6\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/wp-fastest-cache/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/wp-fastest-cache.0.8.9.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wp-fastest-cache/assets/icon-256x256.png?rev=2064586\";s:2:\"1x\";s:69:\"https://ps.w.org/wp-fastest-cache/assets/icon-128x128.png?rev=1068904\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:71:\"https://ps.w.org/wp-fastest-cache/assets/banner-772x250.jpg?rev=1064099\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:51:\"codepress-admin-columns/codepress-admin-columns.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/codepress-admin-columns\";s:4:\"slug\";s:23:\"codepress-admin-columns\";s:6:\"plugin\";s:51:\"codepress-admin-columns/codepress-admin-columns.php\";s:11:\"new_version\";s:5:\"3.4.1\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/codepress-admin-columns/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/codepress-admin-columns.3.4.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:76:\"https://ps.w.org/codepress-admin-columns/assets/icon-256x256.png?rev=1521754\";s:2:\"1x\";s:68:\"https://ps.w.org/codepress-admin-columns/assets/icon.svg?rev=1521754\";s:3:\"svg\";s:68:\"https://ps.w.org/codepress-admin-columns/assets/icon.svg?rev=1521754\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/codepress-admin-columns/assets/banner-1544x500.png?rev=1220017\";s:2:\"1x\";s:78:\"https://ps.w.org/codepress-admin-columns/assets/banner-772x250.png?rev=1220017\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"lazy-load/lazy-load.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/lazy-load\";s:4:\"slug\";s:9:\"lazy-load\";s:6:\"plugin\";s:23:\"lazy-load/lazy-load.php\";s:11:\"new_version\";s:5:\"0.6.1\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/lazy-load/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/lazy-load.0.6.1.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:53:\"https://s.w.org/plugins/geopattern-icon/lazy-load.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:41:\"wordpress-importer/wordpress-importer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wordpress-importer\";s:4:\"slug\";s:18:\"wordpress-importer\";s:6:\"plugin\";s:41:\"wordpress-importer/wordpress-importer.php\";s:11:\"new_version\";s:5:\"0.6.4\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wordpress-importer/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/wordpress-importer.0.6.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wordpress-importer/assets/icon-256x256.png?rev=1908375\";s:2:\"1x\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";s:3:\"svg\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-importer/assets/banner-772x250.png?rev=547654\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_postmeta`
--

CREATE TABLE `bai_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_postmeta`
--

INSERT INTO `bai_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(8, 5, '_menu_item_type', 'custom'),
(9, 5, '_menu_item_menu_item_parent', '0'),
(10, 5, '_menu_item_object_id', '5'),
(11, 5, '_menu_item_object', 'custom'),
(12, 5, '_menu_item_target', ''),
(13, 5, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(14, 5, '_menu_item_xfn', ''),
(15, 5, '_menu_item_url', 'http://omeubairro.online/'),
(26, 7, '_edit_last', '1'),
(27, 7, '_wp_page_template', 'default'),
(28, 7, '_edit_lock', '1550672655:1'),
(29, 9, '_edit_last', '1'),
(30, 9, '_wp_page_template', 'default'),
(31, 9, '_edit_lock', '1551372640:1'),
(32, 11, '_edit_last', '1'),
(33, 11, '_edit_lock', '1551372975:1'),
(34, 11, '_wp_page_template', 'default'),
(35, 13, '_menu_item_type', 'post_type'),
(36, 13, '_menu_item_menu_item_parent', '0'),
(37, 13, '_menu_item_object_id', '11'),
(38, 13, '_menu_item_object', 'page'),
(39, 13, '_menu_item_target', ''),
(40, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(41, 13, '_menu_item_xfn', ''),
(42, 13, '_menu_item_url', ''),
(44, 14, '_menu_item_type', 'post_type'),
(45, 14, '_menu_item_menu_item_parent', '0'),
(46, 14, '_menu_item_object_id', '9'),
(47, 14, '_menu_item_object', 'page'),
(48, 14, '_menu_item_target', ''),
(49, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(50, 14, '_menu_item_xfn', ''),
(51, 14, '_menu_item_url', ''),
(53, 15, '_menu_item_type', 'post_type'),
(54, 15, '_menu_item_menu_item_parent', '0'),
(55, 15, '_menu_item_object_id', '7'),
(56, 15, '_menu_item_object', 'page'),
(57, 15, '_menu_item_target', ''),
(58, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(59, 15, '_menu_item_xfn', ''),
(60, 15, '_menu_item_url', ''),
(62, 16, '_edit_last', '1'),
(63, 16, '_edit_lock', '1551984117:1'),
(64, 17, '_wp_attached_file', '2018/11/problemas-do-meu-bairro-painel.png'),
(65, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:497;s:4:\"file\";s:42:\"2018/11/problemas-do-meu-bairro-painel.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"problemas-do-meu-bairro-painel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(66, 16, '_thumbnail_id', '738'),
(67, 16, 'ativar_link', ''),
(68, 16, '_ativar_link', 'field_5620f3ce78b14'),
(69, 16, 'views_banner_main', '1959'),
(72, 463, '_edit_last', '1'),
(73, 463, 'field_56d733bf82c60', 'a:8:{s:3:\"key\";s:19:\"field_56d733bf82c60\";s:5:\"label\";s:13:\"Informações\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(74, 463, 'position', 'normal'),
(75, 463, 'layout', 'default'),
(76, 463, 'hide_on_screen', ''),
(77, 463, 'field_56d7398722591', 'a:8:{s:3:\"key\";s:19:\"field_56d7398722591\";s:5:\"label\";s:8:\"Páginas\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(78, 463, 'field_56d739f522592', 'a:11:{s:3:\"key\";s:19:\"field_56d739f522592\";s:5:\"label\";s:32:\"Foto destaque de página interna\";s:4:\"name\";s:31:\"foto_destaque_de_pagina_interna\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(79, 463, 'field_57f79211842ed', 'a:8:{s:3:\"key\";s:19:\"field_57f79211842ed\";s:5:\"label\";s:10:\"Cabeçalho\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(80, 463, '_edit_last', '1'),
(81, 463, 'field_56d733bf82c60', 'a:8:{s:3:\"key\";s:19:\"field_56d733bf82c60\";s:5:\"label\";s:13:\"Informações\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(82, 463, 'position', 'normal'),
(83, 463, 'layout', 'default'),
(84, 463, 'hide_on_screen', ''),
(85, 463, 'field_56d7398722591', 'a:8:{s:3:\"key\";s:19:\"field_56d7398722591\";s:5:\"label\";s:8:\"Páginas\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(86, 463, 'field_56d739f522592', 'a:11:{s:3:\"key\";s:19:\"field_56d739f522592\";s:5:\"label\";s:32:\"Foto destaque de página interna\";s:4:\"name\";s:31:\"foto_destaque_de_pagina_interna\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(87, 463, 'field_57f79211842ed', 'a:8:{s:3:\"key\";s:19:\"field_57f79211842ed\";s:5:\"label\";s:10:\"Cabeçalho\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(88, 463, 'field_57fd180f04c49', 'a:8:{s:3:\"key\";s:19:\"field_57fd180f04c49\";s:5:\"label\";s:7:\"Rodapé\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}'),
(89, 463, 'field_5804d84419004', 'a:11:{s:3:\"key\";s:19:\"field_5804d84419004\";s:5:\"label\";s:4:\"Logo\";s:4:\"name\";s:4:\"logo\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:41:\"Tamanho: 94 x 96\r\nFormato: PNG 24 ou JPEG\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(90, 463, 'field_5804d82219002', 'a:8:{s:3:\"key\";s:19:\"field_5804d82219002\";s:5:\"label\";s:15:\"Página contato\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}'),
(92, 463, '_edit_last', '1'),
(93, 463, 'field_56d733bf82c60', 'a:8:{s:3:\"key\";s:19:\"field_56d733bf82c60\";s:5:\"label\";s:13:\"Informações\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(94, 463, 'position', 'normal'),
(95, 463, 'layout', 'default'),
(96, 463, 'hide_on_screen', ''),
(97, 463, 'field_56d7398722591', 'a:8:{s:3:\"key\";s:19:\"field_56d7398722591\";s:5:\"label\";s:8:\"Páginas\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(98, 463, 'field_56d739f522592', 'a:11:{s:3:\"key\";s:19:\"field_56d739f522592\";s:5:\"label\";s:32:\"Foto destaque de página interna\";s:4:\"name\";s:31:\"foto_destaque_de_pagina_interna\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(99, 463, 'field_57f79211842ed', 'a:8:{s:3:\"key\";s:19:\"field_57f79211842ed\";s:5:\"label\";s:10:\"Cabeçalho\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(100, 463, '_edit_last', '1'),
(101, 463, 'field_56d733bf82c60', 'a:8:{s:3:\"key\";s:19:\"field_56d733bf82c60\";s:5:\"label\";s:13:\"Informações\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(102, 463, 'position', 'normal'),
(103, 463, 'layout', 'default'),
(104, 463, 'hide_on_screen', ''),
(105, 463, 'field_56d7398722591', 'a:8:{s:3:\"key\";s:19:\"field_56d7398722591\";s:5:\"label\";s:8:\"Páginas\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:6;}'),
(106, 463, 'field_56d739f522592', 'a:11:{s:3:\"key\";s:19:\"field_56d739f522592\";s:5:\"label\";s:32:\"Foto destaque de página interna\";s:4:\"name\";s:31:\"foto_destaque_de_pagina_interna\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:7;}'),
(107, 463, 'field_57f79211842ed', 'a:8:{s:3:\"key\";s:19:\"field_57f79211842ed\";s:5:\"label\";s:10:\"Cabeçalho\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(108, 463, 'field_57fd180f04c49', 'a:8:{s:3:\"key\";s:19:\"field_57fd180f04c49\";s:5:\"label\";s:7:\"Rodapé\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:8;}'),
(109, 463, 'field_5804d84419004', 'a:11:{s:3:\"key\";s:19:\"field_5804d84419004\";s:5:\"label\";s:4:\"Logo\";s:4:\"name\";s:4:\"logo\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:41:\"Tamanho: 94 x 96\r\nFormato: PNG 24 ou JPEG\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:6:\"object\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(110, 463, 'field_5804d82219002', 'a:8:{s:3:\"key\";s:19:\"field_5804d82219002\";s:5:\"label\";s:15:\"Página contato\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:9;}'),
(112, 463, 'field_581cda55a83c1', 'a:14:{s:3:\"key\";s:19:\"field_581cda55a83c1\";s:5:\"label\";s:8:\"Facebook\";s:4:\"name\";s:8:\"facebook\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(113, 463, 'field_581cda60a83c2', 'a:14:{s:3:\"key\";s:19:\"field_581cda60a83c2\";s:5:\"label\";s:9:\"Instagram\";s:4:\"name\";s:9:\"instagram\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(122, 463, 'field_5857e73c7caaf', 'a:13:{s:3:\"key\";s:19:\"field_5857e73c7caaf\";s:5:\"label\";s:27:\"Informações do cabeçalho\";s:4:\"name\";s:24:\"informacoes_do_cabecalho\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:5:{i:0;a:15:{s:3:\"key\";s:19:\"field_5857e7929ff5a\";s:5:\"label\";s:8:\"Whatsapp\";s:4:\"name\";s:8:\"whatsapp\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5857e7c39ff5b\";s:5:\"label\";s:6:\"E-mail\";s:4:\"name\";s:6:\"e-mail\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}i:2;a:14:{s:3:\"key\";s:19:\"field_5857e9d49ff5c\";s:5:\"label\";s:25:\"Horário de funcionamento\";s:4:\"name\";s:24:\"horario_de_funcionamento\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:10:\"sub_fields\";a:2:{i:0;a:15:{s:3:\"key\";s:19:\"field_5857e9e49ff5d\";s:5:\"label\";s:6:\"Titulo\";s:4:\"name\";s:6:\"titulo\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:14:{s:3:\"key\";s:19:\"field_5b9a9d2680cd6\";s:5:\"label\";s:17:\"Campos adicionais\";s:4:\"name\";s:17:\"campos_adicionais\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:10:\"sub_fields\";a:1:{i:0;a:15:{s:3:\"key\";s:19:\"field_5b9a9f9211fea\";s:5:\"label\";s:15:\"Campo Adicional\";s:4:\"name\";s:15:\"campo_adicional\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Adicionar\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Adicionar\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}i:3;a:15:{s:3:\"key\";s:19:\"field_5b9bc5c4be190\";s:5:\"label\";s:8:\"Telefone\";s:4:\"name\";s:8:\"telefone\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}i:4;a:15:{s:3:\"key\";s:19:\"field_5bae271efa223\";s:5:\"label\";s:8:\"Whatsapp\";s:4:\"name\";s:8:\"whatsapp\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Adicionar\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(125, 464, '_wp_attached_file', '2018/11/problemas-do-meu-bairro-logo.png'),
(126, 464, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:270;s:6:\"height\";i:100;s:4:\"file\";s:40:\"2018/11/problemas-do-meu-bairro-logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"problemas-do-meu-bairro-logo-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(129, 471, '_edit_last', '1'),
(130, 471, 'field_581884a0a1eab', 'a:14:{s:3:\"key\";s:19:\"field_581884a0a1eab\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:9:\"descricao\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:25:\"Digite o texto do painel.\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(131, 471, 'position', 'normal'),
(132, 471, 'layout', 'default'),
(133, 471, 'hide_on_screen', ''),
(134, 471, 'field_581893e7c3d25', 'a:13:{s:3:\"key\";s:19:\"field_581893e7c3d25\";s:5:\"label\";s:22:\"Descrição secundaria\";s:4:\"name\";s:13:\"descricao_sec\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(135, 471, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"banner\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(136, 16, 'descricao', 'Um portal onde pessoas de forma colaborativa ajudam a identificar e resolver problemas de saneamento básico, lixo, asfalto, entre outros problemas'),
(137, 16, '_descricao', 'field_581884a0a1eab'),
(138, 16, 'descricao_sec', ''),
(139, 16, '_descricao_sec', 'field_581893e7c3d25'),
(140, 471, '_edit_lock', '1542032185:1'),
(143, 463, '_edit_lock', '1551983058:1'),
(153, 476, '_edit_last', '1'),
(154, 476, 'field_5c3f3b98d89ab', 'a:12:{s:3:\"key\";s:19:\"field_5c3f3b98d89ab\";s:5:\"label\";s:19:\"Problema resolvido?\";s:4:\"name\";s:8:\"problema\";s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:7:\"choices\";a:2:{s:3:\"sim\";s:3:\"sim\";s:3:\"nao\";s:4:\"não\";}s:13:\"default_value\";s:0:\"\";s:10:\"allow_null\";s:1:\"0\";s:8:\"multiple\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(156, 476, 'position', 'normal'),
(157, 476, 'layout', 'no_box'),
(158, 476, 'hide_on_screen', ''),
(159, 476, '_edit_lock', '1550687113:1'),
(178, 480, '_edit_last', '1'),
(179, 480, 'field_5c3f42c60d95f', 'a:14:{s:3:\"key\";s:19:\"field_5c3f42c60d95f\";s:5:\"label\";s:6:\"Bairro\";s:4:\"name\";s:6:\"bairro\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(180, 480, 'field_5c3f42cf0d960', 'a:14:{s:3:\"key\";s:19:\"field_5c3f42cf0d960\";s:5:\"label\";s:4:\"Tipo\";s:4:\"name\";s:4:\"tipo\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(181, 480, 'field_5c3f42de0d961', 'a:14:{s:3:\"key\";s:19:\"field_5c3f42de0d961\";s:5:\"label\";s:3:\"Tag\";s:4:\"name\";s:3:\"tag\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(182, 480, 'field_5c3f42e60d962', 'a:14:{s:3:\"key\";s:19:\"field_5c3f42e60d962\";s:5:\"label\";s:11:\"Solucionado\";s:4:\"name\";s:11:\"solucionado\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(183, 480, 'field_5c3f42ed0d963', 'a:14:{s:3:\"key\";s:19:\"field_5c3f42ed0d963\";s:5:\"label\";s:10:\"Recorrente\";s:4:\"name\";s:10:\"recorrente\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(185, 480, 'position', 'normal'),
(186, 480, 'layout', 'no_box'),
(187, 480, 'hide_on_screen', ''),
(188, 480, '_edit_lock', '1550671678:1'),
(219, 484, '_edit_last', '1'),
(220, 484, '_edit_lock', '1551110738:1'),
(236, 489, '_edit_last', '1'),
(238, 489, 'field_5c6d5f4323af2', 'a:14:{s:3:\"key\";s:19:\"field_5c6d5f4323af2\";s:5:\"label\";s:15:\"Numero ou skype\";s:4:\"name\";s:12:\"numero_skype\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(239, 489, 'field_5c6d5f4c23af3', 'a:14:{s:3:\"key\";s:19:\"field_5c6d5f4c23af3\";s:5:\"label\";s:10:\"Recorrente\";s:4:\"name\";s:10:\"recorrente\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(241, 489, 'position', 'normal'),
(242, 489, 'layout', 'default'),
(243, 489, 'hide_on_screen', ''),
(244, 489, '_edit_lock', '1551359917:1'),
(247, 480, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(335, 476, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(501, 535, 'email', 'diegocuruma@yahoo.com'),
(502, 535, 'telefone', '(85) 98717-2112'),
(503, 535, 'descricao', 'cdscs'),
(504, 535, 'denuncia_anonima', 'Não'),
(505, 535, 'bairro', 'Maraponga'),
(506, 535, 'recorrente', 'Sim'),
(507, 535, '_edit_last', '1'),
(508, 535, '_edit_lock', '1550678631:1'),
(509, 536, 'email', 'diegocuruma@yahoo.com'),
(510, 536, 'telefone', '(85) 98717-2112'),
(511, 536, 'descricao', 'vsvsa'),
(512, 536, '_edit_lock', '1550678805:1'),
(513, 537, 'email', 'diegocuruma@yahoo.com'),
(514, 537, 'telefone', '(85) 98717-2112'),
(515, 537, 'descricao', 'vsvsa'),
(516, 538, 'email', 'diegocuruma@gmail.com'),
(517, 538, 'telefone', '(85) 98717-2112'),
(518, 538, 'descricao', 'vdsavsa'),
(519, 538, '_edit_lock', '1550678961:1'),
(523, 540, 'email', 'diegocuruma@yahoo.com'),
(524, 540, 'telefone', '(85) 98717-2112'),
(525, 540, 'descricao', 'Buracos'),
(529, 540, '_edit_lock', '1550679106:1'),
(530, 542, 'email', 'diegocuruma@yahoo.com'),
(531, 542, 'telefone', '(85) 98717-2112'),
(532, 542, 'descricao', 'Buracos resolvidos mas é recorrente'),
(536, 544, 'email', 'diegocuruma@yahoo.com'),
(537, 544, 'telefone', '(85) 98717-2112'),
(538, 544, 'descricao', 'Lixo'),
(539, 544, '_edit_lock', '1550679113:1'),
(540, 542, '_edit_lock', '1550679118:1'),
(544, 545, 'email', 'diegocuruma@yahoo.com'),
(545, 545, 'telefone', '(85) 98717-2112'),
(546, 545, 'descricao', 'Lixo'),
(547, 545, '_edit_last', '1'),
(548, 545, '_edit_lock', '1550681637:1'),
(549, 546, '_edit_last', '1'),
(550, 546, '_edit_lock', '1550682076:1'),
(551, 547, 'email', 'diegocuruma@yahoo.com'),
(552, 547, 'telefone', '(85) 98717-2112'),
(553, 547, 'descricao', 'bláblá'),
(640, 489, 'field_5c6d8e9620a26', 'a:14:{s:3:\"key\";s:19:\"field_5c6d8e9620a26\";s:5:\"label\";s:6:\"E-mail\";s:4:\"name\";s:5:\"email\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(698, 489, 'field_5c6d922479a1a', 'a:14:{s:3:\"key\";s:19:\"field_5c6d922479a1a\";s:5:\"label\";s:22:\"A denuncia é anonima?\";s:4:\"name\";s:20:\"a_denuncia_e_anonima\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(838, 489, 'field_5c6d97d6d8ff8', 'a:14:{s:3:\"key\";s:19:\"field_5c6d97d6d8ff8\";s:5:\"label\";s:3:\"Rua\";s:4:\"name\";s:3:\"rua\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(846, 603, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-15272054105b074e2201d69_1527205410_3x2_md.jpg'),
(847, 603, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:768;s:6:\"height\";i:512;s:4:\"file\";s:77:\"2019/02/problemas-do-meu-bairro-15272054105b074e2201d69_1527205410_3x2_md.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:77:\"problemas-do-meu-bairro-15272054105b074e2201d69_1527205410_3x2_md-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1126, 463, 'field_5c740a70abe74', 'a:13:{s:3:\"key\";s:19:\"field_5c740a70abe74\";s:5:\"label\";s:15:\"Desenvolvedores\";s:4:\"name\";s:15:\"desenvolvedores\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:12:{s:3:\"key\";s:19:\"field_5c740a7dabe75\";s:5:\"label\";s:6:\"imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5c81627523323\";s:5:\"label\";s:4:\"Nome\";s:4:\"name\";s:4:\"nome\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Adicionar\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:10;}'),
(1136, 633, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-jonas.jpg'),
(1137, 633, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:41:\"2019/02/problemas-do-meu-bairro-jonas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1138, 463, 'field_5c740d8b97881', 'a:13:{s:3:\"key\";s:19:\"field_5c740d8b97881\";s:5:\"label\";s:5:\"Apoio\";s:4:\"name\";s:5:\"apoio\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:1:{i:0;a:12:{s:3:\"key\";s:19:\"field_5c740d8b97882\";s:5:\"label\";s:6:\"imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:9:\"Adicionar\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:11;}'),
(1150, 636, 'email', 'diegocuruma@yahoo.com'),
(1151, 636, 'telefone', ''),
(1152, 636, 'descricao', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.'),
(1153, 636, 'recorrente', 'Sim'),
(1154, 636, 'a_denuncia_e_anonima', ''),
(1155, 636, 'nome_denunciante', 'Diego'),
(1156, 636, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1159, 636, '_thumbnail_id', '677'),
(1160, 636, '_edit_lock', '1551963037:1'),
(1161, 636, '_edit_last', '1'),
(1164, 638, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1165, 638, '_rua', 'field_5c6d97d6d8ff8'),
(1166, 638, 'a_denuncia_e_anonima', ''),
(1167, 638, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1168, 638, 'nome_denunciante', 'Diego'),
(1169, 638, '_nome_denunciante', 'field_5c6d5f4323af2'),
(1170, 638, 'recorrente', 'Sim'),
(1171, 638, '_recorrente', 'field_5c6d5f4c23af3'),
(1172, 638, 'email', 'diegocuruma@yahoo.com'),
(1173, 638, '_email', 'field_5c6d8e9620a26'),
(1174, 638, 'problema', 'sim'),
(1175, 638, '_problema', 'field_5c3f3b98d89ab'),
(1176, 636, '_rua', 'field_5c6d97d6d8ff8'),
(1177, 636, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1178, 636, '_nome_denunciante', 'field_5c6d5f4323af2'),
(1179, 636, '_recorrente', 'field_5c6d5f4c23af3'),
(1180, 636, '_email', 'field_5c6d8e9620a26'),
(1181, 636, 'problema', 'nao'),
(1182, 636, '_problema', 'field_5c3f3b98d89ab'),
(1185, 639, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1186, 639, '_rua', 'field_5c6d97d6d8ff8'),
(1187, 639, 'a_denuncia_e_anonima', ''),
(1188, 639, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1189, 639, 'nome_denunciante', 'Diego'),
(1190, 639, '_nome_denunciante', 'field_5c6d5f4323af2'),
(1191, 639, 'recorrente', 'Sim'),
(1192, 639, '_recorrente', 'field_5c6d5f4c23af3'),
(1193, 639, 'email', 'diegocuruma@yahoo.com'),
(1194, 639, '_email', 'field_5c6d8e9620a26'),
(1195, 639, 'problema', 'nao'),
(1196, 639, '_problema', 'field_5c3f3b98d89ab'),
(1197, 2, '_edit_last', '1'),
(1198, 2, '_edit_lock', '1551110889:1'),
(1201, 646, '_edit_last', '1'),
(1202, 646, '_edit_lock', '1551372943:1'),
(1203, 646, '_wp_page_template', 'default'),
(1204, 648, '_menu_item_type', 'post_type'),
(1205, 648, '_menu_item_menu_item_parent', '0'),
(1206, 648, '_menu_item_object_id', '646'),
(1207, 648, '_menu_item_object', 'page'),
(1208, 648, '_menu_item_target', ''),
(1209, 648, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1210, 648, '_menu_item_xfn', ''),
(1211, 648, '_menu_item_url', ''),
(1216, 654, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-img-20190227-wa0012.jpg'),
(1217, 654, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:674;s:6:\"height\";i:1200;s:4:\"file\";s:55:\"2019/02/problemas-do-meu-bairro-img-20190227-wa0012.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:55:\"problemas-do-meu-bairro-img-20190227-wa0012-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1220, 655, 'email', 'diegocuruma@gmail.com'),
(1221, 655, 'telefone', ''),
(1222, 655, 'descricao', 'Ná Av. Oliveira Paiva próximo a Av. Hermínio de Castro, terreno baldio com grande quantidade de lixo a céu aberto, pneus, foco de doenças domo dengue e etc..\r\n'),
(1223, 655, 'recorrente', 'Sim'),
(1224, 655, 'a_denuncia_e_anonima', 'Diego'),
(1225, 655, 'nome_denunciante', 'Diego'),
(1226, 655, 'rua', 'Av. Oliveira Paiva, 2235'),
(1227, 656, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-whatsapp-image-2019-02-28-at-08-54-13.jpeg'),
(1228, 656, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:900;s:4:\"file\";s:74:\"2019/02/problemas-do-meu-bairro-whatsapp-image-2019-02-28-at-08-54-13.jpeg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:74:\"problemas-do-meu-bairro-whatsapp-image-2019-02-28-at-08-54-13-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1229, 655, '_thumbnail_id', '656'),
(1230, 655, '_edit_lock', '1551963015:1'),
(1231, 655, '_edit_last', '1'),
(1234, 657, 'rua', 'Av. Oliveira Paiva, 2235'),
(1235, 657, '_rua', 'field_5c6d97d6d8ff8'),
(1236, 657, 'a_denuncia_e_anonima', ''),
(1237, 657, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1238, 657, 'nome_denunciante', 'Diego'),
(1239, 657, '_nome_denunciante', 'field_5c6d5f4323af2'),
(1240, 657, 'recorrente', 'Sim'),
(1241, 657, '_recorrente', 'field_5c6d5f4c23af3'),
(1242, 657, 'email', 'diegocuruma@gmail.com'),
(1243, 657, '_email', 'field_5c6d8e9620a26'),
(1244, 657, 'problema', 'nao'),
(1245, 657, '_problema', 'field_5c3f3b98d89ab'),
(1246, 655, '_rua', 'field_5c6d97d6d8ff8'),
(1247, 655, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1248, 655, '_nome_denunciante', 'field_5c6d5f4323af2'),
(1249, 655, '_recorrente', 'field_5c6d5f4c23af3'),
(1250, 655, '_email', 'field_5c6d8e9620a26'),
(1251, 655, 'problema', 'nao'),
(1252, 655, '_problema', 'field_5c3f3b98d89ab'),
(1413, 675, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-problemas-do-meu-bairro-img-20190227-wa0012.jpg'),
(1414, 675, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:124;s:6:\"height\";i:220;s:4:\"file\";s:79:\"2019/02/problemas-do-meu-bairro-problemas-do-meu-bairro-img-20190227-wa0012.jpg\";s:5:\"sizes\";a:1:{s:4:\"capa\";a:4:{s:4:\"file\";s:79:\"problemas-do-meu-bairro-problemas-do-meu-bairro-img-20190227-wa0012-124x220.jpg\";s:5:\"width\";i:124;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1417, 676, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-maraponga-lixo.jpg'),
(1418, 676, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:124;s:6:\"height\";i:220;s:4:\"file\";s:50:\"2019/02/problemas-do-meu-bairro-maraponga-lixo.jpg\";s:5:\"sizes\";a:1:{s:4:\"capa\";a:4:{s:4:\"file\";s:50:\"problemas-do-meu-bairro-maraponga-lixo-124x220.jpg\";s:5:\"width\";i:124;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1419, 677, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-maraponga-lixo-1.jpg'),
(1420, 677, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:330;s:6:\"height\";i:220;s:4:\"file\";s:52:\"2019/02/problemas-do-meu-bairro-maraponga-lixo-1.jpg\";s:5:\"sizes\";a:1:{s:4:\"capa\";a:4:{s:4:\"file\";s:52:\"problemas-do-meu-bairro-maraponga-lixo-1-330x220.jpg\";s:5:\"width\";i:330;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1425, 678, 'rua', 'Av. Oliveira Paiva, 2235'),
(1426, 678, '_rua', 'field_5c6d97d6d8ff8'),
(1427, 678, 'a_denuncia_e_anonima', ''),
(1428, 678, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1429, 678, 'nome_denunciante', 'Diego'),
(1430, 678, '_nome_denunciante', 'field_5c6d5f4323af2'),
(1431, 678, 'recorrente', 'Sim'),
(1432, 678, '_recorrente', 'field_5c6d5f4c23af3'),
(1433, 678, 'email', 'diegocuruma@gmail.com'),
(1434, 678, '_email', 'field_5c6d8e9620a26'),
(1435, 678, 'problema', 'nao'),
(1436, 678, '_problema', 'field_5c3f3b98d89ab'),
(1437, 489, 'field_5c77e0345e3e8', 'a:14:{s:3:\"key\";s:19:\"field_5c77e0345e3e8\";s:5:\"label\";s:6:\"Cidade\";s:4:\"name\";s:6:\"cidade\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(1438, 489, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(1447, 680, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-foto-galeria-600x400.jpg'),
(1448, 680, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:330;s:6:\"height\";i:220;s:4:\"file\";s:56:\"2019/02/problemas-do-meu-bairro-foto-galeria-600x400.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:56:\"problemas-do-meu-bairro-foto-galeria-600x400-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1478, 682, 'email', 'diegocuruma@yahoo.com'),
(1479, 682, '_email', 'field_5c6d8e9620a26'),
(1480, 682, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1481, 682, '_rua', 'field_5c6d97d6d8ff8'),
(1482, 682, 'cidade', ''),
(1483, 682, '_cidade', 'field_5c77e0345e3e8'),
(1484, 682, 'numero_skype', '85 98175.8310'),
(1485, 682, '_numero_skype', 'field_5c6d5f4323af2'),
(1486, 682, 'a_denuncia_e_anonima', ''),
(1487, 682, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1488, 682, 'recorrente', 'Sim'),
(1489, 682, '_recorrente', 'field_5c6d5f4c23af3'),
(1490, 682, 'problema', 'nao'),
(1491, 682, '_problema', 'field_5c3f3b98d89ab'),
(1492, 636, 'cidade', ''),
(1493, 636, '_cidade', 'field_5c77e0345e3e8'),
(1494, 636, 'numero_skype', ''),
(1495, 636, '_numero_skype', 'field_5c6d5f4323af2'),
(1498, 683, 'email', 'diegocuruma@yahoo.com'),
(1499, 683, '_email', 'field_5c6d8e9620a26'),
(1500, 683, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1501, 683, '_rua', 'field_5c6d97d6d8ff8'),
(1502, 683, 'cidade', ''),
(1503, 683, '_cidade', 'field_5c77e0345e3e8'),
(1504, 683, 'numero_skype', ''),
(1505, 683, '_numero_skype', 'field_5c6d5f4323af2'),
(1506, 683, 'a_denuncia_e_anonima', ''),
(1507, 683, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1508, 683, 'recorrente', 'Sim'),
(1509, 683, '_recorrente', 'field_5c6d5f4c23af3'),
(1510, 683, 'problema', 'nao'),
(1511, 683, '_problema', 'field_5c3f3b98d89ab'),
(1514, 684, 'email', 'diegocuruma@yahoo.com'),
(1515, 684, '_email', 'field_5c6d8e9620a26'),
(1516, 684, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1517, 684, '_rua', 'field_5c6d97d6d8ff8'),
(1518, 684, 'cidade', ''),
(1519, 684, '_cidade', 'field_5c77e0345e3e8'),
(1520, 684, 'numero_skype', '98175.8310'),
(1521, 684, '_numero_skype', 'field_5c6d5f4323af2'),
(1522, 684, 'a_denuncia_e_anonima', ''),
(1523, 684, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1524, 684, 'recorrente', 'Sim'),
(1525, 684, '_recorrente', 'field_5c6d5f4c23af3'),
(1526, 684, 'problema', 'nao'),
(1527, 684, '_problema', 'field_5c3f3b98d89ab'),
(1530, 685, 'email', 'diegocuruma@yahoo.com'),
(1531, 685, '_email', 'field_5c6d8e9620a26'),
(1532, 685, 'rua', 'Rua Holanda com Leon Gradhvol'),
(1533, 685, '_rua', 'field_5c6d97d6d8ff8'),
(1534, 685, 'cidade', ''),
(1535, 685, '_cidade', 'field_5c77e0345e3e8'),
(1536, 685, 'numero_skype', ''),
(1537, 685, '_numero_skype', 'field_5c6d5f4323af2'),
(1538, 685, 'a_denuncia_e_anonima', ''),
(1539, 685, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1540, 685, 'recorrente', 'Sim'),
(1541, 685, '_recorrente', 'field_5c6d5f4c23af3'),
(1542, 685, 'problema', 'nao'),
(1543, 685, '_problema', 'field_5c3f3b98d89ab'),
(1549, 686, 'email', 'diegocuruma@gmail.com'),
(1550, 686, '_email', 'field_5c6d8e9620a26'),
(1551, 686, 'rua', 'Av. Oliveira Paiva, 2235'),
(1552, 686, '_rua', 'field_5c6d97d6d8ff8'),
(1553, 686, 'cidade', 'Fortaleza'),
(1554, 686, '_cidade', 'field_5c77e0345e3e8'),
(1555, 686, 'numero_skype', ''),
(1556, 686, '_numero_skype', 'field_5c6d5f4323af2'),
(1557, 686, 'a_denuncia_e_anonima', ''),
(1558, 686, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1559, 686, 'recorrente', 'Sim'),
(1560, 686, '_recorrente', 'field_5c6d5f4c23af3'),
(1561, 686, 'problema', 'nao'),
(1562, 686, '_problema', 'field_5c3f3b98d89ab'),
(1563, 655, 'cidade', 'Fortaleza');
INSERT INTO `bai_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1564, 655, '_cidade', 'field_5c77e0345e3e8'),
(1565, 655, 'numero_skype', ''),
(1566, 655, '_numero_skype', 'field_5c6d5f4323af2'),
(1569, 687, 'email', 'diegocuruma@gmail.com'),
(1570, 687, '_email', 'field_5c6d8e9620a26'),
(1571, 687, 'rua', 'Av. Oliveira Paiva, 2235'),
(1572, 687, '_rua', 'field_5c6d97d6d8ff8'),
(1573, 687, 'cidade', 'Fortaleza'),
(1574, 687, '_cidade', 'field_5c77e0345e3e8'),
(1575, 687, 'numero_skype', ''),
(1576, 687, '_numero_skype', 'field_5c6d5f4323af2'),
(1577, 687, 'a_denuncia_e_anonima', 'Diego'),
(1578, 687, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1579, 687, 'recorrente', 'Sim'),
(1580, 687, '_recorrente', 'field_5c6d5f4c23af3'),
(1581, 687, 'problema', 'nao'),
(1582, 687, '_problema', 'field_5c3f3b98d89ab'),
(1583, 688, '_edit_last', '1'),
(1584, 688, '_wp_page_template', 'default'),
(1585, 688, '_edit_lock', '1551362803:1'),
(1586, 691, '_edit_last', '1'),
(1587, 691, '_wp_page_template', 'default'),
(1588, 691, '_edit_lock', '1551365933:1'),
(1597, 694, '_wp_attached_file', '2019/02/problemas-do-meu-bairro-16769zivugicoxhwnclq.jpg'),
(1598, 694, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:330;s:6:\"height\";i:206;s:4:\"file\";s:56:\"2019/02/problemas-do-meu-bairro-16769zivugicoxhwnclq.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:56:\"problemas-do-meu-bairro-16769zivugicoxhwnclq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1642, 697, '_wp_attached_file', '2019/02/o-meu-bairro-diego.jpg'),
(1643, 697, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:30:\"2019/02/o-meu-bairro-diego.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1644, 698, '_wp_attached_file', '2019/02/o-meu-bairro-felipe.jpg'),
(1645, 698, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:31:\"2019/02/o-meu-bairro-felipe.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1646, 699, '_wp_attached_file', '2019/02/o-meu-bairro-jonas.jpg'),
(1647, 699, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:30:\"2019/02/o-meu-bairro-jonas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1648, 700, '_wp_attached_file', '2019/02/o-meu-bairro-lacula.jpg'),
(1649, 700, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:31:\"2019/02/o-meu-bairro-lacula.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1659, 707, '_wp_attached_file', '2019/02/o-meu-bairro-logo-o-meu-bairro.png'),
(1660, 707, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:270;s:6:\"height\";i:100;s:4:\"file\";s:42:\"2019/02/o-meu-bairro-logo-o-meu-bairro.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"o-meu-bairro-logo-o-meu-bairro-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1661, 708, '_wp_attached_file', '2019/02/o-meu-bairro-logo-o-meu-bairro-1.png'),
(1662, 708, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:270;s:6:\"height\";i:100;s:4:\"file\";s:44:\"2019/02/o-meu-bairro-logo-o-meu-bairro-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"o-meu-bairro-logo-o-meu-bairro-1-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1663, 709, '_wp_attached_file', '2019/02/o-meu-bairro-logo-o-meu-bairro-2.png'),
(1664, 709, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:270;s:6:\"height\";i:100;s:4:\"file\";s:44:\"2019/02/o-meu-bairro-logo-o-meu-bairro-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"o-meu-bairro-logo-o-meu-bairro-2-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1665, 710, 'email', 'diegocuruma@gmail.com'),
(1666, 710, 'telefone', ''),
(1667, 710, 'descricao', 'Tampa de bueiro aberta, extremamente perigoso, em dia de chuva fica coberto o que deixa mais perigoso ainda.'),
(1668, 710, 'recorrente', 'Sim'),
(1669, 710, 'a_denuncia_e_anonima', 'Diego'),
(1670, 710, 'numero_skype', ''),
(1671, 710, 'rua', 'Av. Godofredo Maciel'),
(1672, 710, 'cidade', 'Fortaleza'),
(1675, 710, '_thumbnail_id', '716'),
(1676, 710, '_edit_lock', '1551963005:1'),
(1677, 710, '_edit_last', '1'),
(1680, 712, 'email', 'diegocuruma@gmail.com'),
(1681, 712, '_email', 'field_5c6d8e9620a26'),
(1682, 712, 'rua', 'Av. Godofredo Maciel'),
(1683, 712, '_rua', 'field_5c6d97d6d8ff8'),
(1684, 712, 'cidade', 'Fortaleza'),
(1685, 712, '_cidade', 'field_5c77e0345e3e8'),
(1686, 712, 'numero_skype', ''),
(1687, 712, '_numero_skype', 'field_5c6d5f4323af2'),
(1688, 712, 'a_denuncia_e_anonima', 'Diego'),
(1689, 712, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1690, 712, 'recorrente', 'Sim'),
(1691, 712, '_recorrente', 'field_5c6d5f4c23af3'),
(1692, 712, 'problema', 'nao'),
(1693, 712, '_problema', 'field_5c3f3b98d89ab'),
(1694, 710, '_email', 'field_5c6d8e9620a26'),
(1695, 710, '_rua', 'field_5c6d97d6d8ff8'),
(1696, 710, '_cidade', 'field_5c77e0345e3e8'),
(1697, 710, '_numero_skype', 'field_5c6d5f4323af2'),
(1698, 710, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1699, 710, '_recorrente', 'field_5c6d5f4c23af3'),
(1700, 710, 'problema', 'nao'),
(1701, 710, '_problema', 'field_5c3f3b98d89ab'),
(1715, 715, '_wp_attached_file', '2019/03/o-meu-bairro-1_d2mapp7120q_8x6ue8kymq.png'),
(1716, 715, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:330;s:6:\"height\";i:220;s:4:\"file\";s:49:\"2019/03/o-meu-bairro-1_d2mapp7120q_8x6ue8kymq.png\";s:5:\"sizes\";a:1:{s:4:\"capa\";a:4:{s:4:\"file\";s:49:\"o-meu-bairro-1_d2mapp7120q_8x6ue8kymq-330x220.png\";s:5:\"width\";i:330;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1717, 716, '_wp_attached_file', '2019/03/o-meu-bairro-bueiro-maraponga.jpg'),
(1718, 716, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:330;s:6:\"height\";i:220;s:4:\"file\";s:41:\"2019/03/o-meu-bairro-bueiro-maraponga.jpg\";s:5:\"sizes\";a:1:{s:4:\"capa\";a:4:{s:4:\"file\";s:41:\"o-meu-bairro-bueiro-maraponga-330x220.jpg\";s:5:\"width\";i:330;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1721, 717, 'email', 'diegocuruma@gmail.com'),
(1722, 717, '_email', 'field_5c6d8e9620a26'),
(1723, 717, 'rua', 'Av. Godofredo Maciel'),
(1724, 717, '_rua', 'field_5c6d97d6d8ff8'),
(1725, 717, 'cidade', 'Fortaleza'),
(1726, 717, '_cidade', 'field_5c77e0345e3e8'),
(1727, 717, 'numero_skype', ''),
(1728, 717, '_numero_skype', 'field_5c6d5f4323af2'),
(1729, 717, 'a_denuncia_e_anonima', 'Diego'),
(1730, 717, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1731, 717, 'recorrente', 'Sim'),
(1732, 717, '_recorrente', 'field_5c6d5f4c23af3'),
(1733, 717, 'problema', 'nao'),
(1734, 717, '_problema', 'field_5c3f3b98d89ab'),
(1768, 719, '_wp_attached_file', '2019/03/o-meu-bairro-edeas.jpg'),
(1769, 719, '_aioseop_opengraph_settings', 'a:0:{}'),
(1770, 719, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:30:\"2019/03/o-meu-bairro-edeas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1771, 720, '_wp_attached_file', '2019/03/o-meu-bairro-diego-curumim.jpg'),
(1772, 720, '_aioseop_opengraph_settings', 'a:0:{}'),
(1773, 720, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:60;s:6:\"height\";i:60;s:4:\"file\";s:38:\"2019/03/o-meu-bairro-diego-curumim.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1774, 721, '_wp_attached_file', '2019/03/o-meu-bairro-e-deas.jpg'),
(1775, 721, '_aioseop_opengraph_settings', 'a:0:{}'),
(1776, 721, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:60;s:6:\"height\";i:60;s:4:\"file\";s:31:\"2019/03/o-meu-bairro-e-deas.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1779, 710, '_aioseop_opengraph_settings', 'a:15:{s:32:\"aioseop_opengraph_settings_title\";s:0:\"\";s:31:\"aioseop_opengraph_settings_desc\";s:0:\"\";s:32:\"aioseop_opengraph_settings_image\";s:86:\"https://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-bueiro-maraponga.jpg\";s:36:\"aioseop_opengraph_settings_customimg\";s:0:\"\";s:37:\"aioseop_opengraph_settings_imagewidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_imageheight\";s:0:\"\";s:32:\"aioseop_opengraph_settings_video\";s:0:\"\";s:37:\"aioseop_opengraph_settings_videowidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_videoheight\";s:0:\"\";s:35:\"aioseop_opengraph_settings_category\";s:7:\"article\";s:34:\"aioseop_opengraph_settings_section\";s:0:\"\";s:30:\"aioseop_opengraph_settings_tag\";s:0:\"\";s:34:\"aioseop_opengraph_settings_setcard\";s:7:\"summary\";s:44:\"aioseop_opengraph_settings_customimg_twitter\";s:0:\"\";s:44:\"aioseop_opengraph_settings_customimg_checker\";s:1:\"0\";}'),
(1782, 655, '_aioseop_opengraph_settings', 'a:15:{s:32:\"aioseop_opengraph_settings_title\";s:0:\"\";s:31:\"aioseop_opengraph_settings_desc\";s:0:\"\";s:32:\"aioseop_opengraph_settings_image\";s:119:\"https://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-whatsapp-image-2019-02-28-at-08-54-13.jpeg\";s:36:\"aioseop_opengraph_settings_customimg\";s:0:\"\";s:37:\"aioseop_opengraph_settings_imagewidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_imageheight\";s:0:\"\";s:32:\"aioseop_opengraph_settings_video\";s:0:\"\";s:37:\"aioseop_opengraph_settings_videowidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_videoheight\";s:0:\"\";s:35:\"aioseop_opengraph_settings_category\";s:7:\"article\";s:34:\"aioseop_opengraph_settings_section\";s:0:\"\";s:30:\"aioseop_opengraph_settings_tag\";s:0:\"\";s:34:\"aioseop_opengraph_settings_setcard\";s:7:\"summary\";s:44:\"aioseop_opengraph_settings_customimg_twitter\";s:0:\"\";s:44:\"aioseop_opengraph_settings_customimg_checker\";s:1:\"0\";}'),
(1785, 636, '_aioseop_opengraph_settings', 'a:15:{s:32:\"aioseop_opengraph_settings_title\";s:0:\"\";s:31:\"aioseop_opengraph_settings_desc\";s:0:\"\";s:32:\"aioseop_opengraph_settings_image\";s:97:\"https://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-maraponga-lixo-1.jpg\";s:36:\"aioseop_opengraph_settings_customimg\";s:0:\"\";s:37:\"aioseop_opengraph_settings_imagewidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_imageheight\";s:0:\"\";s:32:\"aioseop_opengraph_settings_video\";s:0:\"\";s:37:\"aioseop_opengraph_settings_videowidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_videoheight\";s:0:\"\";s:35:\"aioseop_opengraph_settings_category\";s:7:\"article\";s:34:\"aioseop_opengraph_settings_section\";s:0:\"\";s:30:\"aioseop_opengraph_settings_tag\";s:0:\"\";s:34:\"aioseop_opengraph_settings_setcard\";s:7:\"summary\";s:44:\"aioseop_opengraph_settings_customimg_twitter\";s:0:\"\";s:44:\"aioseop_opengraph_settings_customimg_checker\";s:1:\"0\";}'),
(1786, 722, '_wp_attached_file', '2019/03/o-meu-bairro-nelis.jpg'),
(1787, 722, '_aioseop_opengraph_settings', 'a:0:{}'),
(1788, 722, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:60;s:6:\"height\";i:60;s:4:\"file\";s:30:\"2019/03/o-meu-bairro-nelis.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1789, 723, '_aioseop_opengraph_settings', 'a:14:{s:32:\"aioseop_opengraph_settings_title\";s:0:\"\";s:31:\"aioseop_opengraph_settings_desc\";s:0:\"\";s:36:\"aioseop_opengraph_settings_customimg\";s:0:\"\";s:37:\"aioseop_opengraph_settings_imagewidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_imageheight\";s:0:\"\";s:32:\"aioseop_opengraph_settings_video\";s:0:\"\";s:37:\"aioseop_opengraph_settings_videowidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_videoheight\";s:0:\"\";s:35:\"aioseop_opengraph_settings_category\";s:7:\"article\";s:34:\"aioseop_opengraph_settings_section\";s:0:\"\";s:30:\"aioseop_opengraph_settings_tag\";s:0:\"\";s:34:\"aioseop_opengraph_settings_setcard\";s:7:\"summary\";s:44:\"aioseop_opengraph_settings_customimg_twitter\";s:0:\"\";s:44:\"aioseop_opengraph_settings_customimg_checker\";s:1:\"0\";}'),
(1790, 723, '_edit_last', '1'),
(1791, 723, '_edit_lock', '1551974679:1'),
(1794, 724, 'email', ''),
(1795, 724, '_email', 'field_5c6d8e9620a26'),
(1796, 724, 'rua', ''),
(1797, 724, '_rua', 'field_5c6d97d6d8ff8'),
(1798, 724, 'cidade', ''),
(1799, 724, '_cidade', 'field_5c77e0345e3e8'),
(1800, 724, 'numero_skype', ''),
(1801, 724, '_numero_skype', 'field_5c6d5f4323af2'),
(1802, 724, 'a_denuncia_e_anonima', ''),
(1803, 724, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1804, 724, 'recorrente', ''),
(1805, 724, '_recorrente', 'field_5c6d5f4c23af3'),
(1806, 724, 'problema', 'sim'),
(1807, 724, '_problema', 'field_5c3f3b98d89ab'),
(1808, 723, 'email', ''),
(1809, 723, '_email', 'field_5c6d8e9620a26'),
(1810, 723, 'rua', ''),
(1811, 723, '_rua', 'field_5c6d97d6d8ff8'),
(1812, 723, 'cidade', ''),
(1813, 723, '_cidade', 'field_5c77e0345e3e8'),
(1814, 723, 'numero_skype', ''),
(1815, 723, '_numero_skype', 'field_5c6d5f4323af2'),
(1816, 723, 'a_denuncia_e_anonima', ''),
(1817, 723, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1818, 723, 'recorrente', ''),
(1819, 723, '_recorrente', 'field_5c6d5f4c23af3'),
(1820, 723, 'problema', 'sim'),
(1821, 723, '_problema', 'field_5c3f3b98d89ab'),
(1830, 727, '_wp_attached_file', '2019/03/o-meu-bairro-logo-o-meu-bairro-original.png'),
(1831, 727, '_aioseop_opengraph_settings', 'a:0:{}'),
(1832, 727, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:118;s:6:\"height\";i:60;s:4:\"file\";s:51:\"2019/03/o-meu-bairro-logo-o-meu-bairro-original.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1842, 731, '_wp_attached_file', '2019/03/o-meu-bairro-logo-o-meu-bairro.png'),
(1843, 731, '_aioseop_opengraph_settings', 'a:0:{}'),
(1844, 731, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:270;s:6:\"height\";i:100;s:4:\"file\";s:42:\"2019/03/o-meu-bairro-logo-o-meu-bairro.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"o-meu-bairro-logo-o-meu-bairro-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1845, 732, '_wp_attached_file', '2019/03/o-meu-bairro-logo-bairro.png'),
(1846, 732, '_aioseop_opengraph_settings', 'a:0:{}'),
(1847, 732, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:270;s:6:\"height\";i:100;s:4:\"file\";s:36:\"2019/03/o-meu-bairro-logo-bairro.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"o-meu-bairro-logo-bairro-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1848, 733, '_wp_attached_file', '2019/03/o-meu-bairro-logo-bairro-1.png'),
(1849, 733, '_aioseop_opengraph_settings', 'a:0:{}'),
(1850, 733, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:219;s:6:\"height\";i:100;s:4:\"file\";s:38:\"2019/03/o-meu-bairro-logo-bairro-1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"o-meu-bairro-logo-bairro-1-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1851, 734, '_wp_attached_file', '2019/03/o-meu-bairro-logo-bairro-2.png'),
(1852, 734, '_aioseop_opengraph_settings', 'a:0:{}'),
(1853, 734, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:219;s:6:\"height\";i:100;s:4:\"file\";s:38:\"2019/03/o-meu-bairro-logo-bairro-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"o-meu-bairro-logo-bairro-2-150x100.png\";s:5:\"width\";i:150;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1854, 735, '_wp_attached_file', '2019/03/o-meu-bairro-geovani.png'),
(1855, 735, '_aioseop_opengraph_settings', 'a:0:{}'),
(1856, 735, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:60;s:6:\"height\";i:60;s:4:\"file\";s:32:\"2019/03/o-meu-bairro-geovani.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1857, 463, '_aioseop_opengraph_settings', 'a:0:{}'),
(1858, 463, 'rule', 'a:5:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:26:\"acf-options-opcoes-do-site\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(1859, 736, '_wp_attached_file', '2018/11/o-meu-bairro-pessoas-unidas.jpg'),
(1860, 736, '_aioseop_opengraph_settings', 'a:0:{}'),
(1861, 736, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:330;s:6:\"height\";i:136;s:4:\"file\";s:39:\"2018/11/o-meu-bairro-pessoas-unidas.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"o-meu-bairro-pessoas-unidas-150x136.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:136;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1862, 16, '_aioseop_opengraph_settings', 'a:0:{}'),
(1863, 737, '_wp_attached_file', '2018/11/o-meu-bairro-pessoas-unidas-1.jpg'),
(1864, 737, '_aioseop_opengraph_settings', 'a:0:{}'),
(1865, 737, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:660;s:4:\"file\";s:41:\"2018/11/o-meu-bairro-pessoas-unidas-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"o-meu-bairro-pessoas-unidas-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1866, 738, '_wp_attached_file', '2018/11/o-meu-bairro-pessoas-unidas-2.jpg'),
(1867, 738, '_aioseop_opengraph_settings', 'a:0:{}'),
(1868, 738, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:660;s:4:\"file\";s:41:\"2018/11/o-meu-bairro-pessoas-unidas-2.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"o-meu-bairro-pessoas-unidas-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1869, 739, 'email', 'diegocuruma@gmail.com'),
(1870, 739, 'telefone', ''),
(1871, 739, 'descricao', 'Buraco no asfalto na Rua Paulo Firmeza com José Justa no bairro Pio XII.\r\nBuraco sinalizado por moradores'),
(1872, 739, 'recorrente', 'Não'),
(1873, 739, 'a_denuncia_e_anonima', ''),
(1874, 739, 'numero_skype', ''),
(1875, 739, 'rua', 'Rua Paulo Firmeza'),
(1876, 739, 'cidade', 'Fortaleza'),
(1877, 740, '_wp_attached_file', '2019/03/o-meu-bairro-img-20190310-wa0001.jpg'),
(1878, 740, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:293;s:6:\"height\";i:220;s:4:\"file\";s:44:\"2019/03/o-meu-bairro-img-20190310-wa0001.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"o-meu-bairro-img-20190310-wa0001-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1879, 739, '_thumbnail_id', '740'),
(1880, 739, '_edit_lock', '1552265190:1'),
(1881, 739, '_edit_last', '1'),
(1884, 739, '_aioseop_opengraph_settings', 'a:14:{s:32:\"aioseop_opengraph_settings_title\";s:0:\"\";s:31:\"aioseop_opengraph_settings_desc\";s:0:\"\";s:36:\"aioseop_opengraph_settings_customimg\";s:0:\"\";s:37:\"aioseop_opengraph_settings_imagewidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_imageheight\";s:0:\"\";s:32:\"aioseop_opengraph_settings_video\";s:0:\"\";s:37:\"aioseop_opengraph_settings_videowidth\";s:0:\"\";s:38:\"aioseop_opengraph_settings_videoheight\";s:0:\"\";s:35:\"aioseop_opengraph_settings_category\";s:7:\"article\";s:34:\"aioseop_opengraph_settings_section\";s:0:\"\";s:30:\"aioseop_opengraph_settings_tag\";s:0:\"\";s:34:\"aioseop_opengraph_settings_setcard\";s:7:\"summary\";s:44:\"aioseop_opengraph_settings_customimg_twitter\";s:0:\"\";s:44:\"aioseop_opengraph_settings_customimg_checker\";s:1:\"0\";}'),
(1885, 741, 'email', 'diegocuruma@gmail.com'),
(1886, 741, '_email', 'field_5c6d8e9620a26'),
(1887, 741, 'rua', 'Rua Paulo Firmeza'),
(1888, 741, '_rua', 'field_5c6d97d6d8ff8'),
(1889, 741, 'cidade', 'Fortaleza'),
(1890, 741, '_cidade', 'field_5c77e0345e3e8'),
(1891, 741, 'numero_skype', ''),
(1892, 741, '_numero_skype', 'field_5c6d5f4323af2'),
(1893, 741, 'a_denuncia_e_anonima', ''),
(1894, 741, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1895, 741, 'recorrente', 'Não'),
(1896, 741, '_recorrente', 'field_5c6d5f4c23af3'),
(1897, 741, 'problema', 'nao'),
(1898, 741, '_problema', 'field_5c3f3b98d89ab'),
(1899, 739, '_email', 'field_5c6d8e9620a26'),
(1900, 739, '_rua', 'field_5c6d97d6d8ff8'),
(1901, 739, '_cidade', 'field_5c77e0345e3e8'),
(1902, 739, '_numero_skype', 'field_5c6d5f4323af2'),
(1903, 739, '_a_denuncia_e_anonima', 'field_5c6d922479a1a'),
(1904, 739, '_recorrente', 'field_5c6d5f4c23af3'),
(1905, 739, 'problema', 'nao'),
(1906, 739, '_problema', 'field_5c3f3b98d89ab'),
(1907, 742, '_aioseop_opengraph_settings', 'a:0:{}'),
(1908, 742, '_edit_last', '1'),
(1909, 742, '_edit_lock', '1554897594:1'),
(1910, 742, '_oembed_48e1c770ec0c7b000e26c3551af7a6cd', '{{unknown}}'),
(1911, 742, '_oembed_51567006b58722b078797503d8053c8c', '{{unknown}}'),
(1912, 742, '_oembed_f2aa0db360172438a8b0424006ecbf5a', '{{unknown}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_posts`
--

CREATE TABLE `bai_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_posts`
--

INSERT INTO `bai_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(2, 1, '2018-11-09 16:01:06', '2018-11-09 18:01:06', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página \'Sobre\' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins-de-semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href=\"http://omeubairro.online/wp-admin/\">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'draft', 'closed', 'closed', '', 'pagina-exemplo', '', '', '2019-02-25 13:08:09', '2019-02-25 16:08:09', '', 0, 'http://omeubairro.online/?page_id=2', 0, 'page', '', 0),
(5, 1, '2018-11-09 16:26:02', '2018-11-09 18:26:02', '', 'Início', '', 'publish', 'closed', 'closed', '', 'inicio', '', '', '2019-02-26 09:20:17', '2019-02-26 12:20:17', '', 0, 'http://omeubairro.online/?p=5', 1, 'nav_menu_item', '', 0),
(7, 1, '2018-11-09 17:28:52', '2018-11-09 19:28:52', '[insert_post]', 'Identificar problemas', '', 'publish', 'closed', 'closed', '', 'identificar-problemas', '', '', '2019-02-20 11:26:29', '2019-02-20 14:26:29', '', 0, 'http://omeubairro.online/?page_id=7', 0, 'page', '', 0),
(8, 1, '2018-11-09 17:28:52', '2018-11-09 19:28:52', '', 'Identificar problemas', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-11-09 17:28:52', '2018-11-09 19:28:52', '', 7, 'http://omeubairro.online/2018/11/09/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2018-11-09 17:29:20', '2018-11-09 19:29:20', 'O meu bairro ou omeubairro.online é uma ferramenta colaborativa onde cidadãos podem fazer uma denuncia de algum problema no seu bairro, ou, podem também se unir a outros moradores e resolver um problema temporariamente enquanto a prefeitura ou estado não o fazem, assim ajudando o bairro e as pessoas da melhor forma possível.\r\n\r\nFoi idealizado e feito por jovens de periferia provenientes de ONG\'s que hoje trabalham com tecnologia,  justamente pensando em como melhorar a vida das pessoas no bairro, nasceu o projeto que consiste em denuncias e ações colaborativas afim de resolver problemas no próprio bairro.\r\n\r\n&nbsp;\r\n\r\nContato: problemasdobairrofortaleza@<wbr />gmail.com', 'O que é?', '', 'publish', 'closed', 'closed', '', 'o-que-e', '', '', '2019-02-28 13:52:59', '2019-02-28 16:52:59', '', 0, 'http://omeubairro.online/?page_id=9', 0, 'page', '', 0),
(10, 1, '2018-11-09 17:29:20', '2018-11-09 19:29:20', '', 'O que é?', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-11-09 17:29:20', '2018-11-09 19:29:20', '', 9, 'http://omeubairro.online/2018/11/09/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-11-09 17:29:45', '2018-11-09 19:29:45', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\r\n\r\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\r\n\r\nNo cadastro existe a opção de deixar nome, telefone e skype, através desses dados os próprios cidadãos podem se articular, conversar entre si e marcar para resolver problema \"X\".\r\n\r\nPodem também usar os comentários no próprio site para tal, quando resolver um problema podem enviar fotos para atualizar pelos comentários ou por e-mail: problemasdobairrofortaleza@<wbr />gmail.com\r\n\r\n<strong>Banco de ideias:</strong>\r\n\r\nLinks com ideias fáceis e uteis de serem replicadas.\r\n\r\n&nbsp;\r\n\r\n<strong><a class=\"row-title\" href=\"https://omeubairro.online/mapa-de-problemas\" aria-label=\"“Mapa de problemas” (Editar)\">Mapa de problemas</a></strong>\r\n\r\nOs usuários podem também marcar no mapa colaborativo um problema, é só seguir o modelo existente no próprio mapa e cadastrar uma nova ocorrência.\r\n\r\n<strong>Comente:</strong>\r\n\r\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Como funciona?', '', 'publish', 'closed', 'closed', '', 'como-funciona', '', '', '2019-02-28 13:58:23', '2019-02-28 16:58:23', '', 0, 'http://omeubairro.online/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-11-09 17:29:45', '2018-11-09 19:29:45', '', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-11-09 17:29:45', '2018-11-09 19:29:45', '', 11, 'http://omeubairro.online/2018/11/09/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-11-09 17:30:21', '2018-11-09 19:30:21', ' ', '', '', 'publish', 'closed', 'closed', '', '13', '', '', '2019-02-26 09:20:17', '2019-02-26 12:20:17', '', 0, 'http://omeubairro.online/?p=13', 3, 'nav_menu_item', '', 0),
(14, 1, '2018-11-09 17:30:21', '2018-11-09 19:30:21', ' ', '', '', 'publish', 'closed', 'closed', '', '14', '', '', '2019-02-26 09:20:17', '2019-02-26 12:20:17', '', 0, 'http://omeubairro.online/?p=14', 4, 'nav_menu_item', '', 0),
(15, 1, '2018-11-09 17:30:21', '2018-11-09 19:30:21', ' ', '', '', 'publish', 'closed', 'closed', '', '15', '', '', '2019-02-26 09:20:17', '2019-02-26 12:20:17', '', 0, 'http://omeubairro.online/?p=15', 2, 'nav_menu_item', '', 0),
(16, 1, '2018-11-09 17:33:19', '2018-11-09 19:33:19', '', 'Banner de ajuda', '', 'publish', 'closed', 'closed', '', 'banner-de-ajuda', '', '', '2019-03-07 15:41:57', '2019-03-07 18:41:57', '', 0, 'http://omeubairro.online/?post_type=banner&#038;p=16', 0, 'banner', '', 0),
(17, 1, '2018-11-09 17:33:13', '2018-11-09 19:33:13', '', 'painel', '', 'inherit', 'open', 'closed', '', 'painel', '', '', '2018-11-09 17:33:13', '2018-11-09 19:33:13', '', 16, 'http://omeubairro.online/wp-content/uploads/2018/11/problemas-do-meu-bairro-painel.png', 0, 'attachment', 'image/png', 0),
(463, 1, '2016-03-02 18:42:57', '2016-03-02 18:42:57', '', 'Informações do Site', '', 'publish', 'closed', 'closed', '', 'acf_informacoes-do-site', '', '', '2019-03-07 15:26:38', '2019-03-07 18:26:38', '', 0, 'http://localhost/protekseg/?post_type=acf&#038;p=463', 0, 'acf', '', 0),
(464, 1, '2018-11-09 17:42:27', '2018-11-09 19:42:27', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-11-09 17:42:27', '2018-11-09 19:42:27', '', 0, 'http://omeubairro.online/wp-content/uploads/2018/11/problemas-do-meu-bairro-logo.png', 0, 'attachment', 'image/png', 0),
(471, 1, '2016-11-01 10:05:33', '2016-11-01 12:05:33', '', 'Texto do Painel', '', 'publish', 'closed', 'closed', '', 'acf_texto-do-painel', '', '', '2016-11-01 10:05:33', '2016-11-01 12:05:33', '', 0, 'http://localhost/fbs_wp/?post_type=acf&amp;p=471', 0, 'acf', '', 0),
(476, 1, '2019-01-16 12:34:07', '2019-01-16 14:34:07', '', 'Problemas', '', 'publish', 'closed', 'closed', '', 'acf_problemas', '', '', '2019-02-20 11:51:33', '2019-02-20 14:51:33', '', 0, 'http://omeubairro.online/?post_type=acf&#038;p=476', 0, 'acf', '', 0),
(480, 1, '2019-01-16 12:43:15', '2019-01-16 14:43:15', '', 'Campos da página do bairro', '', 'draft', 'closed', 'closed', '', 'acf_campos-da-pagina-do-bairro', '', '', '2019-02-20 11:10:17', '2019-02-20 14:10:17', '', 0, 'http://omeubairro.online/?post_type=acf&#038;p=480', 0, 'acf', '', 0),
(484, 1, '2019-01-16 12:49:12', '2019-01-16 14:49:12', '<em>Boi na Linha: pessoa intrometida, intrusa;\r\nmetáfora popular usada quando há alguém no meio de uma comunicação.</em>\r\n\r\nPara os movimentos sociais, a comunicação rápida e segura entre membros de um coletivo, entre coletivos, ou entre ativistas independentes é fundamental tanto para o planejamento e a eficácia das ações, como também para o fortalecimento do próprio movimento de resistência.\r\n\r\nNa era analógica, espionar as comunicações de um indivíduo ou grupo e apreender documentos sensíveis, eram ações bastante comuns, apesar de serem dirigidas a alvos específicos e um tanto difíceis de serem camufladas. Com a internet, essa prática se tornou mais discreta, silenciosa e ainda mais comum, sendo utilizada em larga escala, para a espionagem em massa, e por motivos que ultrapassam os estritamente políticos.\r\n\r\nA cada dia fica mais evidente que grande parte das atividades na internet estão sujeitas à vigilância, e que há, quase sempre, bois na linha: algo ou alguém coletando seus dados privados, espiando suas conversas e e-mails, ou registrando os sites acessados por você. Esses bois não são inofensivos, e podem inviabilizar as ações de um grupo, expor os ativistas, e favorecer a repressão.\r\n<h6>MAS CALMA, É POSSÍVEL COMBATER A VIGILÂNCIA E MANDAR OS BOIS PASTAREM!</h6>\r\nFelizmente, certas propriedades físicas do nosso mundo fazem com que cifrar informações seja mais fácil que decifrá-las. A mudança de alguns hábitos e a utilização de criptografia e de softwares livres e de código aberto, podem proporcionar um bom nível de privacidade, ainda que não garanta uma segurança total.\r\n<h6>“TEM BOI NA LINHA?” É UM GUIA PRÁTICO DE COMBATE À VIGILÂNCIA NO ÂMBITO DOS MOVIMENTOS SOCIAIS, E É DESTINADO A GRUPOS DE ATIVISTAS, JORNALISTAS, MIDIALIVRISTAS OU A QUALQUER PESSOA QUE PRECISE OU DESEJE SE PROTEGER, E PROTEGER SUAS COMUNICAÇÕES E ARQUIVOS, DA VIGILÂNCIA DO ESTADO E DE INSTITUIÇÕES PRIVADAS.</h6>\r\nLink: <span style=\"color: #0000ff;\"><a style=\"color: #0000ff;\" href=\"http://temboinalinha.org\">temboinalinha.org</a></span>', 'Guia prático de combate à vigilância na internet', '', 'publish', 'closed', 'closed', '', 'guia-pratico-de-combate-a-vigilancia-na-internet', '', '', '2019-02-25 13:07:58', '2019-02-25 16:07:58', '', 0, 'http://omeubairro.online/?post_type=banco&#038;p=484', 0, 'banco', '', 0),
(487, 1, '2019-02-20 10:52:14', '2019-02-20 13:52:14', '<div>\r\n<h2>O que é Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.\r\n\r\n</div>\r\n<div>\r\n<h2>Porque nós o usamos?</h2>\r\nÉ um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>De onde ele vem?</h2>\r\nAo contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32.\r\n\r\nO trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.\r\n\r\n</div>\r\n<div>\r\n<h2>Onde posso conseguí-lo?</h2>\r\nExistem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.\r\n\r\n</div>', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-20 10:52:14', '2019-02-20 13:52:14', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(488, 1, '2019-02-20 10:53:27', '2019-02-20 13:53:27', '<div>\r\n<h2>O que é Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.\r\n\r\n</div>\r\n<div></div>\r\n<div>\r\n\r\n&nbsp;\r\n\r\n</div>', 'O que é?', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-02-20 10:53:27', '2019-02-20 13:53:27', '', 9, 'http://omeubairro.online/9-revision-v1/', 0, 'revision', '', 0),
(489, 1, '2019-02-20 11:08:52', '2019-02-20 14:08:52', '', 'Denuncia', '', 'publish', 'closed', 'closed', '', 'acf_denuncia', '', '', '2019-02-28 10:20:37', '2019-02-28 13:20:37', '', 0, 'http://omeubairro.online/?post_type=acf&#038;p=489', 0, 'acf', '', 0),
(491, 1, '2019-02-20 11:11:44', '2019-02-20 14:11:44', '[insert_post]', 'Identificar problemas', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-02-20 11:11:44', '2019-02-20 14:11:44', '', 7, 'http://omeubairro.online/7-revision-v1/', 0, 'revision', '', 0),
(492, 1, '2019-02-20 11:23:34', '2019-02-20 14:23:34', '[insert_post]scvsa', 'Identificar problemas', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-02-20 11:23:34', '2019-02-20 14:23:34', '', 7, 'http://omeubairro.online/7-revision-v1/', 0, 'revision', '', 0),
(493, 1, '2019-02-20 11:26:29', '2019-02-20 14:26:29', '[insert_post]', 'Identificar problemas', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-02-20 11:26:29', '2019-02-20 14:26:29', '', 7, 'http://omeubairro.online/7-revision-v1/', 0, 'revision', '', 0),
(535, 1, '2019-02-20 13:03:33', '2019-02-20 16:03:33', 'cdscs', 'teste4', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-02-20 13:03:51', '2019-02-20 16:03:51', '', 0, 'http://omeubairro.online/?post_type=anuncio&#038;p=535', 0, 'anuncio', '', 0),
(536, 1, '2019-02-20 13:07:59', '0000-00-00 00:00:00', 'vsvsa', 'bernad', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-02-20 13:07:59', '0000-00-00 00:00:00', '', 0, 'http://omeubairro.online/?post_type=anuncio&p=536', 0, 'anuncio', '', 0),
(537, 1, '2019-02-20 13:08:40', '0000-00-00 00:00:00', 'vsvsa', 'bernad', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-02-20 13:08:40', '0000-00-00 00:00:00', '', 0, 'http://omeubairro.online/?post_type=anuncio&p=537', 0, 'anuncio', '', 0),
(538, 1, '2019-02-20 13:09:00', '0000-00-00 00:00:00', 'vdsavsa', 'joaquim', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-02-20 13:09:00', '0000-00-00 00:00:00', '', 0, 'http://omeubairro.online/?post_type=anuncio&p=538', 0, 'anuncio', '', 0),
(540, 1, '2019-02-20 13:14:36', '2019-02-20 16:14:36', 'Buracos', 'Maraponga', '', 'draft', 'closed', 'closed', '', 'maraponga', '', '', '2019-02-25 12:54:20', '2019-02-25 15:54:20', '', 0, 'http://omeubairro.online/?post_type=problema&#038;p=540', 0, 'problema', '', 0),
(542, 1, '2019-02-20 13:14:36', '2019-02-20 16:14:36', 'Buracos resolvidos mas é recorrente', 'Cidade 2000', '', 'draft', 'closed', 'closed', '', 'cidade-2000', '', '', '2019-02-25 12:54:20', '2019-02-25 15:54:20', '', 0, 'http://omeubairro.online/?post_type=problema&#038;p=542', 0, 'problema', '', 0),
(544, 1, '2019-02-20 13:14:36', '2019-02-20 16:14:36', 'Lixo', 'Bom jardim', '', 'draft', 'closed', 'closed', '', 'bom-jardim', '', '', '2019-02-25 12:54:20', '2019-02-25 15:54:20', '', 0, 'http://omeubairro.online/?post_type=problema&#038;p=544', 0, 'problema', '', 0),
(545, 1, '2019-02-20 13:18:52', '2019-02-20 16:18:52', 'Lixo', 'Bom jardim', '', 'draft', 'closed', 'closed', '', 'bom-jardim-2', '', '', '2019-02-25 12:54:20', '2019-02-25 15:54:20', '', 0, 'http://omeubairro.online/?post_type=problema&#038;p=545', 0, 'problema', '', 0),
(546, 1, '2019-02-20 14:02:22', '2019-02-20 17:02:22', 'b', 'bfsbfb', '', 'draft', 'closed', 'closed', '', 'bfsbfb', '', '', '2019-02-25 12:54:20', '2019-02-25 15:54:20', '', 0, 'http://omeubairro.online/?post_type=problema&#038;p=546', 0, 'problema', '', 0),
(547, 1, '2019-02-25 12:54:20', '0000-00-00 00:00:00', 'bláblá', 'Cambeba', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-02-25 12:54:20', '2019-02-25 15:54:20', '', 0, 'http://omeubairro.online/?post_type=problema&#038;p=547', 0, 'problema', '', 0),
(603, 1, '2019-02-20 15:10:23', '2019-02-20 18:10:23', '', '15272054105b074e2201d69_1527205410_3x2_md', '', 'inherit', 'open', 'closed', '', '15272054105b074e2201d69_1527205410_3x2_md', '', '', '2019-02-20 15:10:23', '2019-02-20 18:10:23', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-15272054105b074e2201d69_1527205410_3x2_md.jpg', 0, 'attachment', 'image/jpeg', 0),
(633, 1, '2019-02-25 12:43:17', '2019-02-25 15:43:17', '', 'jonas', '', 'inherit', 'open', 'closed', '', 'jonas', '', '', '2019-02-25 12:43:17', '2019-02-25 15:43:17', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-jonas.jpg', 0, 'attachment', 'image/jpeg', 0),
(636, 1, '2019-02-25 13:00:48', '2019-02-25 16:00:48', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'publish', 'open', 'open', '', 'maraponga-2', '', '', '2019-03-07 09:52:47', '2019-03-07 12:52:47', '', 0, 'http://omeubairro.online/?p=636', 0, 'post', '', 0),
(638, 1, '2019-02-25 13:00:48', '2019-02-25 16:00:48', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '636-revision-v1', '', '', '2019-02-25 13:00:48', '2019-02-25 16:00:48', '', 636, 'http://omeubairro.online/636-revision-v1/', 0, 'revision', '', 0),
(639, 1, '2019-02-25 13:01:12', '2019-02-25 16:01:12', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '636-revision-v1', '', '', '2019-02-25 13:01:12', '2019-02-25 16:01:12', '', 636, 'http://omeubairro.online/636-revision-v1/', 0, 'revision', '', 0),
(640, 1, '2019-02-25 13:08:09', '2019-02-25 16:08:09', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página \'Sobre\' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins-de-semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href=\"http://omeubairro.online/wp-admin/\">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-02-25 13:08:09', '2019-02-25 16:08:09', '', 2, 'http://omeubairro.online/2-revision-v1/', 0, 'revision', '', 0),
(641, 1, '2019-02-28 13:57:14', '2019-02-28 16:57:14', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\n\n<strong>Denunciar:</strong>\n\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\n\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\n\n<strong>Faça você mesmo:</strong>\n\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\n\nNo cadastro existe a opção de deixar nome, telefone e skype, através desses dados os próprios cidadãos podem se articular, conversar entre si e marcar para resolver problema \"X\".\n\nPodem também usar os comentários no próprio site para tal, quando resolver um problema podem enviar fotos para atualizar pelos comentários ou por e-mail: problemasdobairrofortaleza@<wbr />gmail.com\n\n<strong>Banco de ideias:</strong>\n\nLinks com ideias fáceis e uteis de serem replicadas.\n\n&nbsp;\n\n<strong><a class=\"row-title\" href=\"https://omeubairro.online/wp-admin/post.php?post=646&amp;action=edit\" aria-label=\"“Mapa de problemas” (Editar)\">Mapa de problemas</a></strong>\n\nOs usuários podem também marcar no mapa colaborativo um problema, é só seguir o modelo existente no próprio mapa e cadastrar uma nova ocorrência.\n\n<strong>Comente:</strong>\n\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\n\n&nbsp;\n\n&nbsp;', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-autosave-v1', '', '', '2019-02-28 13:57:14', '2019-02-28 16:57:14', '', 11, 'http://omeubairro.online/11-autosave-v1/', 0, 'revision', '', 0),
(642, 1, '2019-02-25 13:10:30', '2019-02-25 16:10:30', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no site.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, resolva temporariamente o problema e deixe nos comentários a solução, através dos comentários você pode se unir a alguém do seu bairro e resolver o problemas em conjunto.', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-25 13:10:30', '2019-02-25 16:10:30', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(643, 1, '2019-02-28 13:51:43', '2019-02-28 16:51:43', 'O meu bairro ou omeubairro.online é uma ferramenta colaborativa onde cidadãos podem fazer uma denuncia de algum problema no seu bairro, ou, podem também se unir a outros moradores e resolver um problema temporariamente enquanto a prefeitura ou estado não o fazem, assim ajudando o bairro e as pessoas da melhor forma possível.\n\nFoi idealizado e feito por jovens de periferia provenientes de ONG\'s que hoje trabalham com tecnologia,  justamente pensando em como melhorar a vida das pessoas no bairro, nasceu o projeto que consiste em denuncias e ações colaborativas afim de resolver problemas no próprio bairro.\n\n&nbsp;', 'O que é?', '', 'inherit', 'closed', 'closed', '', '9-autosave-v1', '', '', '2019-02-28 13:51:43', '2019-02-28 16:51:43', '', 9, 'http://omeubairro.online/9-autosave-v1/', 0, 'revision', '', 0),
(644, 1, '2019-02-25 13:12:17', '2019-02-25 16:12:17', 'O problemas do meu bairro ou omeubairro.online é uma ferramente colaborativa onde cidadãos podem fazer uma denuncia de algum problema no seu bairro, ou, pode também se unir a outros moradores e resolver um problema temporariamente enquanto a prefeitura ou estado não o fazem, assim ajudando o bairro e as pessoas da melhor forma possível.', 'O que é?', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-02-25 13:12:17', '2019-02-25 16:12:17', '', 9, 'http://omeubairro.online/9-revision-v1/', 0, 'revision', '', 0),
(646, 1, '2019-02-26 09:19:55', '2019-02-26 12:19:55', '<iframe src=\"https://www.google.com/maps/d/u/0/embed?mid=1YqcRWgTXLm4vMGyCoNdDajhk8zHbMbd2\" width=\"100%\" height=\"480\"></iframe>', 'Mapa de problemas', '', 'publish', 'closed', 'closed', '', 'mapa-de-problemas', '', '', '2019-02-26 09:31:18', '2019-02-26 12:31:18', '', 0, 'http://omeubairro.online/?page_id=646', 0, 'page', '', 0),
(647, 1, '2019-02-26 09:19:55', '2019-02-26 12:19:55', '&lt;iframe src=\"https://www.google.com/maps/d/u/0/embed?mid=1YqcRWgTXLm4vMGyCoNdDajhk8zHbMbd2\" width=\"100%\" height=\"600\"&gt;&lt;/iframe&gt;', 'Mapa de problemas', '', 'inherit', 'closed', 'closed', '', '646-revision-v1', '', '', '2019-02-26 09:19:55', '2019-02-26 12:19:55', '', 646, 'http://omeubairro.online/646-revision-v1/', 0, 'revision', '', 0),
(648, 1, '2019-02-26 09:20:17', '2019-02-26 12:20:17', ' ', '', '', 'publish', 'closed', 'closed', '', '648', '', '', '2019-02-26 09:20:17', '2019-02-26 12:20:17', '', 0, 'http://omeubairro.online/?p=648', 5, 'nav_menu_item', '', 0),
(649, 1, '2019-02-26 09:20:39', '2019-02-26 12:20:39', '<iframe src=\"https://www.google.com/maps/d/u/0/embed?mid=1YqcRWgTXLm4vMGyCoNdDajhk8zHbMbd2\" width=\"640\" height=\"480\"></iframe>', 'Mapa de problemas', '', 'inherit', 'closed', 'closed', '', '646-revision-v1', '', '', '2019-02-26 09:20:39', '2019-02-26 12:20:39', '', 646, 'http://omeubairro.online/646-revision-v1/', 0, 'revision', '', 0),
(650, 1, '2019-02-26 09:25:15', '2019-02-26 12:25:15', '<iframe src=\"https://www.google.com/maps/d/u/0/embed?mid=1YqcRWgTXLm4vMGyCoNdDajhk8zHbMbd2\" width=\"100%\" height=\"600\"></iframe>', 'Mapa de problemas', '', 'inherit', 'closed', 'closed', '', '646-revision-v1', '', '', '2019-02-26 09:25:15', '2019-02-26 12:25:15', '', 646, 'http://omeubairro.online/646-revision-v1/', 0, 'revision', '', 0),
(651, 1, '2019-02-26 09:31:18', '2019-02-26 12:31:18', '<iframe src=\"https://www.google.com/maps/d/u/0/embed?mid=1YqcRWgTXLm4vMGyCoNdDajhk8zHbMbd2\" width=\"100%\" height=\"480\"></iframe>', 'Mapa de problemas', '', 'inherit', 'closed', 'closed', '', '646-revision-v1', '', '', '2019-02-26 09:31:18', '2019-02-26 12:31:18', '', 646, 'http://omeubairro.online/646-revision-v1/', 0, 'revision', '', 0),
(654, 1, '2019-02-27 17:13:36', '2019-02-27 20:13:36', '', 'IMG-20190227-WA0012', '', 'inherit', 'open', 'closed', '', 'img-20190227-wa0012', '', '', '2019-02-27 17:13:36', '2019-02-27 20:13:36', '', 636, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-img-20190227-wa0012.jpg', 0, 'attachment', 'image/jpeg', 0),
(655, 1, '2019-02-28 09:15:29', '2019-02-28 12:15:29', 'Na Av. Oliveira Paiva próximo a Av. Hermínio de Castro, terreno baldio com grande quantidade de lixo a céu aberto, pneus, foco de doenças domo dengue e etc..\r\n', 'Parque Manibura', '', 'publish', 'open', 'open', '', 'parque-manibura', '', '', '2019-03-07 09:52:37', '2019-03-07 12:52:37', '', 0, 'https://omeubairro.online/?p=655', 0, 'post', '', 0),
(656, 0, '2019-02-28 09:13:27', '2019-02-28 12:13:27', '', 'WhatsApp Image 2019-02-28 at 08.54.13', '', 'inherit', 'open', 'closed', '', 'whatsapp-image-2019-02-28-at-08-54-13', '', '', '2019-02-28 09:13:27', '2019-02-28 12:13:27', '', 655, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-whatsapp-image-2019-02-28-at-08-54-13.jpeg', 0, 'attachment', 'image/jpeg', 0),
(657, 1, '2019-02-28 09:15:29', '2019-02-28 12:15:29', 'Ná Av. Oliveira Paiva próximo a Av. Hermínio de Castro, terreno baldio com grande quantidade de lixo a céu aberto, pneus, foco de doenças domo dengue e etc..\r\n', 'Parque Manibura', '', 'inherit', 'closed', 'closed', '', '655-revision-v1', '', '', '2019-02-28 09:15:29', '2019-02-28 12:15:29', '', 655, 'http://omeubairro.online/655-revision-v1/', 0, 'revision', '', 0),
(675, 1, '2019-02-28 09:31:00', '2019-02-28 12:31:00', '', 'problemas-do-meu-bairro-img-20190227-wa0012', '', 'inherit', 'open', 'closed', '', 'problemas-do-meu-bairro-img-20190227-wa0012', '', '', '2019-02-28 09:31:00', '2019-02-28 12:31:00', '', 636, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-problemas-do-meu-bairro-img-20190227-wa0012.jpg', 0, 'attachment', 'image/jpeg', 0),
(676, 1, '2019-02-28 09:31:46', '2019-02-28 12:31:46', '', 'maraponga-lixo', '', 'inherit', 'open', 'closed', '', 'maraponga-lixo', '', '', '2019-02-28 09:31:46', '2019-02-28 12:31:46', '', 636, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-maraponga-lixo.jpg', 0, 'attachment', 'image/jpeg', 0),
(677, 1, '2019-02-28 09:33:17', '2019-02-28 12:33:17', '', 'maraponga-lixo', '', 'inherit', 'open', 'closed', '', 'maraponga-lixo-2', '', '', '2019-02-28 09:33:17', '2019-02-28 12:33:17', '', 636, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-maraponga-lixo-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(678, 1, '2019-02-28 09:33:40', '2019-02-28 12:33:40', 'Na Av. Oliveira Paiva próximo a Av. Hermínio de Castro, terreno baldio com grande quantidade de lixo a céu aberto, pneus, foco de doenças domo dengue e etc..\r\n', 'Parque Manibura', '', 'inherit', 'closed', 'closed', '', '655-revision-v1', '', '', '2019-02-28 09:33:40', '2019-02-28 12:33:40', '', 655, 'http://omeubairro.online/655-revision-v1/', 0, 'revision', '', 0),
(680, 1, '2019-02-28 10:36:30', '2019-02-28 13:36:30', '', 'foto galeria 600x400', '', 'inherit', 'open', 'closed', '', 'foto-galeria-600x400', '', '', '2019-02-28 10:36:30', '2019-02-28 13:36:30', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-foto-galeria-600x400.jpg', 0, 'attachment', 'image/jpeg', 0),
(682, 1, '2019-02-28 10:47:29', '2019-02-28 13:47:29', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '636-revision-v1', '', '', '2019-02-28 10:47:29', '2019-02-28 13:47:29', '', 636, 'http://omeubairro.online/636-revision-v1/', 0, 'revision', '', 0),
(683, 1, '2019-02-28 10:47:44', '2019-02-28 13:47:44', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '636-revision-v1', '', '', '2019-02-28 10:47:44', '2019-02-28 13:47:44', '', 636, 'http://omeubairro.online/636-revision-v1/', 0, 'revision', '', 0),
(684, 1, '2019-02-28 10:48:10', '2019-02-28 13:48:10', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '636-revision-v1', '', '', '2019-02-28 10:48:10', '2019-02-28 13:48:10', '', 636, 'http://omeubairro.online/636-revision-v1/', 0, 'revision', '', 0),
(685, 1, '2019-02-28 10:48:21', '2019-02-28 13:48:21', 'Problema recorrente de lixo nessa rua, bem na esquina, mais de 20 metros de lixo, a prefeitura remove, porém, o lixo não para de aparecer por conta de entulho de obras, sofás entre outras coisas jogadas no local.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '636-revision-v1', '', '', '2019-02-28 10:48:21', '2019-02-28 13:48:21', '', 636, 'http://omeubairro.online/636-revision-v1/', 0, 'revision', '', 0),
(686, 1, '2019-02-28 10:50:32', '2019-02-28 13:50:32', 'Na Av. Oliveira Paiva próximo a Av. Hermínio de Castro, terreno baldio com grande quantidade de lixo a céu aberto, pneus, foco de doenças domo dengue e etc..\r\n', 'Parque Manibura', '', 'inherit', 'closed', 'closed', '', '655-revision-v1', '', '', '2019-02-28 10:50:32', '2019-02-28 13:50:32', '', 655, 'http://omeubairro.online/655-revision-v1/', 0, 'revision', '', 0),
(687, 1, '2019-02-28 10:52:16', '2019-02-28 13:52:16', 'Na Av. Oliveira Paiva próximo a Av. Hermínio de Castro, terreno baldio com grande quantidade de lixo a céu aberto, pneus, foco de doenças domo dengue e etc..\r\n', 'Parque Manibura', '', 'inherit', 'closed', 'closed', '', '655-revision-v1', '', '', '2019-02-28 10:52:16', '2019-02-28 13:52:16', '', 655, 'http://omeubairro.online/655-revision-v1/', 0, 'revision', '', 0),
(688, 1, '2019-02-28 11:01:08', '2019-02-28 14:01:08', '', 'Problemas Identificados', '', 'publish', 'closed', 'closed', '', 'problemas-identificados', '', '', '2019-02-28 11:06:21', '2019-02-28 14:06:21', '', 0, 'http://omeubairro.online/?page_id=688', 0, 'page', '', 0),
(689, 1, '2019-02-28 11:01:08', '2019-02-28 14:01:08', '', 'Problemas', '', 'inherit', 'closed', 'closed', '', '688-revision-v1', '', '', '2019-02-28 11:01:08', '2019-02-28 14:01:08', '', 688, 'http://omeubairro.online/688-revision-v1/', 0, 'revision', '', 0),
(690, 1, '2019-02-28 11:06:21', '2019-02-28 14:06:21', '', 'Problemas Identificados', '', 'inherit', 'closed', 'closed', '', '688-revision-v1', '', '', '2019-02-28 11:06:21', '2019-02-28 14:06:21', '', 688, 'http://omeubairro.online/688-revision-v1/', 0, 'revision', '', 0),
(691, 1, '2019-02-28 11:59:04', '2019-02-28 14:59:04', '', 'Problemas resolvidos', '', 'publish', 'closed', 'closed', '', 'problemas-resolvidos', '', '', '2019-02-28 11:59:04', '2019-02-28 14:59:04', '', 0, 'http://omeubairro.online/?page_id=691', 0, 'page', '', 0),
(692, 1, '2019-02-28 11:59:04', '2019-02-28 14:59:04', '', 'Problemas resolvidos', '', 'inherit', 'closed', 'closed', '', '691-revision-v1', '', '', '2019-02-28 11:59:04', '2019-02-28 14:59:04', '', 691, 'http://omeubairro.online/691-revision-v1/', 0, 'revision', '', 0),
(694, 1, '2019-02-28 12:05:39', '2019-02-28 15:05:39', '', '16769ziVUGICoxhwNcLq', '', 'inherit', 'open', 'closed', '', '16769zivugicoxhwnclq', '', '', '2019-02-28 12:05:39', '2019-02-28 15:05:39', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/problemas-do-meu-bairro-16769zivugicoxhwnclq.jpg', 0, 'attachment', 'image/jpeg', 0),
(697, 1, '2019-02-28 12:11:00', '2019-02-28 15:11:00', '', 'diego-', '', 'inherit', 'open', 'closed', '', 'diego-3', '', '', '2019-02-28 12:11:00', '2019-02-28 15:11:00', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-diego.jpg', 0, 'attachment', 'image/jpeg', 0),
(698, 1, '2019-02-28 12:11:01', '2019-02-28 15:11:01', '', 'felipe-', '', 'inherit', 'open', 'closed', '', 'felipe-2', '', '', '2019-02-28 12:11:01', '2019-02-28 15:11:01', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-felipe.jpg', 0, 'attachment', 'image/jpeg', 0),
(699, 1, '2019-02-28 12:11:01', '2019-02-28 15:11:01', '', 'jonas', '', 'inherit', 'open', 'closed', '', 'jonas-2', '', '', '2019-02-28 12:11:01', '2019-02-28 15:11:01', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-jonas.jpg', 0, 'attachment', 'image/jpeg', 0),
(700, 1, '2019-02-28 12:11:13', '2019-02-28 15:11:13', '', 'lacula', '', 'inherit', 'open', 'closed', '', 'lacula-2', '', '', '2019-02-28 12:11:13', '2019-02-28 15:11:13', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-lacula.jpg', 0, 'attachment', 'image/jpeg', 0),
(701, 1, '2019-02-28 13:48:36', '2019-02-28 16:48:36', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\r\n\r\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\r\n\r\n<strong>Comente:</strong>\r\n\r\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-28 13:48:36', '2019-02-28 16:48:36', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(702, 1, '2019-02-28 13:52:59', '2019-02-28 16:52:59', 'O meu bairro ou omeubairro.online é uma ferramenta colaborativa onde cidadãos podem fazer uma denuncia de algum problema no seu bairro, ou, podem também se unir a outros moradores e resolver um problema temporariamente enquanto a prefeitura ou estado não o fazem, assim ajudando o bairro e as pessoas da melhor forma possível.\r\n\r\nFoi idealizado e feito por jovens de periferia provenientes de ONG\'s que hoje trabalham com tecnologia,  justamente pensando em como melhorar a vida das pessoas no bairro, nasceu o projeto que consiste em denuncias e ações colaborativas afim de resolver problemas no próprio bairro.\r\n\r\n&nbsp;\r\n\r\nContato: problemasdobairrofortaleza@<wbr />gmail.com', 'O que é?', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-02-28 13:52:59', '2019-02-28 16:52:59', '', 9, 'http://omeubairro.online/9-revision-v1/', 0, 'revision', '', 0),
(703, 1, '2019-02-28 13:54:47', '2019-02-28 16:54:47', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\r\n\r\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\r\n\r\nNo cadastro existe a opção de deixar nome, telefone e skype, através desses dados os próprios cidadãos podem se articular, conversar entre si e marcar para resolver problema \"X\".\r\n\r\nPodem também usar os comentários no próprio site para tal, quando resolver um problema podem enviar fotos para atualizar pelos comentários ou por e-mail: problemasdobairrofortaleza@<wbr />gmail.com\r\n\r\n&nbsp;\r\n\r\n<strong>Comente:</strong>\r\n\r\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-28 13:54:47', '2019-02-28 16:54:47', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(704, 1, '2019-02-28 13:55:17', '2019-02-28 16:55:17', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\r\n\r\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\r\n\r\nNo cadastro existe a opção de deixar nome, telefone e skype, através desses dados os próprios cidadãos podem se articular, conversar entre si e marcar para resolver problema \"X\".\r\n\r\nPodem também usar os comentários no próprio site para tal, quando resolver um problema podem enviar fotos para atualizar pelos comentários ou por e-mail: problemasdobairrofortaleza@<wbr />gmail.com\r\n\r\n<strong>Banco de ideias:</strong>\r\n\r\nLinks com ideias fáceis e uteis de serem replicadas.\r\n\r\n<strong>Comente:</strong>\r\n\r\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-28 13:55:17', '2019-02-28 16:55:17', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(705, 1, '2019-02-28 13:57:16', '2019-02-28 16:57:16', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\r\n\r\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\r\n\r\nNo cadastro existe a opção de deixar nome, telefone e skype, através desses dados os próprios cidadãos podem se articular, conversar entre si e marcar para resolver problema \"X\".\r\n\r\nPodem também usar os comentários no próprio site para tal, quando resolver um problema podem enviar fotos para atualizar pelos comentários ou por e-mail: problemasdobairrofortaleza@<wbr />gmail.com\r\n\r\n<strong>Banco de ideias:</strong>\r\n\r\nLinks com ideias fáceis e uteis de serem replicadas.\r\n\r\n&nbsp;\r\n\r\n<strong><a class=\"row-title\" href=\"https://omeubairro.online/wp-admin/post.php?post=646&amp;action=edit\" aria-label=\"“Mapa de problemas” (Editar)\">Mapa de problemas</a></strong>\r\n\r\nOs usuários podem também marcar no mapa colaborativo um problema, é só seguir o modelo existente no próprio mapa e cadastrar uma nova ocorrência.\r\n\r\n<strong>Comente:</strong>\r\n\r\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-28 13:57:16', '2019-02-28 16:57:16', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(706, 1, '2019-02-28 13:58:23', '2019-02-28 16:58:23', 'Você quer fazer uma denuncia ou colocar a mão na massa e resolver um problema no seu bairro?\r\n\r\n<strong>Denunciar:</strong>\r\n\r\nVocê pode bater uma foto, descrever o problema e fazer uma denuncia anonima no portal. É rápido e seguro.\r\n\r\nUma base de dados com denuncias em diversas categorias é criado, prefeituras e governos podem acessar e checar por bairro, o portal se torna um local para unir e agrupar as denuncias.\r\n\r\n<strong>Faça você mesmo:</strong>\r\n\r\nCaso você mesmo queira resolver o problema, faça uma denuncia, deixe um contato e entre em contato com outras pessoas interessadas em resolver temporariamente o problema e não esperar pela prefeitura, mesmo, pagando nossos impostos.\r\n\r\nNo cadastro existe a opção de deixar nome, telefone e skype, através desses dados os próprios cidadãos podem se articular, conversar entre si e marcar para resolver problema \"X\".\r\n\r\nPodem também usar os comentários no próprio site para tal, quando resolver um problema podem enviar fotos para atualizar pelos comentários ou por e-mail: problemasdobairrofortaleza@<wbr />gmail.com\r\n\r\n<strong>Banco de ideias:</strong>\r\n\r\nLinks com ideias fáceis e uteis de serem replicadas.\r\n\r\n&nbsp;\r\n\r\n<strong><a class=\"row-title\" href=\"https://omeubairro.online/mapa-de-problemas\" aria-label=\"“Mapa de problemas” (Editar)\">Mapa de problemas</a></strong>\r\n\r\nOs usuários podem também marcar no mapa colaborativo um problema, é só seguir o modelo existente no próprio mapa e cadastrar uma nova ocorrência.\r\n\r\n<strong>Comente:</strong>\r\n\r\nDeixe um comentário e atualize com a melhor solução de como está o problema: <strong>[Resolvido] [Atual]</strong>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Como funciona?', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-02-28 13:58:23', '2019-02-28 16:58:23', '', 11, 'http://omeubairro.online/11-revision-v1/', 0, 'revision', '', 0),
(707, 1, '2019-02-28 15:56:54', '2019-02-28 18:56:54', '', 'logo-o-meu-bairro', '', 'inherit', 'open', 'closed', '', 'logo-o-meu-bairro', '', '', '2019-02-28 15:56:54', '2019-02-28 18:56:54', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-logo-o-meu-bairro.png', 0, 'attachment', 'image/png', 0),
(708, 1, '2019-02-28 15:59:52', '2019-02-28 18:59:52', '', 'logo-o-meu-bairro', '', 'inherit', 'open', 'closed', '', 'logo-o-meu-bairro-2', '', '', '2019-02-28 15:59:52', '2019-02-28 18:59:52', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-logo-o-meu-bairro-1.png', 0, 'attachment', 'image/png', 0),
(709, 1, '2019-02-28 16:01:35', '2019-02-28 19:01:35', '', 'logo-o-meu-bairro', '', 'inherit', 'open', 'closed', '', 'logo-o-meu-bairro-3', '', '', '2019-02-28 16:01:35', '2019-02-28 19:01:35', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/02/o-meu-bairro-logo-o-meu-bairro-2.png', 0, 'attachment', 'image/png', 0),
(710, 1, '2019-03-01 07:37:34', '2019-03-01 10:37:34', 'Tampa de bueiro aberta, extremamente perigoso, em dia de chuva fica coberto o que deixa mais perigoso ainda.\r\nEm frente ao colégio provecto.', 'Maraponga', '', 'publish', 'open', 'open', '', 'maraponga-3', '', '', '2019-03-07 09:52:27', '2019-03-07 12:52:27', '', 0, 'https://omeubairro.online/?p=710', 0, 'post', '', 0),
(712, 1, '2019-03-01 07:37:34', '2019-03-01 10:37:34', 'Tampa de bueiro aberta, extremamente perigoso, em dia de chuva fica coberto o que deixa mais perigoso ainda.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '710-revision-v1', '', '', '2019-03-01 07:37:34', '2019-03-01 10:37:34', '', 710, 'http://omeubairro.online/710-revision-v1/', 0, 'revision', '', 0),
(715, 1, '2019-03-01 08:25:40', '2019-03-01 11:25:40', '', '1_d2MAPp7120q_8x6Ue8KYmQ', '', 'inherit', 'open', 'closed', '', '1_d2mapp7120q_8x6ue8kymq', '', '', '2019-03-01 08:25:40', '2019-03-01 11:25:40', '', 710, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-1_d2mapp7120q_8x6ue8kymq.png', 0, 'attachment', 'image/png', 0),
(716, 1, '2019-03-01 08:27:15', '2019-03-01 11:27:15', '', 'bueiro-maraponga', '', 'inherit', 'open', 'closed', '', 'bueiro-maraponga-2', '', '', '2019-03-01 08:27:15', '2019-03-01 11:27:15', '', 710, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-bueiro-maraponga.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `bai_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(717, 1, '2019-03-01 08:27:38', '2019-03-01 11:27:38', 'Tampa de bueiro aberta, extremamente perigoso, em dia de chuva fica coberto o que deixa mais perigoso ainda.\r\nEm frente ao colégio provecto.', 'Maraponga', '', 'inherit', 'closed', 'closed', '', '710-revision-v1', '', '', '2019-03-01 08:27:38', '2019-03-01 11:27:38', '', 710, 'http://omeubairro.online/710-revision-v1/', 0, 'revision', '', 0),
(719, 1, '2019-03-07 09:29:37', '2019-03-07 12:29:37', '', 'edeas', '', 'inherit', 'open', 'closed', '', 'edeas', '', '', '2019-03-07 09:29:37', '2019-03-07 12:29:37', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-edeas.jpg', 0, 'attachment', 'image/jpeg', 0),
(720, 1, '2019-03-07 09:32:18', '2019-03-07 12:32:18', '', 'diego-curumim', '', 'inherit', 'open', 'closed', '', 'diego-curumim', '', '', '2019-03-07 09:32:18', '2019-03-07 12:32:18', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-diego-curumim.jpg', 0, 'attachment', 'image/jpeg', 0),
(721, 1, '2019-03-07 09:32:18', '2019-03-07 12:32:18', '', 'e-deas', '', 'inherit', 'open', 'closed', '', 'e-deas', '', '', '2019-03-07 09:32:18', '2019-03-07 12:32:18', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-e-deas.jpg', 0, 'attachment', 'image/jpeg', 0),
(722, 1, '2019-03-07 10:02:04', '2019-03-07 13:02:04', '', 'nelis', '', 'inherit', 'open', 'closed', '', 'nelis', '', '', '2019-03-07 10:02:04', '2019-03-07 13:02:04', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-nelis.jpg', 0, 'attachment', 'image/jpeg', 0),
(723, 1, '2019-03-07 13:04:43', '2019-03-07 16:04:43', '', 'dsvdsf', '', 'draft', 'open', 'open', '', 'dsvdsf', '', '', '2019-03-07 13:05:26', '2019-03-07 16:05:26', '', 0, 'http://omeubairro.online/?p=723', 0, 'post', '', 0),
(724, 1, '2019-03-07 13:04:43', '2019-03-07 16:04:43', '', 'dsvdsf', '', 'inherit', 'closed', 'closed', '', '723-revision-v1', '', '', '2019-03-07 13:04:43', '2019-03-07 16:04:43', '', 723, 'http://omeubairro.online/723-revision-v1/', 0, 'revision', '', 0),
(727, 1, '2019-03-07 14:59:51', '2019-03-07 17:59:51', '', 'logo-o-meu-bairro-original', '', 'inherit', 'open', 'closed', '', 'logo-o-meu-bairro-original', '', '', '2019-03-07 14:59:51', '2019-03-07 17:59:51', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-logo-o-meu-bairro-original.png', 0, 'attachment', 'image/png', 0),
(731, 1, '2019-03-07 15:12:29', '2019-03-07 18:12:29', '', 'logo-o-meu-bairro', '', 'inherit', 'open', 'closed', '', 'logo-o-meu-bairro-4', '', '', '2019-03-07 15:12:29', '2019-03-07 18:12:29', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-logo-o-meu-bairro.png', 0, 'attachment', 'image/png', 0),
(732, 1, '2019-03-07 15:12:52', '2019-03-07 18:12:52', '', 'logo-bairro', '', 'inherit', 'open', 'closed', '', 'logo-bairro', '', '', '2019-03-07 15:12:52', '2019-03-07 18:12:52', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-logo-bairro.png', 0, 'attachment', 'image/png', 0),
(733, 1, '2019-03-07 15:13:48', '2019-03-07 18:13:48', '', 'logo-bairro', '', 'inherit', 'open', 'closed', '', 'logo-bairro-2', '', '', '2019-03-07 15:13:48', '2019-03-07 18:13:48', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-logo-bairro-1.png', 0, 'attachment', 'image/png', 0),
(734, 1, '2019-03-07 15:14:34', '2019-03-07 18:14:34', '', 'logo-bairro', '', 'inherit', 'open', 'closed', '', 'logo-bairro-3', '', '', '2019-03-07 15:14:34', '2019-03-07 18:14:34', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-logo-bairro-2.png', 0, 'attachment', 'image/png', 0),
(735, 1, '2019-03-07 15:21:00', '2019-03-07 18:21:00', '', 'geovani', '', 'inherit', 'open', 'closed', '', 'geovani', '', '', '2019-03-07 15:21:00', '2019-03-07 18:21:00', '', 0, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-geovani.png', 0, 'attachment', 'image/png', 0),
(736, 1, '2019-03-07 15:37:43', '2019-03-07 18:37:43', '', 'pessoas-unidas', '', 'inherit', 'open', 'closed', '', 'pessoas-unidas', '', '', '2019-03-07 15:37:43', '2019-03-07 18:37:43', '', 16, 'http://omeubairro.online/wp-content/uploads/2018/11/o-meu-bairro-pessoas-unidas.jpg', 0, 'attachment', 'image/jpeg', 0),
(737, 1, '2019-03-07 15:38:51', '2019-03-07 18:38:51', '', 'pessoas-unidas', '', 'inherit', 'open', 'closed', '', 'pessoas-unidas-2', '', '', '2019-03-07 15:38:51', '2019-03-07 18:38:51', '', 16, 'http://omeubairro.online/wp-content/uploads/2018/11/o-meu-bairro-pessoas-unidas-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(738, 1, '2019-03-07 15:41:54', '2019-03-07 18:41:54', '', 'pessoas-unidas', '', 'inherit', 'open', 'closed', '', 'pessoas-unidas-3', '', '', '2019-03-07 15:41:54', '2019-03-07 18:41:54', '', 16, 'http://omeubairro.online/wp-content/uploads/2018/11/o-meu-bairro-pessoas-unidas-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(739, 1, '2019-03-10 21:46:27', '2019-03-11 00:46:27', 'Buraco no asfalto na Rua Paulo Firmeza com José Justa no bairro Pio XII.\r\nBuraco sinalizado por moradores', 'Pio XII', '', 'publish', 'open', 'open', '', 'pio-xii', '', '', '2019-03-10 21:46:27', '2019-03-11 00:46:27', '', 0, 'https://omeubairro.online/?p=739', 0, 'post', '', 0),
(740, 1, '2019-03-10 21:44:55', '2019-03-11 00:44:55', '', 'IMG-20190310-WA0001', '', 'inherit', 'open', 'closed', '', 'img-20190310-wa0001', '', '', '2019-03-10 21:44:55', '2019-03-11 00:44:55', '', 739, 'http://omeubairro.online/wp-content/uploads/2019/03/o-meu-bairro-img-20190310-wa0001.jpg', 0, 'attachment', 'image/jpeg', 0),
(741, 1, '2019-03-10 21:46:27', '2019-03-11 00:46:27', 'Buraco no asfalto na Rua Paulo Firmeza com José Justa no bairro Pio XII.\r\nBuraco sinalizado por moradores', 'Pio XII', '', 'inherit', 'closed', 'closed', '', '739-revision-v1', '', '', '2019-03-10 21:46:27', '2019-03-11 00:46:27', '', 739, 'http://omeubairro.online/739-revision-v1/', 0, 'revision', '', 0),
(742, 1, '2019-04-10 08:50:33', '2019-04-10 11:50:33', 'O F-Droid é um repositório de software mantido pela comunidade para Android, semelhante à loja do Google Play. O repositório principal, hospedado pelo projeto, contém apenas aplicativos gratuitos de software livre.\r\n\r\nA principal diferença entre o F-Droid e outros app arquivos é que todo seu conteúdo é open-source.\r\n\r\nF-Droid é um excelente app oficial com um fantástico arquivo de apps para Android. No total, você encontrará mais de 1500 apps diferentes, juntamente com suas versões mais antigas também disponíveis para serem baixadas. Tudo isso em uma simples e elegante interface.\r\n\r\nAlém de todos os aplicativos serem checados pela comunidade, eles não tem rastreiam e te vigiam.\r\n\r\n<span style=\"color: #0000ff;\">https://f-droid.org/</span>', 'F-Droid é um repositório de apps alternativo à Play Store do Google', '', 'publish', 'closed', 'closed', '', 'f-droid-e-um-repositorio-de-apps-alternativo-a-play-store-do-google', '', '', '2019-04-10 08:55:19', '2019-04-10 11:55:19', '', 0, 'http://omeubairro.online/?post_type=banco&#038;p=742', 0, 'banco', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_termmeta`
--

CREATE TABLE `bai_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_terms`
--

CREATE TABLE `bai_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  `term_order` int(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_terms`
--

INSERT INTO `bai_terms` (`term_id`, `name`, `slug`, `term_group`, `term_order`) VALUES
(1, 'Outras Categorias', 'outras-categorias', 0, 0),
(2, 'Menu Principal', 'menu-principal', 0, 0),
(3, 'Lixo', 'lixo', 0, 0),
(4, 'Asfalto', 'asfalto', 0, 0),
(5, 'identificados', 'identificados', 0, 0),
(6, 'resolvidos', 'resolvidos', 0, 0),
(7, 'Resolvido', 'resolvido', 0, 0),
(8, 'Buracos', 'buracos', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_term_relationships`
--

CREATE TABLE `bai_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_term_relationships`
--

INSERT INTO `bai_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(5, 2, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(535, 6, 0),
(536, 6, 0),
(537, 6, 0),
(538, 5, 0),
(539, 5, 0),
(540, 5, 0),
(542, 6, 0),
(544, 5, 0),
(545, 5, 0),
(546, 5, 0),
(636, 3, 0),
(648, 2, 0),
(655, 3, 0),
(710, 8, 0),
(723, 4, 0),
(723, 7, 0),
(739, 4, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_term_taxonomy`
--

CREATE TABLE `bai_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_term_taxonomy`
--

INSERT INTO `bai_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 5),
(3, 3, 'category', '', 0, 2),
(4, 4, 'category', '', 0, 1),
(5, 5, 'categoria', '', 0, 0),
(6, 6, 'categoria', '', 0, 0),
(7, 7, 'category', '', 0, 0),
(8, 8, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_usermeta`
--

CREATE TABLE `bai_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_usermeta`
--

INSERT INTO `bai_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'diegocurumim'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'bai_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'bai_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,addtoany_settings_pointer'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"a64eaacba8e6c599ae55e4daa1e2c493c22faa6ba49a5e1d46610af243aba530\";a:4:{s:10:\"expiration\";i:1563470384;s:2:\"ip\";s:15:\"181.221.192.227\";s:2:\"ua\";s:142:\"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 YaBrowser/19.6.3.185 Yowser/2.5 Safari/537.36\";s:5:\"login\";i:1563297584;}}'),
(17, 1, 'bai_dashboard_quick_press_last_post_id', '3'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"181.221.192.0\";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'bai_user-settings', 'libraryContent=browse&hidetb=1&editor=tinymce&unfold=1&mfold=o'),
(23, 1, 'bai_user-settings-time', '1554897028'),
(24, 1, 'last_login_time', '2019-07-16 14:19:44'),
(25, 1, 'closedpostboxes_post', 'a:0:{}'),
(26, 1, 'metaboxhidden_post', 'a:11:{i:0;s:14:\"acf_acf_banner\";i:1;s:7:\"acf_463\";i:2;s:7:\"acf_471\";i:3;s:12:\"revisionsdiv\";i:4;s:11:\"postexcerpt\";i:5;s:13:\"trackbacksdiv\";i:6;s:10:\"postcustom\";i:7;s:16:\"commentstatusdiv\";i:8;s:11:\"commentsdiv\";i:9;s:7:\"slugdiv\";i:10;s:9:\"authordiv\";}'),
(27, 1, 'ac_preferences_check-review', 'a:1:{s:18:\"first-login-review\";i:1551440720;}'),
(28, 1, 'bai_ac_preferences_settings', 'a:1:{s:11:\"list_screen\";s:4:\"post\";}'),
(29, 1, 'aioseop_seen_about_page', '2.11'),
(30, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:22:\"semperplugins-rss-feed\";s:4:\"side\";s:12:\"gadwp-widget\";s:7:\"column3\";s:0:\"\";s:7:\"column4\";s:0:\"\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_users`
--

CREATE TABLE `bai_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `bai_users`
--

INSERT INTO `bai_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'diegocurumim', '$P$BBTlAbQpEQqmwrXMonPyoa6iM2dYme0', 'diegocurumim', 'diegocuruma@gmail.com', '', '2018-11-09 18:01:05', '', 0, 'diegocurumim');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_yoast_seo_links`
--

CREATE TABLE `bai_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bai_yoast_seo_meta`
--

CREATE TABLE `bai_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bai_aiowps_events`
--
ALTER TABLE `bai_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bai_aiowps_failed_logins`
--
ALTER TABLE `bai_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bai_aiowps_global_meta`
--
ALTER TABLE `bai_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `bai_aiowps_login_activity`
--
ALTER TABLE `bai_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bai_aiowps_login_lockdown`
--
ALTER TABLE `bai_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bai_aiowps_permanent_block`
--
ALTER TABLE `bai_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bai_commentmeta`
--
ALTER TABLE `bai_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bai_comments`
--
ALTER TABLE `bai_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `bai_links`
--
ALTER TABLE `bai_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `bai_options`
--
ALTER TABLE `bai_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `bai_postmeta`
--
ALTER TABLE `bai_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bai_posts`
--
ALTER TABLE `bai_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `bai_termmeta`
--
ALTER TABLE `bai_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bai_terms`
--
ALTER TABLE `bai_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `bai_term_relationships`
--
ALTER TABLE `bai_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `bai_term_taxonomy`
--
ALTER TABLE `bai_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `bai_usermeta`
--
ALTER TABLE `bai_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `bai_users`
--
ALTER TABLE `bai_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `bai_yoast_seo_links`
--
ALTER TABLE `bai_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `bai_yoast_seo_meta`
--
ALTER TABLE `bai_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bai_aiowps_events`
--
ALTER TABLE `bai_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_aiowps_failed_logins`
--
ALTER TABLE `bai_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_aiowps_global_meta`
--
ALTER TABLE `bai_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_aiowps_login_activity`
--
ALTER TABLE `bai_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `bai_aiowps_login_lockdown`
--
ALTER TABLE `bai_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_aiowps_permanent_block`
--
ALTER TABLE `bai_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_commentmeta`
--
ALTER TABLE `bai_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_comments`
--
ALTER TABLE `bai_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_links`
--
ALTER TABLE `bai_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_options`
--
ALTER TABLE `bai_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4836;

--
-- AUTO_INCREMENT for table `bai_postmeta`
--
ALTER TABLE `bai_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1913;

--
-- AUTO_INCREMENT for table `bai_posts`
--
ALTER TABLE `bai_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=743;

--
-- AUTO_INCREMENT for table `bai_termmeta`
--
ALTER TABLE `bai_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bai_terms`
--
ALTER TABLE `bai_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bai_term_taxonomy`
--
ALTER TABLE `bai_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bai_usermeta`
--
ALTER TABLE `bai_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `bai_users`
--
ALTER TABLE `bai_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bai_yoast_seo_links`
--
ALTER TABLE `bai_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
